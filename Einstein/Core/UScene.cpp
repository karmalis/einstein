#include "UScene.hpp"
#include "Tasking\Channel.hpp"
#include "ResourceManager.hpp"

#include "Engine.hpp"

#include "Scene\Loader.hpp"

namespace Einstein {

	namespace Core {

		/* Scene Root */
		SceneRoot::SceneRoot() :
			UniversalObject("Root")
		{
			
		}

		SceneRoot::~SceneRoot()
		{

		}

		void SceneRoot::ClearSceneObjects()
		{
			for (auto& it : this->_children)
			{
				it->Destroy();
			}

			this->_childrenIDMap.clear();
			this->_childrenStringMap.clear();
			this->_children.clear();
		}

		/* Universal Scene */
		UniversalScene::UniversalScene() :
			System("UniversalScene")
		{
			Tasking::Channel::add<Indexer>(this);
			Tasking::Channel::add<LoadScene>(this);

			this->_sceneLoader = new Loader();

			// Register default core components
			this->_sceneLoader->AddComponentCreator("CameraComponent", CameraComponent::Create);

			// Create the Root Object
			this->AddIndex(new SceneRoot());
		}

		bool UniversalScene::Initialise()
		{
			this->_engine->UpdateSystem(this, true, true);

			return System::Initialise();

			gLog << "Setting up Universal Scene System";
		}

		void UniversalScene::Update()
		{
			System::Update();

			// Read the relevant systems and update actors based upon them
			// i.e. get time
			this->GetSceneRoot()->Update(0);
		}

		void UniversalScene::ShutDown()
		{
			gLog << "Shutting Down Universal Scene System";

			delete[] this->_sceneLoader;

			System::ShutDown();
		}

		SceneRoot* UniversalScene::GetSceneRoot() const
		{
			return this->GetIndexObject<SceneRoot>("Root");
		}

		void UniversalScene::UnloadScene()
		{
			this->GetSceneRoot()->ClearSceneObjects();
		}

		void UniversalScene::AddIndex(UIndexable* object)
		{
			this->AddIndex(std::unique_ptr<UIndexable>(object));
		}

		void UniversalScene::AddIndex(std::unique_ptr<UIndexable> object)
		{
			ScopedLock lock(this->_mutex);

			gLog << "Adding index: " << object->GetName();

			auto it = this->_uIndexMap.find(object->GetName());

			if (it == this->_uIndexMap.end())
			{
				object->_scene = this;
				this->_uIndexMap[object->GetIndexPath()] = object.get();
				this->_uIndexList.push_back(std::move(object));
			}
			else {
				throw new Util::Exception(std::string("Trying to add a duplicate index: ") + object->GetName());
			}
		}

		void UniversalScene::Remove(std::string index)
		{
			UIndexable* s = nullptr;

			auto it = this->_uIndexMap.find(index);
			if (it != this->_uIndexMap.end())
			{
				s = it->second;
			}

			if (s) this->Remove(s);
			else {
				gLogError << "Trying to remove a universal index that is not registered " << index;
			}
		}

		void UniversalScene::Remove(UIndexable* index)
		{
			ScopedLock lock(this->_mutex);

			gLog << "Removing index: " << index->GetName();

			auto it = std::remove_if(
				this->_uIndexList.begin(),
				this->_uIndexList.end(),
				[index](const IndexPtr& ptr) {
				return ptr.get() == index;
			}
			);

			if (it != this->_uIndexList.end())
			{
				this->_uIndexList.erase(it);

				for (auto jt = this->_uIndexMap.begin(); jt != this->_uIndexMap.end(); ++jt)
				{
					if (jt->second == index)
					{
						this->_uIndexMap.erase(jt);
						break;
					}

				}
			}
		}

		void UniversalScene::operator()(const Indexer& ri)
		{
			switch (ri.command)
			{
			case Indexer::ADD:
			{
				this->AddIndex(ri._index);
			} break;
			case Indexer::REMOVE:
			{
				this->Remove(ri._index);
			} break;
			case Indexer::REINDEX:
			{
				this->Reindex(ri._index);
			} break;
			case Indexer::NONE:
			default:
			{

			} break;
			}
		}

		void UniversalScene::operator()(const LoadScene& ls)
		{
			gLog << "Loading scene: " << ls.scene;
			ResourceManager* resourceManager = this->_engine->GetSystem<ResourceManager>();
			if (resourceManager)
			{
				this->_sceneLoader->LoadScene(
					resourceManager->GetJSONDocumentResource("default_scene.json"),
					this
					);
				this->_sceneLoader->LoadScene(
					resourceManager->GetJSONDocumentResource(ls.scene),
					this
					);
				gLog << "Finished loading Scene";
			}
			else {
				gLogError << "Could not load scene";
			}
		}

		void UniversalScene::Reindex(UIndexable* object)
		{
			ScopedLock lock(this->_mutex);

			gLog << "Reindexing: " << object->GetName();

			IndexMap newMap;
			for (auto it = this->_uIndexMap.begin(); it != this->_uIndexMap.end(); ++it)
			{
				if (it->second->GetID() != object->GetID())
				{
					newMap[it->first] = it->second;
				}
			}

			newMap[object->GetIndexPath()] = object;			
			std::swap(this->_uIndexMap, newMap);
		}

		Loader* UniversalScene::GetSceneLoader()
		{
			return this->_sceneLoader;
		}

		CameraComponent* UniversalScene::GetMainCamera()
		{
			std::list<CameraComponent*> cameraList = this->GetIndexObjectsByType<CameraComponent>();
			for (const auto it : cameraList)
			{
				if (it->IsMain())
				{
					return it;
				}
			}

			return nullptr;
		}

	}
}