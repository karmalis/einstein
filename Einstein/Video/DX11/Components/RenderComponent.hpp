#ifndef __RENDER_COMPONENT_HPP__
#define __RENDER_COMPONENT_HPP__

#include "../../../Core/Actors/ActorComponent.hpp"
#include "../../../Includes.hpp"

#include "DX11Mesh.hpp"
#include "../DX11TextureManager.hpp"

namespace Einstein {

	namespace Video {

		class DX11RenderComponent : public Core::ActorComponent
		{
		public:

			using MeshList = std::vector < DX11Mesh > ;

			virtual bool Initialise(const rapidjson::Value& params);
			virtual void Update(const double& deltaTime);
			virtual void Destroy();

			void Render(ID3D11DeviceContext* context, const D3DXMATRIX& matView, const D3DXMATRIX& matProjection, const DX11TextureManager& textureManager);

			static Core::ActorComponent* Create(const rapidjson::Value& params, Core::Actor* actor);

		protected:

			bool LoadMesh(const rapidjson::Value& meshParams);

			DX11RenderComponent(std::string name);

			MeshList _meshList;

		};

	}
}

#endif