#ifndef __CHANNEL_HPP__
#define __CHANNEL_HPP__

#include "ChannelQueue.hpp"

#include <memory>

namespace Einstein {

	namespace Core {

		namespace Tasking {

			class Channel {

			public:

				template <typename tEvent, class tHandler>
				static void add(tHandler* handler);

				template <typename tEvent, class tHandler>
				static void remove(tHandler* handler);

				template <typename tEvent>
				static void broadcast(const tEvent& message);

			};

			template <typename tMessage>
			class MessageHandler
			{
			public:

				MessageHandler();
				virtual ~MessageHandler();

				virtual void operator()(const tMessage&) = 0;
			};

		}

	}

}

#include "Channel.inl"

#endif