#ifndef __TASK_MANAGER_HPP__
#define __TASK_MANAGER_HPP__

#include "Task.hpp"


#include <tbb\concurrent_priority_queue.h>
#include <list>

namespace Einstein {

	namespace Core {

		namespace Tasking {

			class TaskManager
			{
			protected:

				typedef tbb::concurrent_priority_queue<WrappedTask, CompareWrappedTasks> TaskQueue;

			public:

				TaskManager(size_t numWorkers = 0);

				void addWork(Task t, bool repeating = false, bool background = false);

				void addRepeatingWork(Task t, bool background = false);
				void addBackgroundWork(Task t, bool repeating = false);
				void addRepeatingBackgroundWork(Task t);

				void start();
				void stop();

			protected:

				void addWork(WrappedTask t);
				void execute(WrappedTask t);

				TaskQueue _mainTasks;
				TaskQueue _backgroundTasks;

				bool _isRunning;

				size_t _numWorkers;
				std::list<tbb::tbb_thread*> _threads;

			};

		}

	}

}

#endif