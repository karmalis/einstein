#include "System.hpp"



namespace Einstein {

	namespace Core {

		System::System(std::string name) 
			: Named(std::move(name)),
			_stateFlags(IDLE)
		{
			Tasking::Channel::add<StateChangeAction>(this);
		}

		bool System::Initialise()
		{
			gLog << "Initialising " << this->getName();
			return true;
		}

		void System::Update()
		{

		}

		void System::ShutDown()
		{
			gLog << "Shutting down " << this->getName();
		}

		void System::ChangeStateFlags(const int& flags)
		{
			//Tasking::Channel::broadcast(StateChangeNotification{ this->_stateFlags, flags, this->getName() });
			this->_stateFlags = flags;
		}

		void System::operator()(const StateChangeAction& state)
		{
			if (state._target != this->getName()) return;

			this->ChangeStateFlags(state._stateFlags);
		}

	}

}