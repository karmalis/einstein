#include "ShadowMap.hpp"

#include "../../Core/Logging/Logger.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			ShadowMap::ShadowMap()
			{

			}

			ShadowMap::~ShadowMap()
			{

			}

			bool ShadowMap::Initialise(const unsigned int& width, const unsigned int& height, std::string shader)
			{
				this->_width = width;
				this->_height = height;

				glGenFramebuffers(1, &this->_fbo);
				glBindFramebuffer(GL_FRAMEBUFFER, this->_fbo);

				glGenTextures(1, &this->_shadowTexture);
				glBindTexture(GL_TEXTURE_2D, this->_shadowTexture);

				glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

				glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, this->_shadowTexture, 0);

				glDrawBuffer(GL_NONE);
				glReadBuffer(GL_NONE);

				GLuint status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

				if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
				{
					gLogError << "Could not initialise shadow map for light";
					return false;
				}

				if (!this->_depthShader.Load(shader))
				{
					gLogError << "Could not initialise depth shader";
					return false;
				}

				return true;
			}

			void ShadowMap::Start()
			{
				glBindFramebuffer(GL_FRAMEBUFFER, this->_fbo);
				glDepthMask(GL_TRUE);
				glEnable(GL_DEPTH_TEST);
				glDrawBuffer(GL_NONE);
				glReadBuffer(GL_NONE);
				glClear(GL_DEPTH_BUFFER_BIT);
				this->_depthShader.Enable();
			}

			void ShadowMap::Stop()
			{
				this->_depthShader.Disable();
				glDepthMask(GL_FALSE);
				glEnable(GL_DEPTH_TEST);
				glBindFramebuffer(GL_FRAMEBUFFER, 0);
				glDrawBuffer(GL_BACK);
				glReadBuffer(GL_BACK);
			}

			void ShadowMap::Destroy()
			{
				this->_depthShader.Unload();
				glDeleteTextures(1, &this->_shadowTexture);
				glDeleteFramebuffers(1, &this->_fbo);
			}

		}

	}

}