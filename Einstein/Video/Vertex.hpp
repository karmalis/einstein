#ifndef __VERTEX_HPP__
#define __VERTEX_HPP__

#include <mathfu\vector.h>

namespace Einstein {

	namespace Video {

		struct Vertex2d
		{
			mathfu::Vector<double, 2> position;
			mathfu::Vector<double, 2> textureCoordinate;
			mathfu::Vector<double, 2> normal;
		};

		struct Vertex2f
		{
			mathfu::Vector<float, 2> position;
			mathfu::Vector<float, 2> textureCoordinate;
			mathfu::Vector<float, 2> normal;
		};

		struct Vertex2fElement2D
		{
			mathfu::Vector<float, 3> position;
			mathfu::Vector<float, 4> color;
			mathfu::Vector<float, 2> textureCoordinate;
			
		};

		struct Vertex3d
		{
			mathfu::Vector<double, 3> position;
			mathfu::Vector<double, 2> textureCoordinate;
			mathfu::Vector<double, 4> color;
			mathfu::Vector<double, 3> normal;
			mathfu::Vector<double, 3> tangent;
		};

		struct Vertex3f
		{
			mathfu::Vector<float, 3> position;
			mathfu::Vector<float, 2> textureCoordinate;
			//mathfu::Vector<float, 4> color;
			mathfu::Vector<float, 3> normal;
			mathfu::Vector<float, 3> tangent;
		};

		struct VertexPos
		{
			Vertex3d pos;
		};

	}

}

#endif