#include "UObject.hpp"

#include "Logging\Logger.hpp"

namespace Einstein {

	namespace Core {

		
		UniversalObject::UniversalObject(const std::string& name) :
			UIndexable(name)
		{
			gLogDebug << "Creating new object: " << this->_name << " => " << this->_ID;
		}

		void UniversalObject::Update(const double& deltaTime)
		{
			for (auto& it : this->_children)
			{
				it->Update(deltaTime);
			}
		}

		void UniversalObject::Destroy()
		{
			gLogDebug << "Destroying Object: " << this->_name << " => " << this->_ID;

			if (this->_children.size() > 0)
			{
				for (auto& it : this->_children)
				{
					it->Destroy();

					auto fIDit = this->_childrenIDMap.find(it->GetID());
					auto fSit = this->_childrenStringMap.find(it->GetName());

					if (fIDit != this->_childrenIDMap.end()) this->_childrenIDMap.erase(fIDit);
					if (fSit != this->_childrenStringMap.end()) this->_childrenStringMap.erase(fSit);
				}

				this->_children.clear();
			}
		}

		UObjectPtr UniversalObject::FindChild(const std::string& name)
		{
			ChildrenStringMap::iterator it = this->_childrenStringMap.find(name);
			if (it != this->_childrenStringMap.end()) return it->second;
			else return UObjectPtr();
		}

		UObjectPtr UniversalObject::FindChild(const unsigned int& id)
		{
			ChildrenIDMap::iterator it = this->_childrenIDMap.find(id);
			if (it != this->_childrenIDMap.end()) return it->second;
			else return UObjectPtr();
		}

		void UniversalObject::AddChild(const UObjectPtr& uObject)
		{
			this->_children.push_back(uObject);
			this->_childrenIDMap[uObject->GetID()] = uObject;
			this->_childrenStringMap[uObject->GetName()] = uObject;
			uObject->SetParent(this);
		}

		void UniversalObject::SetParent(const UObjectPtr& uObject)
		{
			this->_parent = uObject;
		}

		std::string UniversalObject::GetIndexPath() const
		{
			std::string result;

			if (this->_parent)
			{
				result = this->_parent->GetIndexPath();
			}

			return result.append("/").append(this->_name);
		}

		void UniversalObject::Detach()
		{
			this->_parent = UObjectPtr();
		}

	}

}