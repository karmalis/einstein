#pragma once

#include "LogMessage.hpp"
#include <memory>

namespace Einstein {
	namespace Core {
		
		class LogSink {
		public:

			/**
			The wrapped type T should have:
			- void operator()(const LogMessage& meta, const std::string& message) const;
			*/

			template <typename T>
			LogSink(T impl); // allows implicit conversion to LogSink

			LogSink(const LogSink& sink);
			LogSink& operator= (LogSink sink);

			bool operator == (const LogSink& sink) const;

			void forward(
				const LogMessage::Meta& meta,
				const std::string& message
				) const;

		protected:

			struct Concept {
				virtual ~Concept() = default;
				virtual Concept* clone() const = 0;

				virtual void forward(
					const LogMessage::Meta& meta,
					const std::string& message
					) const = 0;

			};

			template <typename T>
			struct Model : Concept {
				Model(T impl);

				virtual Concept* clone() const override;

				virtual void forward(
					const LogMessage::Meta& meta,
					const std::string& message
					) const override;

				T _impl;
			};

			std::unique_ptr<Concept> _wrapper;
		};

		LogSink makeConsoleSink();
		LogSink makeFileSink(const std::string& filename);

	}
}

#include "LogSink.inl";
