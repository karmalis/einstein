#include "TextureManager.hpp"

#include "../OGLSystem.hpp"
#include "../../../Core/Engine.hpp"

#include "../../../Util/Random.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL
		{

			tbb::atomic<unsigned int> globalTextureID = tbb::make_atomic<unsigned int>(100);

			TextureManager::TextureManager()
			{
				int x = 0;
			}

			bool TextureManager::Initialise()
			{
				gLog << "Initialising Texture Manager";

				gLog << "Texture Manager Initialized";

				return true;
			}

			void TextureManager::Update()
			{

				//this->_renderSystem->Lock();
				//glfwMakeContextCurrent(this->_slaveContextWindow);

				for (auto it : this->_textureList)
				{
					it->Update(0);
				}
				//this->_renderSystem->Unlock();

			}

			void TextureManager::ShutDown()
			{
				//glfwDestroyWindow(this->_slaveContextWindow);

				//this->_renderSystem->Lock();
				while (!this->_textureList.empty())
				{
					auto it = this->_textureList.front();
					it->Unload();
					this->_idTextureMap.erase(this->_idTextureMap.find(it->GetID()));
					this->_namedTextureMap.erase(this->_namedTextureMap.find(it->GetName()));
					this->_textureList.pop_front();
				}

				//this->_renderSystem->Unlock();
			}

			Texture* TextureManager::GetTexture(const unsigned int& id)
			{
				auto it = this->_idTextureMap.find(id);
				if (it != this->_idTextureMap.end()) return it->second;
				return nullptr;
			}

			Texture* TextureManager::GetTexture(const std::string& name)
			{
				auto it = this->_namedTextureMap.find(name);
				if (it != this->_namedTextureMap.end()) return it->second;
				return nullptr;
			}

			bool TextureManager::TextureLoaded(const std::string& name)
			{
				auto it = this->_namedTextureMap.find(name);
				if (it != this->_namedTextureMap.end()) return true;
				else return false;
			}

			const unsigned int& TextureManager::LoadTexture(const rapidjson::Value& params)
			{
				std::string file = params["file"].GetString();
				std::string name = params["name"].GetString();

				return this->LoadFromFile(file, name);
			}

			const unsigned int& TextureManager::LoadFromMemory(const unsigned char* data, const unsigned int& width, const unsigned int& height, const std::string& name)
			{
				std::string useName = name;

				if (!useName.empty() && this->TextureLoaded(useName))
				{
					this->GetTexture(useName)->IncUseCount();
					return this->GetTexture(useName)->GetID();
				}

				if (useName.empty())
				{
					useName = Util::Random::String(8);
				}

				Texture* texture = new Texture(width, height, data);
				texture->Setname(useName);
				unsigned int id = texture->GetID();
				this->_idTextureMap[id] = texture;
				this->_namedTextureMap[useName] = texture;
				this->_textureList.push_back(std::move(texture));

				return texture->GetID();
			}

			const unsigned int& TextureManager::LoadFromFile(const std::string& filename, const std::string& name)
			{
				std::string useName = name;

				if (!useName.empty() && this->TextureLoaded(useName))
				{
					this->GetTexture(useName)->IncUseCount();
					return this->GetTexture(useName)->GetID();
				}

				Core::ResourceManager* rm = Core::gEngine.GetSystem<Core::ResourceManager>();
				std::string fullPath = rm->GetResourcePath(filename);

				if (fullPath.empty())
				{
					gLogError << "Could not load texture: Texture file not found";
					return 0;
				}

				Texture* texture = new Texture();

				if (!texture->LoadFromFile(fullPath))
				{
					gLogError << "Could not load texture";
					delete[] texture;
					return 0;
				}

				texture->Setname(useName);
				unsigned int id = texture->GetID();
				this->_idTextureMap[id] = texture;
				this->_namedTextureMap[useName] = texture;
				this->_textureList.push_back(std::move(texture));

				return texture->GetID();
			}

			const unsigned int& TextureManager::LoadCubeMap(const std::vector<std::string>& filenames, const std::string& name)
			{
				std::string useName = name;

				if (!useName.empty() && this->TextureLoaded(useName))
				{
					this->GetTexture(useName)->IncUseCount();
					return true;
				}

				Core::ResourceManager* rm = Core::gEngine.GetSystem<Core::ResourceManager>();
				std::vector<std::string> fullPaths;
				fullPaths.resize(filenames.size());
				for (unsigned short i = 0; i < filenames.size(); i++)
				{
					fullPaths.at(i) = rm->GetResourcePath(filenames[i]);
				}


				Texture* texture = new Texture();
				if (!texture->LoadCubeMap(fullPaths))
				{
					gLogError << "Could not load cubemap";
					return false;
				}

				texture->Setname(useName);
				unsigned int id = texture->GetID();
				this->_idTextureMap[id] = texture;
				this->_namedTextureMap[useName] = texture;
				this->_textureList.push_back(std::move(texture));

				return true;
			}

			bool TextureManager::UpdateTexture(const unsigned int& id, const unsigned char* data)
			{
				auto it = this->_idTextureMap.find(id);

				if (it != this->_idTextureMap.end())
				{
					it->second->SetPixels(data);
					return true;
				}

				return false;
			}

			void TextureManager::DestroyTexture(const std::string& hash)
			{
				Texture* texture = this->GetTexture(hash);
				this->DestroyTexture(texture);
			}

			void TextureManager::DestroyTexture(Texture* texturePtr)
			{
				if (texturePtr != nullptr)
				{
					texturePtr->Unload();
					this->_idTextureMap.erase(this->_idTextureMap.find(texturePtr->GetID()));
					this->_namedTextureMap.erase(this->_namedTextureMap.find(texturePtr->GetName()));
					this->_textureList.remove_if([&texturePtr](Texture* tex){return texturePtr == tex; });
				}
			}

			void TextureManager::DestroyTexture(const unsigned int& id)
			{
				Texture* texture = this->GetTexture(id);
				this->DestroyTexture(texture);
			}


		}
	}

}