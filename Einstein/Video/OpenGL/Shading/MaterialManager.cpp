#include "MaterialManager.hpp"
#include "../../../Core/Engine.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			tbb::atomic<unsigned int> globalMaterialID = tbb::make_atomic<unsigned int>(100);

			MaterialManager::MaterialManager()
			{

			}

			bool MaterialManager::Initialise()
			{
				gLog << "Initialising Material Manager";


				gLog << "Material Manager initialized";
				
				return true;
			}

			void MaterialManager::Update()
			{

			}

			void MaterialManager::ShutDown()
			{
				while (!this->_materialList.empty())
				{
					auto it = this->_materialList.front();
					it->Destroy();
					this->_idMaterialMap.erase(this->_idMaterialMap.find(it->GetID()));
					this->_namedMaterialMap.erase(this->_namedMaterialMap.find(it->GetName()));
					this->_materialList.pop_front();
				}
			}

			Material* MaterialManager::GetMaterial(const unsigned int& id)
			{
				auto it = this->_idMaterialMap.find(id);
				if (it != this->_idMaterialMap.end()) return it->second;
				return nullptr;
			}

			Material* MaterialManager::GetMaterial(const std::string& name)
			{
				auto it = this->_namedMaterialMap.find(name);
				if (it != this->_namedMaterialMap.end()) return it->second;
				return nullptr;
			}

			bool MaterialManager::MaterialExists(const std::string& name)
			{
				auto it = this->_namedMaterialMap.find(name);
				if (it != this->_namedMaterialMap.end()) return true;
				else return false;
			}

			const unsigned int& MaterialManager::LoadMaterial(const rapidjson::Value& params)
			{
				Material* material = new Material();
				if (material->Load(params))
				{
					unsigned int id = globalMaterialID.fetch_and_increment();
					material->SetID(id);
					this->_idMaterialMap[id] = material;
					this->_namedMaterialMap[material->GetName()] = material;
					this->_materialList.push_back(std::move(material));

					return id;
				}

				return 0;
			}

			void MaterialManager::DestroyMaterial(const std::string& hash)
			{
				Material* material = this->GetMaterial(hash);
				this->DestroyMaterial(material);
			}

			void MaterialManager::DestroyMaterial(Material* materialPtr)
			{
				if (materialPtr != nullptr)
				{
					materialPtr->Destroy();
					this->_idMaterialMap.erase(materialPtr->GetID());
					this->_namedMaterialMap.erase(materialPtr->GetName());
					this->_materialList.remove_if([&materialPtr](Material* mat){return materialPtr == mat; });
				}
			}

			void MaterialManager::DestroyMaterial(const unsigned int& id)
			{
				Material* material = this->GetMaterial(id);
				this->DestroyMaterial(material);
			}

		}

	}

}