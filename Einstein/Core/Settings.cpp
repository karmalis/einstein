#include "Settings.hpp"

namespace Einstein {

	namespace Core {

		Settings::Settings() :
			Named("Settings")
		{

		}

		Settings::~Settings()
		{

		}

		bool Settings::load(std::string filename)
		{
			std::ifstream file;
			file.open(filename);

			if (!file.good())
			{
				throw new Util::Exception(" Could not open settings file");
			}

			std::string fileContents;
			file.seekg(0, std::ios::end);
			fileContents.resize(file.tellg());
			file.seekg(0, std::ios::beg);
			file.read(&fileContents[0], fileContents.size());
			file.close();

			this->_doc.Parse(fileContents.c_str());

			if (this->_doc.HasParseError())
			{
				throw new Util::Exception( "Could not parse JSON Settings file" );
			}

			return true;
		}

		bool Settings::hasKey(std::string key) const
		{
			return this->_doc.HasMember(key.c_str());
		}

		bool Settings::get(std::string key, bool value) const
		{
			if (!this->hasKey(key)) return value;
			if (!this->_doc[key.c_str()].IsBool()) return value;
			
			return this->_doc[key.c_str()].GetBool();
		}

		int Settings::get(std::string key, int value) const
		{
			if (!this->hasKey(key)) return value;
			if (!this->_doc[key.c_str()].IsInt()) return value;
			
			return this->_doc[key.c_str()].GetInt();
		}

		unsigned int Settings::get(std::string key, unsigned int value) const
		{
			if (!this->hasKey(key)) return value;
			if (!this->_doc[key.c_str()].IsUint()) return value;
			
			return this->_doc[key.c_str()].GetUint();
		}

		int64_t Settings::get(std::string key, int64_t value) const
		{
			if (!this->hasKey(key)) return value;
			if (!this->_doc[key.c_str()].IsInt64()) return value;
			
			return this->_doc[key.c_str()].GetInt64();
		}

		uint64_t Settings::get(std::string key, uint64_t value) const
		{
			if (!this->hasKey(key)) return value;
			if (!this->_doc[key.c_str()].IsUint64()) return value;

			return this->_doc[key.c_str()].GetUint64();
		}

		double Settings::get(std::string key, double value) const
		{
			if (!this->hasKey(key)) return value;
			if (!this->_doc[key.c_str()].IsDouble()) return value;

			return this->_doc[key.c_str()].GetDouble();
		}

		std::string Settings::get(std::string key, std::string value) const
		{
			if (!this->hasKey(key)) return value;
			if (!this->_doc[key.c_str()].IsString()) return value;
			
			return this->_doc[key.c_str()].GetString();
		}

	}

}