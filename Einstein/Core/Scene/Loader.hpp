#ifndef __LOADER_HPP__
#define __LOADER_HPP__

#include <rapidjson\document.h>

#include "../UScene.hpp"
#include "../Actors/Actor.hpp"
#include "../Actors/ActorComponent.hpp"

namespace Einstein {

	namespace Core {

		class Loader
		{
		public:

			using ComponentCreatorFunc = ActorComponent* (*)(const rapidjson::Value& params, Actor* actor);
			using ComponentCreatorMap = std::unordered_map < std::string, ComponentCreatorFunc > ;

			void LoadScene(const rapidjson::Document& doc, UniversalScene* scene);
			void AddComponentCreator(const std::string& name, const ComponentCreatorFunc& func);

		protected:

			ComponentCreatorMap _componentCreatorMap;

		};

	}

}

#endif