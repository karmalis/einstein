#include "RenderComponent.hpp"

#include "../../../Core/Logging/Logger.hpp"
#include "../../../Core/Tasking/EventQueue.hpp"
#include "../../../Core/System.hpp"
#include "../../../Core/Engine.hpp"
#include "../../../Core/UScene.hpp"
#include "../../../Core/Actors/Actor.hpp"
#include "../../../Util/Exception.hpp"

#include "Mesh.hpp"
#include "Light.hpp"

#include "../OGLRenderer.hpp"

#include <algorithm>

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			Core::ActorComponent* RenderComponent::Create(const rapidjson::Value& params, Core::Actor* actor)
			{
				if (!params.HasMember("rc_type"))
				{
					gLogError << "No Render Component Type set";
					return nullptr;
				}

				std::string rc_type = params["rc_type"].GetString();
				std::transform(rc_type.begin(), rc_type.end(), rc_type.begin(), ::tolower);
				if (rc_type == "mesh")
				{
					Core::Tasking::Event evt;
					evt.handler = [&params, actor](const Core::System* system) -> void {
						Core::ActorComponent* mesh = Mesh::Create(params, actor);
						actor->AddComponent(mesh);
						Core::gEngine.GetSystem<Core::UniversalScene>()->AddIndex(std::move(mesh));
					};
					Core::Tasking::EventQueue<RenderSystem>::instance().QueueEvent(evt);

					return nullptr;
				}
				else if (rc_type == "light")
				{
					Core::Tasking::Event evt;
					evt.handler = [&params, actor](const Core::System* system) -> void {
						Core::ActorComponent* light = Light::Create(params, actor);

						if (light != nullptr)
						{
							actor->AddComponent(light);
							Core::gEngine.GetSystem<Core::UniversalScene>()->AddIndex(std::move(light));
						}
						
					};
					Core::Tasking::EventQueue<RenderSystem>::instance().QueueEvent(evt);

					return nullptr;
				}
				else {
					throw new Util::Exception("Invalid RenderComponent Type");
				}
			}

			bool RenderComponent::Initialise(const rapidjson::Value& params)
			{
				params.FindMember("name");
				if (params.HasMember("name"))
				{
					this->_name = params["name"].GetString();
				}

				return true;
			}

		}

	}

}