#include "CameraComponent.hpp"

#include "../../../Core/Logging/Logger.hpp"
#include "../Actor.hpp"
#include "../../Engine.hpp"

namespace Einstein {

	namespace Core {

		CameraComponent::CameraComponent(std::string name) :
			ActorComponent(name),
			_isMain(false),
			_fov(45.0f),
			_near(0.1f),
			_far(1000.0f),
			_type(PERSPECTIVE)

		{

		}

		bool CameraComponent::Initialise(const rapidjson::Value& params)
		{
			if (!params.IsObject()) return false;

			if (params.HasMember("main"))
			{
				this->_isMain = params["main"].GetBool();
			}

			if (params.HasMember("type"))
			{
				this->_type = params["type"].GetInt();
			}

			if (params.HasMember("fov"))
			{
				this->_fov = params["fov"].GetDouble();
			}

			if (params.HasMember("near"))
			{
				this->_near = params["near"].GetDouble();
			}

			if (params.HasMember("far"))
			{
				this->_far = params["far"].GetDouble();
			}

			return true;
		}

		void CameraComponent::Update(const double& deltaTime)
		{
			if (this->_requiresUpdate || this->_owner->RequiresUpdate())
			{
				this->GetView(true);
				this->GetProjection(true);
				this->_requiresUpdate = false;
			}
		}

		void CameraComponent::Destroy()
		{

		}

		const mathfu::mat4& CameraComponent::GetView(const bool& recalc)
		{
			if (recalc)
			{
				this->_view = mathfu::mat4::LookAt(
					this->_owner->GetTarget(),
					this->_owner->GetDerivedPosition(),
					this->_owner->GetUp()
					);
			}

			return this->_view;
		}

		const mathfu::mat4& CameraComponent::GetProjection(const bool& recalc)
		{
			if (recalc)
			{
				int sWidth = gEngine.GetSettings()->get("width", 800);
				int sHeight = gEngine.GetSettings()->get("height", 600);

				switch (this->_type)
				{
				case PERSPECTIVE:
				{
					this->_projection = mathfu::mat4::Perspective(
						this->_fov,
						(float)sWidth / (float)sHeight,
						this->_near,
						this->_far,
						-1.0f
						);
				} break;
				case ORTHO:
				{
					this->_projection = mathfu::mat4::Ortho(
						0.0f,
						(float)sWidth,
						(float)sHeight,
						0.0f,
						this->_near,
						this->_far
						);
				} break;
				}
			}

			return this->_projection;
		}

		ActorComponent* CameraComponent::Create(const rapidjson::Value& params, Core::Actor* actor)
		{
			std::string name = actor->GetName();
			std::string type = params["type"].GetString();
			std::string actorID = std::to_string(actor->GetID());

			name.append("_").append(type).append("_").append(actorID);

			ActorComponent* result = new CameraComponent(name);

			if (result->Initialise(params["properties"]))
			{
				return std::move(result);
			}

			delete[] result;

			return nullptr;
		}

	}

}