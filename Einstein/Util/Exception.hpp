#ifndef __EXCEPTION_HPP__
#define __EXCEPTION_HPP__

#include <string>
#include <exception>

#include "Named.hpp"

namespace Einstein {

	namespace Util {

		class Exception : public std::exception, public Named
		{
		protected:

			Exception(std::string name, std::string description);

		public:

			Exception(std::string description = "");
			
			virtual const char* what() const throw();

		protected:

			std::string _description;

		};

	}

}

#endif