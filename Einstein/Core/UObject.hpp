#ifndef __U_OBJECT_HPP__
#define __U_OBJECT_HPP__

#include <map>
#include <list>
#include <memory>
#include <tbb\atomic.h>

#include "UIndexable.hpp"

namespace Einstein {

	namespace Core {

		class UIndexable;
		class UniversalObject;

		typedef UniversalObject* UObjectPtr;

		class UniversalObject : public UIndexable
		{
		public:

			UniversalObject(const std::string& name);
			~UniversalObject() {};

			typedef std::map<unsigned int, UObjectPtr> ChildrenIDMap;
			typedef std::map<std::string, UObjectPtr> ChildrenStringMap;
			typedef std::list<UObjectPtr > Children;

			virtual void Update(const double& deltaTime);
			virtual void Destroy();

			UObjectPtr GetParent() const { return this->_parent; }
			unsigned int GetID() const { return this->_ID; }
			std::string GetName() const { return this->_name; }
			UObjectPtr FindChild(const std::string& name);
			UObjectPtr FindChild(const unsigned int& id);
			Children GetChildren() { return this->_children; }

			void AddChild(const UObjectPtr& uObject);
			void SetParent(const UObjectPtr& uObject);
			void Detach();

			virtual std::string GetIndexPath() const;

		protected:

			UObjectPtr _parent;
			ChildrenIDMap _childrenIDMap;
			ChildrenStringMap _childrenStringMap;
			Children _children;
		};

	}

}

#endif