#ifndef __OGL_MESH_HPP__
#define __OGL_MESH_HPP__

#include <list>
#include <vector>
#include <rapidjson\document.h>

#include "../../Vertex.hpp"
#include "RenderComponent.hpp"

#define INVALID_OGL_VALUE 0xFFFFFFFF

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			class Mesh : public RenderComponent
			{
			public:

				virtual bool Initialise(const rapidjson::Value& params);
				virtual void Update(const double& deltaTime);
				virtual void Destroy();

				virtual void Render();

				static Core::ActorComponent* Create(const rapidjson::Value& params, Core::Actor* actor);

				void SetTextureID(const unsigned int& id);
				const unsigned int& GetTextureID();

				void SetNormalID(const unsigned int& id);
				const unsigned int& GetNormalID();

			protected:

				struct MeshEntry {
					MeshEntry();
					~MeshEntry();

					void Init(const std::vector<Vertex3f>& vertices, const std::vector<unsigned int>& indices);
					void Unload();

					unsigned int _indexCount;
					GLuint _vb;
					GLuint _ib;
				};

				Mesh(std::string name);

				bool LoadMesh(const rapidjson::Value& params);
				bool LoadShading(const rapidjson::Value& params);
				bool LoadTextures(const rapidjson::Value& params);
				bool LoadMaterial(const rapidjson::Value& params);

				std::vector<MeshEntry> _entries;
				unsigned int _textureID;
				unsigned int _normalID;
				unsigned int _materialID;
				
			};

		}

	}
}

#endif