#include "OGLSystem.hpp"

#include "../../Core/Engine.hpp"
#include "../../Core/UScene.hpp"
#include "../../Core/Scene/Loader.hpp"

#include "../../Core/Actors/Components/CameraComponent.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			RenderSystem::RenderSystem() :
				Core::System("OGLSystem"),
				_isInit(false),
				_lock(),
				_mutex()
			{
				
			}

			bool RenderSystem::Initialise()
			{
				Core::System::Initialise();
				gLog << " Initialising renderer ";

				if (!this->InitWindow())
				{
					gLogFatal << "Could not initialize window";
					return false;
				}

				gLog << "Setting up auto updater ";
				this->_engine->UpdateSystem(this, true, false);

				this->_materialManager.reset(new MaterialManager);
				if (!this->_materialManager->Initialise())
				{
					gLogFatal << "Could not initialise material manager";
					return false;
				}

				this->_textureManager.reset(new TextureManager);
				if (!this->_textureManager->Initialise())
				{
					gLogFatal << "Could not initialise texture manager";
					return false;
				}

				// Add the Render Component creator to the mix
				Core::UniversalScene* us = this->_engine->GetSystem<Core::UniversalScene>();
				if (us == nullptr)
				{
					gLogFatal << "Render System could not find universal scene";
					return false;
				}

				gLog << "Registering RenderComponent Creator";
				us->GetSceneLoader()->AddComponentCreator("RenderComponent", &RenderComponent::Create);
				//us->GetSceneLoader()->AddComponentCreator("RenderComponent", &DX11RenderComponent::Create);



				return true;
			}

			void RenderSystem::Update()
			{
				Core::System::Update();


				Core::Tasking::Event evt;
				while (Core::Tasking::EventQueue<RenderSystem>::instance().PollEvents(evt))
				{
					// Temporary, add an event processor
					// for each system
					evt.handler(this);
				}

				this->_mainRenderer->PreRender();
				RenderComponentList renderList = this->_engine->GetSystem<Core::UniversalScene>()->GetIndexObjectsByType<RenderComponent>();
				Core::CameraComponent* mainCamera = this->_engine->GetSystem<Core::UniversalScene>()->GetMainCamera();
				
				if (mainCamera == nullptr) return;

				this->_mainRenderer->RenderFrame(renderList, mainCamera);
				this->_mainRenderer->PostRender();
				
			}

			void RenderSystem::ShutDown()
			{
				gLog << " Shutting down renderer ";
				this->_textureManager->ShutDown();
				this->_materialManager->ShutDown();
				glfwTerminate();
				Core::System::ShutDown();
				//this->CleanD3D();
			}

			bool RenderSystem::InitWindow()
			{
				gLog << "Initialising OpenGL and main renderer";

				if (!glfwInit())
				{
#ifdef _DEBUG
					throw new Util::Exception("Could not initialise OpenGL");
#endif
					gLogError << "Could not initialise OpenGL";
					return false;
				}

				Core::Settings* settings = Core::gEngine.GetSettings();

				WindowParams wparam;

				wparam.width = settings->get("width", wparam.width);
				wparam.height = settings->get("height", wparam.height);
				wparam.fullscreen = settings->get("fullscreen", wparam.fullscreen);
				wparam.visible = true;
				wparam.name = "Einstein Prototype";

				// Get the default shaders
				const rapidjson::Value& graphicsData = settings->_doc["graphics"];
				if (!graphicsData.IsObject())
				{
					gLogError << "Could not load default shaders";
					return false;
				} 

				wparam.nullShader = graphicsData["shaders"]["null"].GetString();
				wparam.depthShader = graphicsData["shaders"]["depth"].GetString();
				wparam.geometryShader = graphicsData["shaders"]["geometry"].GetString();
				wparam.uiShader = graphicsData["shaders"]["ui"].GetString();
				wparam.pointLightShader = graphicsData["shaders"]["pointlight"].GetString();
				wparam.dirLightShader = graphicsData["shaders"]["dirlight"].GetString();
				wparam.spotlightShader = graphicsData["shaders"]["spotlight"].GetString();
				wparam.alphaShader = graphicsData["shaders"]["alpha"].GetString();
				wparam.combineShader = graphicsData["shaders"]["combine"].GetString();
				

				this->_mainRenderer = std::make_unique<Renderer>(std::move(wparam));
				
				if (!this->_mainRenderer->LoadWindow())
				{
					gLogError << "Could not load main window";
					glfwTerminate();
					return false;
				}

				if (!this->_mainRenderer->Load())
				{
					glfwTerminate();
					return false;
				}

				gLog << " Status: Using GLFW Version: " << glfwGetVersionString();
				gLog << " Status: Using OpenGL Version " << glfwGetWindowAttrib(this->_mainRenderer->_renderWindow, GLFW_CONTEXT_VERSION_MAJOR) << "."
					<< glfwGetWindowAttrib(this->_mainRenderer->_renderWindow, GLFW_CONTEXT_VERSION_MINOR) << " Revision: "
					<< glfwGetWindowAttrib(this->_mainRenderer->_renderWindow, GLFW_CONTEXT_REVISION);
				gLog << " Status: Using GLEW: " << glewGetString(GLEW_VERSION);

				gLog << "	Done";

				return true;


			}

			GLFWwindow* RenderSystem::GetMainWindowContext()
			{
				return this->_mainRenderer->_masterWindow;
			}

			void RenderSystem::Lock()
			{
				
				//while (!this->_lock.try_acquire(this->_mutex)) {}
				this->_mutex.lock();
			}

			void RenderSystem::Unlock()
			{
				//this->_lock.release();
				this->_mutex.unlock();
			}

			MaterialManager* RenderSystem::GetMaterialManager()
			{
				return this->_materialManager.get();
			}

			TextureManager* RenderSystem::GetTextureManager()
			{
				return this->_textureManager.get();
			}

		}

	}

}