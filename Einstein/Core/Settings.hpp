#ifndef __SETTINGS_HPP__
#define	__SETTINGS_HPP__

#include <rapidjson\document.h>
#include <rapidjson\reader.h>
#include <rapidjson\stringbuffer.h>

#include <string>
#include <fstream>
#include <ostream>
#include <sstream>

#include "../Util/Named.hpp"
#include "../Util/NonCopyable.hpp"
#include "../Util/Exception.hpp"

namespace Einstein {

	namespace Core {

		class Settings : public Util::Named, public Util::NonCopyable
		{
		public:

			Settings();
			~Settings();

			bool load(std::string filename);

			bool hasKey(std::string key) const;

			bool get(std::string key, bool value = false) const;
			int get(std::string key, int value = 0) const;
			unsigned int get(std::string key, unsigned int value = 0) const;
			int64_t get(std::string key, int64_t value = 0) const;
			uint64_t get(std::string key, uint64_t value = 0) const;
			double get(std::string key, double value = 0) const;
			std::string get(std::string key, std::string value = "") const;

			rapidjson::Document _doc;
			
		};

	}

}

#endif