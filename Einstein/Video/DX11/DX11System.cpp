#include "DX11System.hpp"
#include "../../Core/Engine.hpp"


#include "../../Core/UScene.hpp"
#include "../../Core/Scene/Loader.hpp"

namespace Einstein  {

	namespace Video {

		DX11System::DX11System() :
			Core::System("DX11System"),
			_screenWidth(DEFAULT_SCREEN_WIDTH),
			_screenHeight(DEFAULT_SCREEN_HEIGHT),
			_fullScreen(false),
			_isInit(false),
			_textureManager()
		{

		}

		bool DX11System::Initialise()
		{
			Core::System::Initialise();
			gLog << " Initialising renderer ";

			if (!this->InitWindow())
			{
				gLogFatal << "Could not initialize window";
				return false;
			}

			gLog << "Setting up auto updater ";
			this->_engine->UpdateSystem(this, true, false);

			// Add the Render Component creator to the mix
			Core::UniversalScene* us = this->_engine->GetSystem<Core::UniversalScene>();
			if (us == nullptr)
			{
				gLogFatal << "Render System could not find universal scene";
				return false;
			}

			gLog << "Registering RenderComponent Creator";
			us->GetSceneLoader()->AddComponentCreator("RenderComponent", &DX11RenderComponent::Create);

			return true;
		}

		void DX11System::Update()
		{
			Core::System::Update();

			MSG msg = { 0 };

			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);

				if (msg.message == WM_QUIT)
				{
					// Quit
					Core::Tasking::Channel::broadcast(Core::Engine::OnStop());
				}
			}
			else {
				this->RenderFrame();
			}
		}

		void DX11System::ShutDown()
		{
			gLog << " Shutting down renderer ";
			Core::System::ShutDown();
			this->CleanD3D();
		}

		bool DX11System::InitWindow()
		{
			Core::Settings* settings = this->_engine->GetSettings();
			
			this->_screenWidth = settings->get("width", this->_screenWidth);
			this->_screenHeight = settings->get("height", this->_screenHeight);
			this->_fullScreen = settings->get("fullscreen", this->_fullScreen);

			gLog << "Initialising Window";
			ZeroMemory(&this->_wc, sizeof(WNDCLASSEX));

			this->_wc.cbSize = sizeof(WNDCLASSEX);
			this->_wc.style = CS_HREDRAW | CS_VREDRAW;
			this->_wc.lpfnWndProc = DX11System::WindowProc;
			this->_wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
			this->_wc.lpszClassName = "Einstein";

			RegisterClassEx(&this->_wc);

			int top = 0;
			int left = 0;
			if (!this->_fullScreen)
			{
				const HWND hDesktop = GetDesktopWindow();
				RECT dr;
				GetWindowRect(hDesktop, &dr);

				top = (dr.bottom / 2) - (this->_screenHeight / 2);

				left = (dr.right / 2) - (this->_screenWidth / 2);
			}			

			RECT wr;
			wr.left = left;
			wr.top = top;
			wr.right = left + this->_screenWidth;
			wr.bottom = top + this->_screenHeight;
			AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);

			this->_hInstance = (HINSTANCE)::GetModuleHandle(NULL);
			this->_hWnd = CreateWindowEx(
				NULL,
				"Einstein",
				"Einstein Window",
				WS_OVERLAPPEDWINDOW,
				wr.left,
				wr.top,
				this->_screenWidth,
				this->_screenHeight,
				NULL,
				NULL,
				this->_hInstance,
				NULL
				);
			
			ShowWindow(this->_hWnd, 1);
			this->InitD3D(this->_hWnd);
			this->InitStates();

			this->_isInit = true;

			return true;
		}

		LRESULT CALLBACK DX11System::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
		{
			switch (message)
			{
			case WM_DESTROY:
			{
				Core::Tasking::Channel::broadcast(Core::Engine::OnStop());
				return 0;
			}
			}

			return DefWindowProc(hWnd, message, wParam, lParam);
		}

		bool DX11System::InitD3D(HWND hWnd)
		{
			gLog << "Initialising D3D";
			DXGI_SWAP_CHAIN_DESC scd;

			ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));

			scd.BufferCount = 1;
			scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			scd.OutputWindow = hWnd;
			scd.SampleDesc.Count = 4;
			scd.Windowed = true;

			UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
#if defined(_DEBUG)
			creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
			
			D3D11CreateDeviceAndSwapChain(
				NULL,
				D3D_DRIVER_TYPE_HARDWARE,
				NULL,
				creationFlags,
				NULL,
				NULL,
				D3D11_SDK_VERSION,
				&scd,
				&this->_swapChain,
				&this->_device,
				NULL,
				&this->_devcon
				);

			D3D11_TEXTURE2D_DESC texd;
			ZeroMemory(&texd, sizeof(texd));

			texd.Width = this->_screenWidth;
			texd.Height = this->_screenHeight;
			texd.ArraySize = 1;
			texd.MipLevels = 1;
			texd.SampleDesc.Count = 4;
			texd.Format = DXGI_FORMAT_D32_FLOAT;
			texd.BindFlags = D3D11_BIND_DEPTH_STENCIL;

			ID3D11Texture2D* depthBuffer;
			this->_device->CreateTexture2D(&texd, NULL, &depthBuffer);

			D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
			ZeroMemory(&dsvd, sizeof(dsvd));

			dsvd.Format = DXGI_FORMAT_D32_FLOAT;
			dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

			this->_device->CreateDepthStencilView(depthBuffer, &dsvd, &this->_zBuffer);
			depthBuffer->Release();

			ID3D11Texture2D* backBuffer;
			this->_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);

			this->_device->CreateRenderTargetView(backBuffer, NULL, &this->_backBuffer);
			backBuffer->Release();

			this->_devcon->OMSetRenderTargets(1, &this->_backBuffer, NULL);

			D3D11_VIEWPORT viewport;
			ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

			viewport.TopLeftX = 0;
			viewport.TopLeftY = 0;
			viewport.Width = this->_screenWidth;
			viewport.Height = this->_screenHeight;
			viewport.MinDepth = 0;
			viewport.MaxDepth = 1;

			this->_devcon->RSSetViewports(1, &viewport);

			this->InitPipeline();
			this->InitGraphics();

			this->_isInit = true;

			return true;
		}

		bool DX11System::InitStates()
		{
			D3D11_RASTERIZER_DESC rd;
			rd.FillMode = D3D11_FILL_SOLID;
			rd.CullMode = D3D11_CULL_BACK;
			rd.FrontCounterClockwise = FALSE;
			rd.DepthClipEnable = FALSE;
			rd.ScissorEnable = FALSE;
			rd.AntialiasedLineEnable = TRUE;
			rd.MultisampleEnable = FALSE;
			rd.DepthBias = 0;
			rd.DepthBiasClamp = 0.0f;
			rd.SlopeScaledDepthBias = 0.0f;

			this->_device->CreateRasterizerState(&rd, &this->_rSDefault);

			// set the changed values for wireframe mode
			rd.FillMode = D3D11_FILL_WIREFRAME;
			rd.AntialiasedLineEnable = TRUE;

			this->_device->CreateRasterizerState(&rd, &this->_rSWireframe);

			this->_devcon->RSSetState(this->_rSDefault);

			return true; 
		}

		bool DX11System::RenderFrame(void)
		{
			if (!this->_isInit) return false;

			D3DXMATRIX matView, matProjection;

			D3DXMatrixLookAtLH(
				&matView,
				&D3DXVECTOR3(0.0f, 0.0f, -10.0f), // Camera pos
				&D3DXVECTOR3(0.0f, 0.0f, 0.0f), // Camera look-at
				&D3DXVECTOR3(0.0f, 1.0f, 0.0f) // Camera up
				);

			D3DXMatrixPerspectiveFovLH(
				&matProjection,
				(FLOAT)D3DXToRadian(45),
				((float)this->_screenWidth / (float)this->_screenHeight),
				0.1f,
				1000.0f);

			
			this->_devcon->ClearRenderTargetView(this->_backBuffer, D3DXCOLOR(0.0f, 0.2f, 0.4f, 1.0f));
			this->_devcon->ClearDepthStencilView(this->_zBuffer, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

			// Do rendering stuff
			// "Download" all of the render components fromt he universal scene manager
			Core::UniversalScene* us = this->_engine->GetSystem<Core::UniversalScene>();

			if (us)
			{
				std::list<DX11RenderComponent*> renderList = us->GetIndexObjectsByType<DX11RenderComponent>();
				for (auto it : renderList)
				{
					it->Render(this->_devcon, matView, matProjection, this->_textureManager);
				}
			}

			
			// End Rendering
			
			this->_swapChain->Present(0, 0);

			return true;

		}

		void DX11System::CleanD3D()
		{
			this->_swapChain->Release();
			this->_backBuffer->Release();
			this->_device->Release();
			this->_devcon->Release();
		}

		void DX11System::InitGraphics()
		{
			gLog << "Initialising Graphics";
			
		}

		void DX11System::InitPipeline()
		{
			gLog << "Initialising Pipeline";
			
		}

		ID3D11Device* DX11System::GetD11Device() const
		{
			return this->_device;
		}

		const DX11TextureManager& DX11System::GetTextureManager()
		{
			return this->_textureManager;
		}

		DX11TextureManager* DX11System::GetTextureManagerPtr()
		{
			return &this->_textureManager;
		}
	}

}