#ifndef __CAMERA_COMPONENT_HPP__
#define __CAMERA_COMPONENT_HPP__

#include "../ActorComponent.hpp"
#include <mathfu\glsl_mappings.h>
#include <mathfu\matrix.h>

namespace Einstein {

	namespace Core {

		class CameraComponent : public ActorComponent
		{
		public:

			enum {
				PERSPECTIVE,
				ORTHO
			};

			virtual bool Initialise(const rapidjson::Value& params);
			virtual void Update(const double& deltaTime);
			virtual void Destroy();

			static Core::ActorComponent* Create(const rapidjson::Value& params, Core::Actor* actor);

			const bool& IsMain() { return this->_isMain; }
			const float& GetFOV() { return this->_fov; }
			const float& GetNear() { return this->_near; }
			const float& GetFar() { return this->_far; }

			void SetAsMain(const bool& value) { this->_isMain = value; this->_requiresUpdate = true; }
			void SetFOV(const float& value) { this->_fov = value; this->_requiresUpdate = true; }
			void SetNear(const float& value) { this->_near = value; this->_requiresUpdate = true; }
			void SetFar(const float& value) { this->_far = value; this->_requiresUpdate = true; }

			const mathfu::mat4& GetView(const bool& recalc = false);
			const mathfu::mat4& GetProjection(const bool& recalc = false);

			const mathfu::mat4& GetView() const { return this->_view; }
			const mathfu::mat4& GetProjection() const { return this->_projection; }

		protected:

			CameraComponent(std::string name);

			bool _isMain;
			float _fov;
			float _near;
			float _far;
			unsigned short _type;

			mathfu::mat4 _view;
			mathfu::mat4 _projection;

		};

	}

}

#endif