#include "Named.hpp"

namespace Einstein {

	namespace Util {

		Named::Named(std::string name) :
			_name(std::move(name))
		{

		}

		const std::string& Named::getName() const
		{
			return this->_name;
		}

		void Named::setName(std::string name)
		{
			this->_name = std::move(name);
		}

	}

}