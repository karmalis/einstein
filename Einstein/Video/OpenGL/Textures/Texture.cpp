#include "Texture.hpp"

#include "../../../Core/Logging/Logger.hpp"
#include <FreeImage.h>

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			Texture::Texture() :
				_boundedTexture(0),
				_useCount(0),
				_width(0),
				_height(0),
				_ID(0),
				_pixels(nullptr)
			{

			}

			Texture::Texture(
				const unsigned int& width,
				const unsigned int& height,
				const unsigned char* pixels,
				const unsigned int& w_width,
				const unsigned int& w_height) :
				_width(width),
				_height(height),
				_pixels(pixels)
			{
				this->Load(width, height, pixels, w_width, w_height);
			}

			Texture::~Texture()
			{
				if (this->_pixels != nullptr)
					delete[] this->_pixels;
			}

			bool Texture::Load(const unsigned int& width,
				const unsigned int& height,
				const unsigned char* pixels,
				const unsigned int& w_width,
				const unsigned int& w_height)
			{
				gLog << "Loading texture";

				GLenum error;
				glGenTextures(1, &this->_ID);
				glBindTexture(GL_TEXTURE_2D, this->_ID);

				unsigned int useWidth = width;
				unsigned int useHeight = height;

				if (w_width != 0 && w_height != 0)
				{
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w_width, w_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
					useWidth = w_width;
					useHeight = w_height;
				}
				else {
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
				}

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE_EXT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE_EXT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

				glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_BGRA, GL_UNSIGNED_BYTE, pixels);

				this->_width = useWidth;
				this->_height = useHeight;
				this->_pixels = pixels;

				this->_reload = false;

				this->Update(0);

				error = glGetError();

				if (error != GL_NO_ERROR)
				{
					gLogError << "Could not load texture: ";
					return false;
				}

				this->SetTextureType(TEXTURE_2D);

				gLog << "Texture loaded";

				return true;
			}

			bool Texture::LoadCubeMap(std::vector<std::string> filenames,
				GLenum image_format,
				GLint internal_format,
				GLint level,
				GLint border)
			{
				gLog << "Loading cubemap";

				GLenum error;

				GLenum types[6] = {
					GL_TEXTURE_CUBE_MAP_POSITIVE_X,
					GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
					GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
					GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
					GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
					GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
				};

				glGenTextures(1, &this->_ID);
				glBindTexture(GL_TEXTURE_CUBE_MAP, this->_ID);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 0);

				std::vector<std::string>::iterator it;
				unsigned short intIt = 0;

				for (it = filenames.begin(); it != filenames.end(); it++)
				{
					FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
					FIBITMAP *dib(0);
					BYTE* bits(0);

					fif = FreeImage_GetFileType(it->c_str(), 0);
					if (fif == FIF_UNKNOWN)
						fif = FreeImage_GetFIFFromFilename(it->c_str());

					if (fif == FIF_UNKNOWN)
					{
						gLogError << "Cannot load cubemap: Invalid file format";
						return false;
					}

					if (FreeImage_FIFSupportsReading(fif))
					{
						dib = FreeImage_Load(fif, it->c_str());
					}

					if (!dib)
					{
						gLogError << "Cannot load cubemap: Cannot read file: " << it->c_str();
						return false;
					}

					unsigned int bpp = FreeImage_GetBPP(dib);
					bits = FreeImage_GetBits(dib);
					unsigned int width = FreeImage_GetWidth(dib);
					unsigned int height = FreeImage_GetHeight(dib);
					unsigned char* pixels = bits;

					if (bits == 0 || width == 0 || height == 0)
					{
						gLogError << "Something went wrong while loading cubemap";
					}

					GLint gl_bpp_format;
					switch (bpp)
					{
					case 32:
						gl_bpp_format = GL_BGRA; break;
					default:
						gl_bpp_format = GL_BGR; break;
					}

					switch (fif)
					{
					case FIF_JPEG:
					{
						glTexImage2D(types[intIt++], level, GL_RGBA, width, height, border, gl_bpp_format, GL_UNSIGNED_BYTE, pixels);
					} break;
					default:
					{
						glTexImage2D(types[intIt++], level, GL_RGBA, width, height, border, gl_bpp_format, GL_UNSIGNED_BYTE, pixels);
					} break;
					}

					error = glGetError();

					if (error != GL_NO_ERROR)
					{
						gLogError << "Could not load cubemap. GL Error: " << error;
						return false;
					}


				}

				glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
				this->_width = 0;
				this->_height = 0;
				this->_pixels = nullptr;
				this->_reload = false;

				this->Update(0);

				error = glGetError();

				if (error != GL_NO_ERROR)
				{
					gLogError << "Could not load cubemap. GL Error: " << error;
					return false;
				}

				this->SetTextureType(CUBEMAP);

				gLog << "Cubemap loaded";

				return true;
			}

			bool Texture::Reload()
			{
				if (!this->_reload) return true;

				if (this->_pixels != nullptr)
				{
					glBindTexture(GL_TEXTURE_2D, this->_ID);
					glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, this->_width, this->_height, GL_BGRA, GL_UNSIGNED_BYTE, this->_pixels);
				}

				this->_reload = false;
				this->Update(0);

				GLenum error = glGetError();

				if (error != GL_NO_ERROR)
				{
					gLogError << "Could not reload texture. GL Error: " << error;
					return false;
				}

				return true;
			}

			void Texture::Unload()
			{
				gLog << "Unloading texture";

				glDeleteTextures(1, &this->_ID);
				if (this->_pixels != nullptr)
				{
					delete[] this->_pixels;
				}
			}

			void Texture::Update(double deltaTime)
			{
				if (this->_reload) this->Reload();
			}

			void Texture::Bind(GLenum texture, GLenum type)
			{
				if (texture != 0)
				{
					glActiveTexture(texture);
				}

				glBindTexture(type, this->_ID);

				GLenum error = glGetError();

				if (error != GL_NO_ERROR)
				{
					gLogError << "Could not bind texture. GL Error: " << error;
					return;
				}

				this->_unboundTexture = false;
			}

			void Texture::Unbind(GLenum type)
			{
				this->_unboundTexture = true;
				this->_boundedTexture = 0;
				glBindTexture(type, this->_boundedTexture);
			}

			bool Texture::LoadFromFile(
				const std::string& filename,
				GLenum image_format,
				GLint internal_format,
				GLint level,
				GLint border)
			{
				FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
				FIBITMAP *dib(0);
				BYTE* bits(0);

				fif = FreeImage_GetFileType(filename.c_str(), 0);

				if (fif == FIF_UNKNOWN)
					fif = FreeImage_GetFIFFromFilename(filename.c_str());

				if (fif == FIF_UNKNOWN)
				{
					gLogError << "Cannot load texture: invalid file type";
					return false;
				}

				if (FreeImage_FIFSupportsReading(fif))
				{
					dib = FreeImage_Load(fif, filename.c_str());
				}

				if (!dib)
				{
					gLogError << "Cannot load texture: cannot read texture";
					return false;
				}

				if (fif != FIF_JPEG)
				{

				}

				unsigned int bpp = FreeImage_GetBPP(dib);
				bits = FreeImage_GetBits(dib);
				this->_width = FreeImage_GetWidth(dib);
				this->_height = FreeImage_GetHeight(dib);

				if (bits == 0 || this->_width == 0 || this->_height == 0)
				{
					gLogError << "Something went wrong while reading texture file";
					return false;
				}

				this->_pixels = bits;

				GLenum error;
				glGenTextures(1, &this->_ID);
				glBindTexture(GL_TEXTURE_2D, this->_ID);

				GLint gl_bpp_format;
				switch (bpp)
				{
				case 32:
					gl_bpp_format = GL_BGRA; break;
				default:
					gl_bpp_format = GL_BGR; break;
				}

				switch (fif)
				{
				case FIF_JPEG:
				{
					glTexImage2D(GL_TEXTURE_2D, level, GL_RGBA, this->_width, this->_height, border, gl_bpp_format, GL_UNSIGNED_BYTE, this->_pixels);
					glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				} break;
				default:
				{
					glTexImage2D(GL_TEXTURE_2D, level, GL_RGBA, this->_width, this->_height, border, gl_bpp_format, GL_UNSIGNED_BYTE, NULL);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE_EXT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE_EXT);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
					glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, this->_width, this->_height, gl_bpp_format, GL_UNSIGNED_BYTE, this->_pixels);
				} break;
				}

				error = glGetError();
				if (error != GL_NO_ERROR)
				{
					gLogError << "Failed to load texture into OpenGL";
					return false;
				}

				this->SetTextureType(TEXTURE_2D);

				this->_reload = false;

				return true;
			}

		}

	}

}