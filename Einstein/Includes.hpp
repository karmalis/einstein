#ifndef __INCLUDES_HPP__
#define __INCLUDES_HPP__

#include <Windows.h>
#include <windowsx.h>

#pragma warning( disable : 4005 )

#include <D3D11.h>
#include <D3DX11.h>
#include <D3DX10.h>

#pragma warning( default : 4005 )

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")

#endif