#include "Material.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			Material::Material() :
				_ID(0),
				_name("untitled"),
				_emissive(0.0f, 0.0f, 0.0f, 0.0f),
				_ambient(0.0f, 0.0f, 0.0f, 0.0f),
				_diffuse(0.0f, 0.0f, 0.0f, 0.0f),
				_specular(0.0f, 0.0f, 0.0f, 0.0f),
				_shininess(0.0f)
			{}

			Material::~Material()
			{

			}

			bool Material::Load(const rapidjson::Value& params)
			{
				this->_name = params["name"].GetString();

				if (params.HasMember("diffuse"))
				{
					this->_diffuse = Colour4f(
						params["diffuse"][0].GetDouble(),
						params["diffuse"][1].GetDouble(),
						params["diffuse"][2].GetDouble(),
						params["diffuse"][3].GetDouble()
						);
				}

				if (params.HasMember("ambient"))
				{
					this->_diffuse = Colour4f(
						params["diffuse"][0].GetDouble(),
						params["diffuse"][1].GetDouble(),
						params["diffuse"][2].GetDouble(),
						params["diffuse"][3].GetDouble()
						);
				}

				if (params.HasMember("specular"))
				{
					this->_diffuse = Colour4f(
						params["diffuse"][0].GetDouble(),
						params["diffuse"][1].GetDouble(),
						params["diffuse"][2].GetDouble(),
						params["diffuse"][3].GetDouble()
						);
				}

				if (params.HasMember("emissive"))
				{
					this->_diffuse = Colour4f(
						params["diffuse"][0].GetDouble(),
						params["diffuse"][1].GetDouble(),
						params["diffuse"][2].GetDouble(),
						params["diffuse"][3].GetDouble()
						);
				}

				if (params.HasMember("shininess"))
				{
					this->_shininess = params["shininess"].GetDouble();
				}

				return true;
			}

			void Material::Destroy()
			{

			}

			const unsigned int Material::GetID()
			{
				return this->_ID;
			}

			const std::string& Material::GetName()
			{
				return this->_name;
			}

			const Colour4f& Material::GetEmissive()
			{
				return this->_emissive;
			}

			const Colour4f& Material::GetAmbient()
			{
				return this->_ambient;
			}

			const Colour4f& Material::GetDiffuse()
			{
				return this->_diffuse;
			}

			const Colour4f& Material::GetSpecular()
			{
				return this->_specular;
			}

			const float& Material::GetShininess()
			{
				return this->_shininess;
			}

			void Material::SetID(const unsigned int& id)
			{
				this->_ID = id;
			}

			void Material::SetName(const std::string& name)
			{
				this->_name = name;
			}

			void Material::SetEmissive(const Colour4f& colour)
			{
				this->_emissive = colour;
			}

			void Material::SetDiffuse(const Colour4f& colour)
			{
				this->_diffuse = colour;
			}

			void Material::SetSpecular(const Colour4f& colour)
			{
				this->_specular = colour;
			}

			void Material::SetAmbient(const Colour4f& colour)
			{
				this->_ambient = colour;
			}

			void Material::SetShininess(const float& value)
			{
				this->_shininess = value;
			}


		}

	}

}