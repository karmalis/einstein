#ifndef __E_ENGINE_HPP__
#define __E_ENGINE_HPP__

#include <memory>
#include <vector>
#include <map>

#include "Settings.hpp"
#include "Tasking\Channel.hpp"
#include "Tasking\TaskManager.hpp"
#include "../Util/Exception.hpp"


namespace Einstein {

	namespace App {
		class Application;
	}
	
	namespace Core {

		class System;

		class Engine
		{
		public:

			typedef std::unique_ptr<System> SystemPtr;
			typedef std::vector<SystemPtr> SystemList;
			typedef std::map<std::string, System*> SystemMap;

			Engine();

			void SetApplication(App::Application*);
			void AddSystem(System* system);

			template <typename T>
			T* GetSystem() const;

			void Remove(std::string systemName);
			void Remove(System* system);

			bool Initialise();
			void Run();
			void ShutDown();

			void UpdateSystem(System* system, bool repeating = false, bool background = false);

			struct OnStop {};
			
			void operator()(const OnStop&);

			Settings* GetSettings() { return &this->_settings;  }

		protected:

			void SetApplication(std::unique_ptr<App::Application>&& application);
			void AddSystem(std::unique_ptr<System>&& system);

			bool InitialiseSystems();
			void ShutDownSystems();

			SystemList _systems;
			SystemMap _systemLookup;
			Tasking::TaskManager _taskManager;
			
			Settings _settings;

			std::unique_ptr<App::Application> _application;


		};

		extern Engine gEngine;

	}

}

#include "Engine.inl"

#endif