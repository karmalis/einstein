#include "VideoTest.hpp"
#include "../../Core/Engine.hpp"

#include "../../Core/Tasking/Channel.hpp"
#include "../../Core/UScene.hpp"

namespace Einstein {

	namespace Test {

		VideoTest::VideoTest() :
			App::Application("VideoTest")
		{

		}

		bool VideoTest::Initialise()
		{
			this->_engine->UpdateSystem(this, true, false);
			return Core::System::Initialise();
		}

		void VideoTest::Update()
		{
			switch (this->_stateFlags)
			{
			case INIT:				
				this->ChangeStateFlags(RUNNING);
				break;
			case RUNNING:
				
				break;
			case IDLE:
				
				break;
			default: break;
			}

			Core::System::Update();
		}

		void VideoTest::ShutDown()
		{
			Core::System::ShutDown();
		}

	}

}