#include "Shader.hpp"

#include "../../Core/Logging/Logger.hpp"
#include "../../Core/Engine.hpp"
#include "../../Core/ResourceManager.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			GLShader::GLShader() :
				_program(0),
				_isLoaded(false)
			{
			}

			GLShader::~GLShader()
			{

			}

			bool GLShader::Load(std::string shaderName)
			{
				return this->Load(shaderName + ".vert", shaderName + ".frag");
			}

			bool GLShader::Load(std::string vertName, std::string fragName, std::string geomName)
			{
				if (!vertName.empty())
				{
					this->SetupShaderFromFile(GL_VERTEX_SHADER, vertName);
				}

				if (!fragName.empty())
				{
					this->SetupShaderFromFile(GL_FRAGMENT_SHADER, fragName);
				}

				if (!geomName.empty())
				{
					this->SetupShaderFromFile(GL_GEOMETRY_SHADER, geomName);
				}

				return this->LinkProgram();
			}

			bool GLShader::SetupShaderFromSource(GLenum type, std::string source)
			{
				this->Unload();

				if (this->_program == 0)
				{
					this->_program = glCreateProgram();
				}

				GLuint shader = glCreateShader(type);
				if (shader == 0)
				{
					gLogError << "Could not create shader: " << source;
					return false;
				}

				GLuint error = glGetError();
				if (error != GL_NO_ERROR)
				{
					gLogError << "Could not create shader program: " << source;
					return false;
				}

				//source = this->parseForIncludes(source);
				const char* srcPtr = source.c_str();
				int size = source.size();
				glShaderSource(shader, 1, &srcPtr, &size);
				glCompileShader(shader);

				GLint status = GL_FALSE;
				glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
				error = glGetError();
				if (error != GL_NO_ERROR || status == GL_FALSE)
				{
					gLogError << "Could not compile shader: " << source;
				}

				this->_shaders[type] = shader;

				return true;
			}

			bool GLShader::SetupShaderFromFile(GLenum type, std::string filename)
			{

				Core::ResourceManager* rm = Core::gEngine.GetSystem<Core::ResourceManager>();
				std::string fileFull = rm->GetResourcePath(filename);

				if (fileFull.empty())
				{
					gLogError << "Shader source file is empty: " << filename;
					return false;
				}

				std::string fileActual;
				std::ifstream ifs(fileFull);

				if (ifs)
				{
					ifs.seekg(0, std::ios::end);
					fileActual.resize(ifs.tellg());
					ifs.seekg(0, std::ios::beg);
					ifs.read(&fileActual[0], fileActual.size());
					ifs.close();

					return this->SetupShaderFromSource(type, fileActual);
				}
				else {
					gLogError << "Could not read shader file: " << filename;
					return false;
				}
			}

			bool GLShader::LinkProgram()
			{
				if (this->_shaders.empty())
				{
					gLogError << "Trying to link program with no shaders setup";
					return false;
				}

				if (this->_program != 0)
				{
					for (std::unordered_map<GLenum, GLuint>::const_iterator it = this->_shaders.begin(); it != this->_shaders.end(); ++it)
					{
						GLuint shader = it->second;
						if (shader) {
							glAttachShader(this->_program, shader);
							GLenum error = glGetError();
							this->_isLoaded = true;
						}
					}
					glLinkProgram(this->_program);

					this->Enable();
					this->setUniform1i("u_ColorMap", COLOR_TEXTURE_UNIT_INDEX);
					this->setUniform1i("u_NormalMap", NORMAL_TEXTURE_UNIT_INDEX);
					this->Disable();

					return true;
				}
				else {
					gLogError << "Trying to link an uninitialized program";
				}

				return false;
			}

			void GLShader::bindAttribute(GLuint location, std::string name)
			{
				glBindAttribLocation(this->_program, location, name.c_str());
			}

			void GLShader::Unload()
			{
				if (this->_isLoaded)
				{
					for (std::unordered_map<GLenum, GLuint>::const_iterator it = this->_shaders.begin(); it != this->_shaders.end(); ++it)
					{
						GLuint shader = it->second;
						if (shader) {
							glDetachShader(this->_program, shader);
							glDeleteShader(shader);
						}
					}

					if (this->_program != 0)
					{
						glDeleteProgram(this->_program);
						this->_program = 0;
					}
				}

				this->_isLoaded = false;
			}

			void GLShader::Enable()
			{
				if (this->_isLoaded)
				{
					GLenum error = glGetError();
					glUseProgram(this->_program);
				}
			}

			void GLShader::Disable()
			{
				if (this->_isLoaded)
				{
					glUseProgram(0);
				}
			}

			// set a texture reference
			void GLShader::setUniformTexture(std::string name, int textureTarget, GLint textureID, int textureLocation)
			{
				if (this->_isLoaded)
				{
					glActiveTexture(GL_TEXTURE0 + textureLocation);
					glEnable(textureTarget);
					glBindTexture(textureTarget, textureID);
					glDisable(textureTarget);
					this->setUniform1i(name, textureLocation);
					glActiveTexture(GL_TEXTURE0);
				}
			}

			// set a single uniform value
			void GLShader::setUniform1i(std::string name, int v1)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform1i(loc, v1);
				}
			}

			void GLShader::setUniform2i(std::string name, int v1, int v2)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform2i(loc, v1, v2);
				}
			}

			void GLShader::setUniform3i(std::string name, int v1, int v2, int v3)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform3i(loc, v1, v2, v3);
				}
			}

			void GLShader::setUniform4i(std::string name, int v1, int v2, int v3, int v4)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform4i(loc, v1, v2, v3, v4);
				}
			}

			void GLShader::setUniform1f(std::string name, float v1)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform1f(loc, v1);
				}
			}

			void GLShader::setUniform2f(std::string name, float v1, float v2)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform2f(loc, v1, v2);
				}
			}

			void GLShader::setUniform3f(std::string name, float v1, float v2, float v3)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform3f(loc, v1, v2, v3);
				}
			}

			void GLShader::setUniform4f(std::string name, float v1, float v2, float v3, float v4)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform4f(loc, v1, v2, v3, v4);
				}
			}

			void GLShader::setUniform1iv(std::string name, int* v, int count)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform1iv(loc, count, v);
				}
			}

			void GLShader::setUniform2iv(std::string name, int* v, int count)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform2iv(loc, count, v);
				}
			}

			void GLShader::setUniform3iv(std::string name, int* v, int count)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform3iv(loc, count, v);
				}
			}

			void GLShader::setUniform4iv(std::string name, int* v, int count)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform4iv(loc, count, v);
				}
			}

			void GLShader::setUniform1fv(std::string name, float* v, int count)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform1fv(loc, count, v);
				}
			}

			void GLShader::setUniform2fv(std::string name, float* v, int count)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform2fv(loc, count, v);
				}
			}

			void GLShader::setUniform3fv(std::string name, float* v, int count)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform3fv(loc, count, v);
				}
			}

			void GLShader::setUniform4fv(std::string name, float* v, int count)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniform4fv(loc, count, v);
				}
			}

			void GLShader::setUniformMatrix4f(std::string name, const GLfloat* matrix, GLboolean trans)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniformMatrix4fv(loc, 1, trans, matrix);
				}
			}

			void GLShader::setUniformMatrix4f(std::string name, const mathfu::mat4& matrix, GLboolean trans)
			{
				if (this->_isLoaded)
				{
					int loc = this->getUniformLocation(name);
					if (loc != -1) glUniformMatrix4fv(loc, 1, trans, &matrix[0]);
				}
			}

			const GLint& GLShader::getAttributeLocation(std::string name)
			{
				if (this->_isLoaded)
				{
					return glGetAttribLocation(this->_program, name.c_str());
				}

				return -1;
			}

			const GLint& GLShader::getUniformLocation(std::string name)
			{
				if (this->_isLoaded)
				{
					return glGetUniformLocation(this->_program, name.c_str());
				}

				return -1;
			}

			void GLShader::setAttribute1s(GLint location, short v1)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib1s(location, v1);
				}
			}

			void GLShader::setAttribute2s(GLint location, short v1, short v2)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib2s(location, v1, v2);
				}
			}

			void GLShader::setAttribute3s(GLint location, short v1, short v2, short v3)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib3s(location, v1, v2, v3);
				}
			}

			void GLShader::setAttribute4s(GLint location, short v1, short v2, short v3, short v4)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib4s(location, v1, v2, v3, v4);
				}
			}

			void GLShader::setAttribute1d(GLint location, double v1)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib1d(location, v1);
				}
			}

			void GLShader::setAttribute2d(GLint location, double v1, double v2)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib2d(location, v1, v2);
				}
			}

			void GLShader::setAttribute3d(GLint location, double v1, double v2, double v3)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib3d(location, v1, v2, v3);
				}
			}

			void GLShader::setAttribute4d(GLint location, double v1, double v2, double v3, double v4)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib4d(location, v1, v2, v3, v4);
				}
			}

			void GLShader::setAttribute1f(GLint location, float v1)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib1f(location, v1);
				}
			}

			void GLShader::setAttribute2f(GLint location, float v1, float v2)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib2f(location, v1, v2);
				}
			}

			void GLShader::setAttribute3f(GLint location, float v1, float v2, float v3)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib3f(location, v1, v2, v3);
				}
			}

			void GLShader::setAttribute4f(GLint location, float v1, float v2, float v3, float v4)
			{
				if (this->_isLoaded)
				{
					glVertexAttrib4f(location, v1, v2, v3, v4);
				}
			}

			void GLShader::setAttribute1fv(std::string name, float* v, GLsizei stride)
			{
				if (this->_isLoaded)
				{
					GLint location = getAttributeLocation(name);
					if (location != -1)
					{
						glVertexAttribPointer(location, 1, GL_FLOAT, GL_FALSE, stride, v);
						glEnableVertexAttribArray(location);
					}
				}
			}

			void GLShader::setAttribute2fv(std::string name, float* v, GLsizei stride)
			{
				if (this->_isLoaded)
				{
					GLint location = getAttributeLocation(name);
					if (location != -1)
					{
						glVertexAttribPointer(location, 2, GL_FLOAT, GL_FALSE, stride, v);
						glEnableVertexAttribArray(location);
					}
				}
			}

			void GLShader::setAttribute3fv(std::string name, float* v, GLsizei stride)
			{
				if (this->_isLoaded)
				{
					GLint location = getAttributeLocation(name);
					if (location != -1)
					{
						glVertexAttribPointer(location, 3, GL_FLOAT, GL_FALSE, stride, v);
						glEnableVertexAttribArray(location);
					}
				}
			}

			void GLShader::setAttribute4fv(std::string name, float* v, GLsizei stride)
			{
				if (this->_isLoaded)
				{
					GLint location = getAttributeLocation(name);
					if (location != -1)
					{
						glVertexAttribPointer(location, 4, GL_FLOAT, GL_FALSE, stride, v);
						glEnableVertexAttribArray(location);
					}
				}
			}

		}

	}

}