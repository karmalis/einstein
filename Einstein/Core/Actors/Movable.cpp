#include "Movable.hpp"

namespace Einstein {

	namespace Core {

		mathfu::Vector<float, 3> Movable::UNIT_X = mathfu::Vector<float, 3>(1.0f, 0, 0);
		mathfu::Vector<float, 3> Movable::UNIT_Y = mathfu::Vector<float, 3>(0, 1.0f, 0);
		mathfu::Vector<float, 3> Movable::UNIT_Z = mathfu::Vector<float, 3>(0, 0, 1.0f);


		Movable::Movable() :
			_currDistance(0.0f),
			_oreantation(0, 0, 0, 0),
			_currPitchAngle(0.0f),
			_currYawAngle(0.0f),
			_currRollAngle(0.0f),
			_calculateTargetByDirection(true),
			_position(0, 0, 0),
			_rotX(0),
			_rotY(0),
			_rotZ(0),
			_up(0, 1.0f, 0),
			_right(1.0f, 0, 0),
			_target(0, 0, 0),
			_viewDir(0, 0, 1.0f),
			_currMovementDirection(0, 0, 0),
			_scale(DEFAULT_SCALE, DEFAULT_SCALE, DEFAULT_SCALE),
			_movementType(FREE)
		{
		}

		const mathfu::Quaternion<float>& Movable::GetOrientation(bool calc)
		{
			if (calc)
			{

				mathfu::Quaternion<float> qx = mathfu::Quaternion<float>::FromAngleAxis(
					this->_rotX, mathfu::kAxisX3f
					);
				mathfu::Quaternion<float> qy = mathfu::Quaternion<float>::FromAngleAxis(
					this->_rotY, mathfu::kAxisY3f
					);
				mathfu::Quaternion<float> qz = mathfu::Quaternion<float>::FromAngleAxis(
					this->_rotZ, mathfu::kAxisZ3f
					);

				this->_oreantation = qx * qy * qz;
			}

			return this->_oreantation;
		}

		void Movable::Pitch(const float& angle)
		{
			this->Rotate(mathfu::kAxisX3f, angle);
		}

		void Movable::Yaw(const float& angle)
		{
			if (this->_movementType == FPS)
			{
				mathfu::Quaternion<float> q = mathfu::Quaternion<float>::FromAngleAxis(
					angle, mathfu::kAxisY3f
					);
				this->_oreantation = q * this->_oreantation;
			}
			else {
				this->Rotate(mathfu::kAxisY3f, angle);
			}
		}

		void Movable::Roll(const float& angle)
		{
			this->Rotate(mathfu::kAxisZ3f, angle);
		}

		void Movable::Rotate(const mathfu::Vector<float, 3>& axis, const float& angle)
		{
			switch (this->_movementType)
			{
			case FREE:
			case FPS:
			default:
			{
				mathfu::Quaternion<float> q = mathfu::Quaternion<float>::FromAngleAxis(
					angle * (M_PI / 180),axis
					);
				//q.Normalize();
				this->_oreantation = this->_oreantation * q;

			} break;
			case ORBITAL:
			{
				/* TODO: FILL IN */
			} break;
			}
		}

		void Movable::Move(const float& distance, const mathfu::Vector<float, 3>& direction)
		{
			switch (this->_movementType)
			{
			case FREE:
			default:
			{
				this->_position += this->_oreantation * (direction * distance);
			} break;
			case ORBITAL:
			{
				// TODO FILL UP
			} break;
			case FPS:
			{
				mathfu::Vector<float, 3> oldPosition = this->_position;
				mathfu::Vector<float, 3> newPosition = this->_position + this->_oreantation * (direction * distance);
				newPosition(3) = oldPosition(3);
				this->_position = newPosition;
			} break;
			}
		}
	
		void Movable::TriggerMove(const float& speed, const mathfu::Vector<float, 3>& direction)
		{
			this->_currMovementDirection = this->_currMovementDirection + direction;
			this->_currDistance = speed;
		}

		void Movable::TriggerStop(const mathfu::Vector<float, 3>& direction)
		{
			this->_currMovementDirection -= direction;
		}

		void Movable::AllStop()
		{
			this->_currMovementDirection = mathfu::Vector<float, 3>(0, 0, 0);
			this->_currDistance = 0;
			this->_currRollAngle = 0;
			this->_currYawAngle = 0;
			this->_currPitchAngle = 0;
		}

		void Movable::TriggerPitch(const float& angle)
		{
			this->_currPitchAngle += angle;
		}

		void Movable::TriggerYaw(const float& angle)
		{
			this->_currYawAngle += angle;
		}

		void Movable::TriggerRoll(const float& angle)
		{
			this->_currRollAngle += angle;
		}

		void Movable::UpdateMovable(const double& deltaTime)
		{
			if (this->_currMovementDirection.Length() != 0 && this->_currDistance != 0)
			{
				this->Move(this->_currDistance, this->_currMovementDirection);
			}

			if (this->_currPitchAngle != 0)
			{
				this->Pitch(this->_currPitchAngle);
			}

			if (this->_currRollAngle != 0)
			{
				this->Roll(this->_currRollAngle);
			}

			if (this->_currYawAngle != 0)
			{
				this->Yaw(this->_currYawAngle);
			}

			if (this->_calculateTargetByDirection)
			{
				this->_target = this->_position + this->_viewDir;
			}

			this->_right = (this->_oreantation * UNIT_X).Normalized();
			this->_up = (this->_oreantation * UNIT_Y).Normalized();
			this->_viewDir = (this->_oreantation * UNIT_Z).Normalized();

			mathfu::Vector<float, 3> eulerAngles = this->_oreantation.ToEulerAngles();
			this->_rotX = eulerAngles.x();
			this->_rotY = eulerAngles.y();
			this->_rotZ = eulerAngles.z();

		}

		void Movable::SetRotationX(const float& x)
		{
			this->_rotX = x;
			this->GetOrientation(true);
		}

		void Movable::SetRotationY(const float& y)
		{
			this->_rotY = y;
			this->GetOrientation(true);
		}

		void Movable::SetRotationZ(const float& z)
		{
			this->_rotZ = z;
			this->GetOrientation(true);
		}

	}

}