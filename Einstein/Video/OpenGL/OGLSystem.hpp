#ifndef __OGL_SYSTEM_HPP__
#define __OGL_SYSTEM_HPP__

#include "../../Core/System.hpp"

#include "../../Core/Tasking/EventQueue.hpp"

#include "OGLIncludes.hpp"
#include "OGLRenderer.hpp"
#include "Shading\MaterialManager.hpp"
#include "Textures\TextureManager.hpp"

#include <tbb\mutex.h>

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			class MaterialManager;
			class TextureManager;

			class RenderSystem : public Core::System
			{
			public:

				using ScopedLock = tbb::mutex::scoped_lock;

				RenderSystem();

				virtual bool Initialise();
				virtual void Update();
				virtual void ShutDown();

				GLFWwindow* GetMainWindowContext();

				void Lock();
				void Unlock();

				MaterialManager* GetMaterialManager();
				TextureManager* GetTextureManager();

			protected:

				bool _isInit;

				bool InitWindow();

				std::unique_ptr<Renderer> _mainRenderer;
				std::unique_ptr<MaterialManager> _materialManager;
				std::unique_ptr<TextureManager> _textureManager;

				ScopedLock _lock;
				tbb::mutex _mutex;

			};
		}
	}

}

#endif