#ifndef __OGL_SHADER_HPP__
#define __OGL_SHADER_HPP__

#include "OGLIncludes.hpp"
#include <mathfu\glsl_mappings.h>

#include <unordered_map>

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			class GLShader
			{
			public:

				GLShader();
				~GLShader();

				bool Load(std::string shaderName);
				bool Load(std::string vertName, std::string fragName, std::string geomName = "");

				void Unload();

				bool IsLoaded();
				void Enable();
				void Disable();

				void setUniformTexture(std::string name, int textureTarget, GLint textureID, int textureLocation);

				// set a single uniform value
				void setUniform1i(std::string name, int v1);
				void setUniform2i(std::string name, int v1, int v2);
				void setUniform3i(std::string name, int v1, int v2, int v3);
				void setUniform4i(std::string name, int v1, int v2, int v3, int v4);

				void setUniform1f(std::string name, float v1);
				void setUniform2f(std::string name, float v1, float v2);
				void setUniform3f(std::string name, float v1, float v2, float v3);
				void setUniform4f(std::string name, float v1, float v2, float v3, float v4);

				// set an array of uniform values
				void setUniform1iv(std::string name, int* v, int count = 1);
				void setUniform2iv(std::string name, int* v, int count = 1);
				void setUniform3iv(std::string name, int* v, int count = 1);
				void setUniform4iv(std::string name, int* v, int count = 1);

				void setUniform1fv(std::string name, float* v, int count = 1);
				void setUniform2fv(std::string name, float* v, int count = 1);
				void setUniform3fv(std::string name, float* v, int count = 1);
				void setUniform4fv(std::string name, float* v, int count = 1);

				void setUniformMatrix4f(std::string name, const GLfloat* matrix, GLboolean trans = GL_TRUE);
				void setUniformMatrix4f(std::string name, const mathfu::mat4& matrix, GLboolean trans = GL_FALSE);

				const GLint& getAttributeLocation(std::string name);

				void setAttribute1s(GLint location, short v1);
				void setAttribute2s(GLint location, short v1, short v2);
				void setAttribute3s(GLint location, short v1, short v2, short v3);
				void setAttribute4s(GLint location, short v1, short v2, short v3, short v4);

				void setAttribute1d(GLint location, double v1);
				void setAttribute2d(GLint location, double v1, double v2);
				void setAttribute3d(GLint location, double v1, double v2, double v3);
				void setAttribute4d(GLint location, double v1, double v2, double v3, double v4);

				void setAttribute1f(GLint location, float v1);
				void setAttribute2f(GLint location, float v1, float v2);
				void setAttribute3f(GLint location, float v1, float v2, float v3);
				void setAttribute4f(GLint location, float v1, float v2, float v3, float v4);

				void setAttribute1fv(std::string name, float* v, GLsizei stride = sizeof(float));
				void setAttribute2fv(std::string name, float* v, GLsizei stride = sizeof(float) * 2);
				void setAttribute3fv(std::string name, float* v, GLsizei stride = sizeof(float) * 3);
				void setAttribute4fv(std::string name, float* v, GLsizei stride = sizeof(float) * 4);

				void bindAttribute(GLuint location, std::string name);

				const GLuint& getProgram() { return this->_program; };
				const GLuint& getShader(GLenum type) { return this->_shaders[type]; };

				const GLint& getUniformLocation(std::string name);

			protected:


				bool LinkProgram();
				bool SetupShaderFromSource(GLenum type, std::string source);
				bool SetupShaderFromFile(GLenum type, std::string filename);

				GLuint _program;
				bool _isLoaded;
				std::unordered_map<GLenum, GLuint> _shaders;
				std::unordered_map<std::string, GLint> _uniformLocations;
			};

		}

	}

}

#endif