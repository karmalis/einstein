#ifndef __ENGINE_INL__
#define __ENGINE_INL__

#include "Engine.hpp"

namespace Einstein {

	namespace Core {

		template <typename T>
		T* Engine::GetSystem() const
		{
			T* result = nullptr;

			for (const auto& system : _systems)
			{
				result = dynamic_cast<T*>(system.get());

				if (result)
					return result;
			}

			throw new Util::Exception("System type not found");
		}

	}

}

#endif