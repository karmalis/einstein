#ifndef __ACTOR_COMPONENT__
#define __ACTOR_COMPONENT__

#include "../UIndexable.hpp"

#include <rapidjson\document.h>
#include <rapidjson\reader.h>
#include <string>

namespace Einstein {

	namespace Core {

		class Actor;

		class ActorComponent : public UIndexable
		{
		public:

			ActorComponent(const ActorComponent&) = delete;
			ActorComponent& operator= (const ActorComponent&) = delete;

			virtual bool Initialise(const rapidjson::Value& doc) = 0;
			virtual void Update(const double& deltaTime) = 0;
			virtual void Destroy() = 0;
			
			Actor* GetOnwer() { return this->_owner; }

			virtual std::string GetIndexPath() const;

			void SetOwner(Actor* actor) { this->_owner = actor; this->_requiresUpdate = true; }

		protected:

			ActorComponent(std::string name) : UIndexable(name) {}

			Actor* _owner;
		};

	}

}

#endif