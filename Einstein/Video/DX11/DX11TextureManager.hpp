#ifndef __DX11_TEXTURE_MANAGER_HPP__
#define __DX11_TEXTURE_MANAGER_HPP__

#include "../../Includes.hpp"

#include <rapidjson\document.h>
#include <unordered_map>
#include <tbb\atomic.h>

namespace Einstein {

	namespace Video {

		extern tbb::atomic<unsigned int> globalTextureID;

		class DX11TextureManager
		{
		public:

			DX11TextureManager();
			~DX11TextureManager();

			using TextureIDMap = std::unordered_map < std::string, unsigned int > ;
			using TextureMap = std::unordered_map < unsigned int, ID3D11ShaderResourceView* >;

			ID3D11ShaderResourceView* GetTexture(const unsigned int& id) const;
			ID3D11ShaderResourceView* GetTexture(const rapidjson::Value& params);
			ID3D11ShaderResourceView* GetTexture(const std::string& target);

			void DestroyTexture(const unsigned int& id);
			const unsigned int& LoadTexture(const rapidjson::Value& params);
			const unsigned int& LoadTexture(const std::string& target);

		protected:

			TextureMap _textureMap;
			TextureIDMap _textureIDMap;

			unsigned int _currTextureID;

		};

	}

}

#endif