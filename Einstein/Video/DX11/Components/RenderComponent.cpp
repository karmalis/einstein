#include "RenderComponent.hpp"

#include "../../../Core/Actors/Actor.hpp"

#include "../../../Core/Engine.hpp"
#include "../../../Core/ResourceManager.hpp"
#include "../DX11System.hpp"


namespace Einstein {

	namespace Video {

		DX11RenderComponent::DX11RenderComponent(std::string name) :
			ActorComponent(name)
		{

		}

		bool DX11RenderComponent::Initialise(const rapidjson::Value& params)
		{
			// Below is temprorary
			if (!params.HasMember("mesh")) return false;
			if (!params["mesh"].IsArray()) return false;

			for (rapidjson::Value::ConstValueIterator itr = params["mesh"].Begin(); itr != params["mesh"].End(); ++itr)
			{
				if (!this->LoadMesh(*itr))
				{
					gLogError << "Could not load mesh: " << (*itr)["name"].GetString();
				}
			}

			return true;
		}

		void DX11RenderComponent::Update(const double& deltaTime)
		{

		}

		void DX11RenderComponent::Render(ID3D11DeviceContext* context, const D3DXMATRIX& matView, const D3DXMATRIX& matProjection, const DX11TextureManager& textureManager)
		{
			for (const auto& singleMesh : this->_meshList)
			{
				singleMesh.Render(context, matView, matProjection, textureManager);
			}
		}

		void DX11RenderComponent::Destroy()
		{
			this->_meshList.clear();
		}

		Core::ActorComponent* DX11RenderComponent::Create(const rapidjson::Value& params, Core::Actor* actor)
		{
			std::string name = actor->GetName();
			std::string type = params["type"].GetString();
			std::string actorID = std::to_string(actor->GetID());
			
			name.append("_");
			name.append(type);
			name.append("_");
			name.append(actorID);

			//name.append("_").append(params['type'].GetString()).append("_");
			//name.append(std::to_string(actor->GetAndIncrementCID()));

			ActorComponent* result = new DX11RenderComponent(name);
			
			if (result->Initialise(params["properties"]))
			{
				return std::move(result);
			}



			return nullptr;

		}

		bool DX11RenderComponent::LoadMesh(const rapidjson::Value& meshParams)
		{
			std::string meshFile = meshParams["file"].GetString();

			DX11Mesh mesh;

			Core::ResourceManager* rm = Core::gEngine.GetSystem<Core::ResourceManager>();
			std::string meshPath = rm->GetResourcePath(meshFile);

			DX11System* rs = Core::gEngine.GetSystem<DX11System>();
			if (rs == nullptr)
			{
				gLogError << "Trying to load a mesh while rendering system not setup";
				return false;
			}

			Assimp::Importer importer;

			const aiScene* scene = importer.ReadFile(meshPath, 
				aiProcess_LimitBoneWeights |
				aiProcess_CalcTangentSpace |
				aiProcess_Triangulate |
				aiProcess_JoinIdenticalVertices |
				aiProcess_MakeLeftHanded |
				aiProcess_OptimizeMeshes |
				aiProcess_FlipUVs |
				aiProcess_GenSmoothNormals |
				aiProcess_FixInfacingNormals
				);

			if (!scene)
			{
				gLogError << "Could not load mesh file: " << meshFile << "which translated to " << meshPath;
				gLogError << "Importer reporded: " << importer.GetErrorString();
				return false;
			}

			bool hasTangents = true;
			bool hasNormals = true;

			for (unsigned int i = 0; i < scene->mNumMeshes; i++)
			{
				if (!scene->mMeshes[i]->HasNormals())
				{
					hasNormals = false;
				}

				if (!scene->mMeshes[i]->HasTangentsAndBitangents())
				{
					hasTangents = false;
				}
			}

			if (!hasNormals)
			{
				scene = importer.ApplyPostProcessing(aiPostProcessSteps::aiProcess_GenNormals);
			}

			if (!hasTangents)
			{
				scene = importer.ApplyPostProcessing(aiPostProcessSteps::aiProcess_CalcTangentSpace);
			}

			if (mesh.Load(scene, rs->GetD11Device(), meshPath))
			{
				DX11Mesh::ShaderProfile vShaderProfile, pShaderProfile;
				vShaderProfile.file = rm->GetResourcePath(meshParams["shader"]["vertex"]["file"].GetString());
				vShaderProfile.function = meshParams["shader"]["vertex"]["function"].GetString();
				vShaderProfile.profile = meshParams["shader"]["vertex"]["profile"].GetString();
				pShaderProfile.file = rm->GetResourcePath(meshParams["shader"]["pixel"]["file"].GetString());
				pShaderProfile.function = meshParams["shader"]["pixel"]["function"].GetString();
				pShaderProfile.profile = meshParams["shader"]["pixel"]["profile"].GetString();
				
				unsigned int textureOffset = sizeof(mathfu::Vector<float, 4>);
				unsigned int normalOffset = sizeof(mathfu::Vector<float, 4>) + sizeof(mathfu::Vector<float,4>);
				unsigned int tangentOffset = normalOffset + textureOffset;;

				// TODO: Write up dynamic input description
				D3D11_INPUT_ELEMENT_DESC ied[] =
				{
					{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
					{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
					{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
					{ "NORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
					{ "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
				};
				unsigned short iedSize = 5;

				if (!mesh.LoadShaders(vShaderProfile, pShaderProfile, ied, iedSize, rs->GetD11Device()))
				{
					gLogWarning << "Could not load shader for" << meshFile;
				}
			}
			else {
				gLogError << "Could not load mesh";
				return false;
			}

			if (meshParams.HasMember("texture"))
			{
				unsigned int id = rs->GetTextureManagerPtr()->LoadTexture(meshParams["texture"]);
				mesh._textureID = id;
			}

			mesh._owner = this;
			this->_meshList.push_back(std::move(mesh));

			return true;
		}

	}
}