#include "Loader.hpp"

#include "../Engine.hpp"
#include "../ResourceManager.hpp"

#include "../Logging/Logger.hpp"
#include "../Actors/Actor.hpp"

namespace Einstein {

	namespace Core {

		void Loader::LoadScene(const rapidjson::Document& doc, UniversalScene* scene)
		{
			
			ResourceManager* resourceManager = scene->_engine->GetSystem<ResourceManager>();

			//const rapidjson::Document& doc = resourceManager->GetJSONDocumentResource(sceneFile);
			//if (!doc.Empty()) return;

			if (!resourceManager) return;
			if (!doc.HasMember("actors")) return;

			const rapidjson::Value& actors = doc["actors"];
			for (rapidjson::Value::ConstValueIterator itr = actors.Begin(); itr != actors.End(); ++itr)
			{
				const rapidjson::Document& actorDoc = resourceManager->GetJSONDocumentResource(itr->GetString());
				
				if (!actorDoc.HasMember("properties")) continue;

				Actor* actor = nullptr;
				actor = Actor::create(actorDoc);
				if (actor == nullptr) continue;
				scene->GetSceneRoot()->AddChild(actor);
				scene->AddIndex(std::move(actor));

				if (!actorDoc.HasMember("components")) continue;
				
				const rapidjson::Value& components = actorDoc["components"];
				for (rapidjson::SizeType i = 0; i < components.Size(); i++)
				{
					std::string type = components[i]["type"].GetString();
					ComponentCreatorMap::const_iterator cit = this->_componentCreatorMap.find(type);
					if (cit == this->_componentCreatorMap.cend()) {
						gLogWarning << "No such component creator of " << type;
						continue;
					}

					ActorComponent* component = nullptr;

					try {
						component = cit->second(components[i], actor);
					}
					catch (Util::Exception e)
					{
						gLogError << "Exception when creating a component: " << e.what();
					}
					
					if (component != nullptr)
					{
						actor->AddComponent(component);
						scene->AddIndex(std::move(component));
					}
				}
				
			}
			
		}

		void Loader::AddComponentCreator(const std::string& name, const Loader::ComponentCreatorFunc& func)
		{
			this->_componentCreatorMap[name] = func;
		}

	}

}
