#ifndef __CORE_ACTIVE_HPP__
#define __CORE_ACTIVE_HPP__

#include <memory>
#include <functional>

#include <tbb\concurrent_queue.h>
#include <tbb\tbb_thread.h>

namespace Einstein {

	namespace Core {

		typedef std::function<void()> Callback;

		class Active;

		struct ActiveThreadExecutor
		{
			//ActiveThreadExecutor(Active* active);
			void operator()(Active* active);

			//Active* _active;
		};

		class Active
		{
		private:

			Active();

		public:
			
			typedef tbb::concurrent_queue<Callback> MessageQueue;

			Active(const Active&) = delete;
			Active& operator= (const Active&) = delete;

			~Active();

			static std::unique_ptr<Active> create();

			void send(Callback message);

		private:

			bool _isDone;
			MessageQueue _messageQueue;
			tbb::tbb_thread* _thread;

			ActiveThreadExecutor _atExecutor;

			friend struct ActiveThreadExecutor;
		};

	}

}


#endif