#ifndef _G_BUFFER_HPP__
#define _G_BUFFER_HPP__

#include "OGLIncludes.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			class GBuffer
			{
			public:

				enum {
					GBUFFER_TEXTURE_TYPE_POSITION,
					GBUFFER_TEXTURE_TYPE_DIFFUSE,
					GBUFFER_TEXTURE_TYPE_NORMAL,
					GBUFFER_NUM_TEXTURES
				};

				GBuffer();
				~GBuffer();

				bool Initialise(int width, int height);
				void Destroy();

				void PreRender();
				void BindForGeomPass();
				void BindForStencilPass();
				void BindForLightPass();
				void BindForSeperateMaterialPass();
				void BindForFinalPass();
				void BindFor2DElementPass();

				const GLuint& GetDepthTexture() { return this->_depthTexture; }
				const GLuint& GetForwardTexture() { return this->_frTexture;  }

			protected:

				GLuint _fbo;
				GLuint _textures[GBUFFER_NUM_TEXTURES];
				GLuint _depthTexture;
				GLuint _finalTexture;
				GLuint _frTexture;


			};

		}

	}

}

#endif