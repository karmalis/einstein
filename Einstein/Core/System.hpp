#ifndef __SYSTEM_HPP__
#define __SYSTEM_HPP__

#include "../Util/Named.hpp"
#include "../Util/NonCopyable.hpp"
#include "Settings.hpp"
#include "Logging\Logger.hpp"
#include "Tasking\Channel.hpp"
#include "Tasking\EventQueue.hpp"

namespace Einstein {

	namespace Core {

		class Engine;

		class System : public Util::Named, Util::NonCopyable
		{
		public:

			enum {
				IDLE = 0x0
			};

			struct StateChangeAction {
				StateChangeAction(const std::string& target) : _stateFlags(0x0), _target(target) {}
				StateChangeAction(const std::string& target, const short& flags) : _stateFlags(flags), _target(target) {}

				short _stateFlags;
				std::string _target;
			};

			struct StateChangeNotification {
				int _changeFrom;
				int _changeTo;
				std::string _systemName;

				StateChangeNotification(const int& from, const int& to, const std::string& name) : _changeFrom(from), _changeTo(to), _systemName(name) {}
			};

			friend class Engine;

			System(std::string name);

			virtual bool Initialise();
			virtual void Update();
			virtual void ShutDown();

			void ChangeStateFlags(const int& flags);
			int GetStateFlags() const { return this->_stateFlags; }

			void operator()(const StateChangeAction& state);

		protected:

			Tasking::Channel _channel;
			Engine* _engine;
			int _stateFlags;


		};
	}

}

#endif