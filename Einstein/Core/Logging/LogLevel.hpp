#pragma once

#include <iostream>

namespace Einstein {

	namespace Core {

		enum eLogLevel {
			E_DEBUG,
			E_MESSAGE,
			E_WARNING,
			E_ERROR,
			E_FATAL
		};

		void setLogLevel(eLogLevel level, bool enabled);
		bool logLevel(eLogLevel level);

	}

}

std::ostream& operator << (std::ostream& os, const Einstein::Core::eLogLevel& level);
