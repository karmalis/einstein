#ifndef __DX11_SYSTEM_HPP__
#define __DX11_SYSTEM_HPP__

#include "../../Includes.hpp"
#include "../../Core/System.hpp"
#include "Components\RenderComponent.hpp"
#include "DX11TextureManager.hpp"

#include <queue>

#define DEFAULT_SCREEN_WIDTH 800
#define DEFAULT_SCREEN_HEIGHT 600



namespace Einstein {

	namespace Video {

		class DX11System : public Core::System
		{
		public:

			using RenderQueue = std::queue < DX11RenderComponent > ;

			DX11System();

			virtual bool Initialise();
			virtual void Update();
			virtual void ShutDown();

			static LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

			// ID3D11Device is threadsafe so we can call it from anywhere we want
			// NOTE: ID3D11DeviceContext is not thread-safe
			ID3D11Device* GetD11Device() const;

			const DX11TextureManager& GetTextureManager();
			DX11TextureManager* GetTextureManagerPtr();

		protected:

			RenderQueue _renderQueue;

			HWND _hWnd;
			WNDCLASSEX _wc;
			HINSTANCE _hInstance;

			IDXGISwapChain* _swapChain;
			ID3D11Device* _device;
			ID3D11DeviceContext* _devcon;
			ID3D11RenderTargetView* _backBuffer;
			ID3D11DepthStencilView* _zBuffer;

			ID3D11RasterizerState* _rSDefault;    
			ID3D11RasterizerState* _rSWireframe;   

			DX11TextureManager _textureManager;

			int _screenWidth;
			int _screenHeight;
			bool _fullScreen;

			bool _isInit;

			bool InitWindow();
			bool InitD3D(HWND hWnd);
			bool InitStates();
			bool RenderFrame(void);
			void CleanD3D(void);

			void InitGraphics(void);
			void InitPipeline(void);

		};

	}

}



#endif