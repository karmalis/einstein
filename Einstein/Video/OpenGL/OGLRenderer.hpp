#ifndef __OGL_RENDERER_HPP__
#define __OGL_RENDERER_HPP__

#include "../../Core/Tasking/EventQueue.hpp"
#include "../../Core/Actors/Components/CameraComponent.hpp"

#include "OGLIncludes.hpp"
#include "GBuffer.hpp"
#include "Shader.hpp"

#define DEFAULT_SCREEN_WIDTH 800
#define DEFAULT_SCREEN_HEIGHT 600

#include "Components\RenderComponent.hpp"
#include "Components\Mesh.hpp"
#include "Components\Lights\Spot.hpp"
#include "Element2D.hpp"

#include <deque>

namespace Einstein {

	namespace Video {
		
		namespace OpenGL {

			struct WindowParams
			{
				int width;
				int height;
				bool fullscreen;
				bool visible;
				std::string name;
				GLFWwindow* master;

				std::string nullShader;
				std::string depthShader;
				std::string geometryShader;
				std::string uiShader;
				std::string pointLightShader;
				std::string spotlightShader;
				std::string dirLightShader;
				std::string alphaShader;
				std::string combineShader;

				WindowParams();

			};

			using RenderComponentList = std::list < RenderComponent* > ;
			using MeshComponentList = std::list < Mesh* > ;
			using SpotLightComponentList = std::list < SpotLight* > ;
			using Element2DRenderQueue = std::deque < Element2D >;

			class Renderer
			{
			public:

				Renderer();
				Renderer(const WindowParams& windowParams);
				~Renderer();

				bool Load();

				void PreRender();
				void RenderFrame(const RenderComponentList& renderList, const Core::CameraComponent* camera);
				void PostRender();

				void Destroy();

				const WindowParams& GetWindowParams();

			protected:

				bool LoadWindow();
				bool LoadDefaultShaders();
				bool LoadScreenElements();

				void ShadowMapPass(const MeshComponentList& meshList, const SpotLightComponentList& lightList, const Core::CameraComponent* camera);
				void GeometryPass(const MeshComponentList& meshList, const Core::CameraComponent* camera);
				void SeperateMaterialPass(const MeshComponentList& meshList, const Core::CameraComponent* camera);
				void SpotlightPass(const SpotLightComponentList& lightList, const Core::CameraComponent* camera);
				void PointlightPass();
				void DirectionallightPass();
				void CombinePasses();
				void Element2DPass();
				void FinalPass();

				GLFWwindow* _renderWindow;
				GLFWwindow* _masterWindow;

				WindowParams _windowParams;
				GBuffer _gBuffer;

				// Default Shaders
				GLShader _geometryShader;
				GLShader _nullShader;
				GLShader _pointLightShader;
				GLShader _dirLightShader;
				GLShader _spotLightShader;
				GLShader _uiShader;
				GLShader _combineShader;
				GLShader _alphaShader; // Used to "render" alpha transparency for custom shading

				Element2D _additionalPassElement;
				Element2DRenderQueue _2dQueue;

				friend class RenderSystem;
			};

		}

		

	}

}


#endif