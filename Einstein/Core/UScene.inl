#ifndef __U_SCENE_INL__
#define __U_SCENE_INL__

#include "UScene.hpp"

#include "../Util/Exception.hpp"

namespace Einstein {

	namespace Core {

		template<typename T>
		T* UniversalScene::GetIndexObject(const std::string& name) const
		{
			T* result = nullptr;

			for (const auto& index : _uIndexList)
			{
				if (index.get()->GetName() == name)
				{
					result = dynamic_cast<T*>(index.get());

					if (result)
						return result;
				}
				
			}

			throw new Util::Exception(name + " object could not be found or system");

			return result;
		}

		template <typename T>
		std::list<T*> UniversalScene::GetIndexObjectsByType() const
		{
			std::list<T*> result;

			for (const auto& index : _uIndexList)
			{
				T* res = dynamic_cast<T*>(index.get());
				if (res)
				{
					result.push_back(res);
				}
			}

			return result;
		}


	}

}

#endif