#ifndef __CHANNEL_QUEUE_HPP__
#define __CHANNEL_QUEUE_HPP__

#include <algorithm>
#include <functional>
#include <iostream>
#include <tbb/mutex.h>
#include <vector>
#include <utility>

namespace Einstein {

	namespace Core {

		namespace Tasking {

			class Channel;

			template <typename tMessage>
			class ChannelQueue
			{
			public:

				using Handler = std::function < void(const tMessage&) > ;
				using ScopedLock = tbb::mutex::scoped_lock;
				
				static ChannelQueue& instance();

				template <typename tHandler> void add(tHandler* handler);
				template <typename tHandler> void remove(tHandler* handler);

				void broadcast(const tMessage& message);

			protected:

				ChannelQueue();

				template <typename tHandler> Handler createHandler(tHandler* handler);

				tbb::mutex _mutex;
				std::vector<Handler> _handlers;
				std::vector<void*> _originalPtrs;

			};

		}

	}

}

#include "ChannelQueue.inl"

#endif