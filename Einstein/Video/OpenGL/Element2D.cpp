#include "Element2D.hpp"

#include <vector>

namespace Einstein {

	namespace Video {

		Element2D::Element2D() :
			_position(0.0f, 0.0f, 0.0f),
			_size(0.0f, 0.0f),
			_scale(0.0f, 0.0f),
			_data(nullptr),
			_textureID(0),
			_vb(0),
			_ib(0),
			_flip(false)
		{

		}

		bool Element2D::Load()
		{
			glGenBuffers(1, &this->_vb);
			glGenBuffers(1, &this->_ib);

			// Vertices
			std::vector<Vertex2fElement2D> Vertices;
			
			Vertex2fElement2D v1, v2, v3, v4;
			// Setting the position
			v1.position = this->_position;
			v2.position[0] = this->_position.x();
			v2.position[1] = this->_position.y() + this->_size.y();
			v3.position[0] = this->_position.x() + this->_size.x();
			v3.position[1] = this->_position.y() + this->_size.y();
			v4.position[0] = this->_position.x() + this->_size.x();
			v4.position[1] = this->_position.y();

			v1.position[2] = this->_position.z();
			v2.position[2] = this->_position.z();
			v3.position[2] = this->_position.z();
			v4.position[2] = this->_position.z();

			// Setting the texture coordinate
			v1.textureCoordinate = mathfu::Vector < float, 2 > {0, 0};
			v2.textureCoordinate = mathfu::Vector < float, 2 > {0, 1.0f};
			v3.textureCoordinate = mathfu::Vector < float, 2 > {1.0f, 1.0f};
			v4.textureCoordinate = mathfu::Vector < float, 2 > {1.0f, 0};
			// Setting the color
			mathfu::Vector<float, 4> color{ 1.0f, 1.0f, 1.0f, 1.0f };
			v1.color = color;
			v2.color = color;
			v3.color = color;
			v4.color = color;

			Vertices.push_back(v1); Vertices.push_back(v2);
			Vertices.push_back(v3); Vertices.push_back(v4);

			// Indices
			std::vector<unsigned int> Indices = {
				0, 1, 2, 0, 2, 3
			};

			glBindBuffer(GL_ARRAY_BUFFER, this->_vb);
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex2fElement2D) * Vertices.size(), &Vertices[0], GL_STATIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, this->_ib);
			glBufferData(GL_ARRAY_BUFFER, sizeof(unsigned int) * Indices.size(), &Indices[0], GL_STATIC_DRAW);

			return true;
		}

		void Element2D::Destroy()
		{
			glDeleteBuffers(1, &this->_vb);
			glDeleteBuffers(1, &this->_ib);
		}

		void Element2D::Render()
		{
			glEnable(GL_TEXTURE_2D);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glEnableVertexAttribArray(2);

			glBindBuffer(GL_ARRAY_BUFFER, this->_vb);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex2fElement2D), 0);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2fElement2D), (const GLvoid*)28);
			glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex2fElement2D), (const GLvoid*)12);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->_ib);

			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex2fElement2D), (const GLvoid*)28);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, this->GetTextureID());
			//glBindTexture(GL_TEXTURE_2D, 6);

			glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);

			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			glDisableVertexAttribArray(2);
			glDisable(GL_TEXTURE_2D);
		}

	}

}