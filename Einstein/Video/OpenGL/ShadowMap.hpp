#ifndef __SHADOW_MAP_HPP__
#define __SHADOW_MAP_HPP__

#include "OGLIncludes.hpp"
#include "Shader.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			class ShadowMap
			{
			public:

				ShadowMap();
				~ShadowMap();

				bool Initialise(const unsigned int& width = 512, const unsigned int& height = 512, std::string shader = "default/depth_shader");
				void Start();
				void Stop();
				void Destroy();

				const GLuint& GetShadowTexture() { return this->_shadowTexture; }
				const GLShader& GetDepthShader() { return this->_depthShader; }
				const unsigned int& GetWidth() { return this->_width; }
				const unsigned int& GetHeight() { return this->_height; }

			protected:

				GLuint _fbo;
				GLuint _shadowTexture;				
				GLShader _depthShader;
				unsigned int _width;
				unsigned int _height;
			};

		}

	}

}

#endif