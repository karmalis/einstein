#include "UIndexable.hpp"

#include "Logging\Logger.hpp"

namespace Einstein {

	namespace Core {

		tbb::atomic<unsigned int> globalCIID = tbb::make_atomic<unsigned int>(1);

		unsigned int GetUniqueID()
		{
			return globalCIID.fetch_and_increment();
		}

		UIndexable::UIndexable(const std::string& name) :
			_ID(GetUniqueID()),
			_name(name),
			_requiresUpdate(false)
		{
			gLogDebug << "Creating new Indexable: " << name << " => " << this->_ID;
		}



	}

}