#include "Spot.hpp"
#include "../../../../Core/Logging/Logger.hpp"
#include "../../../../Core/Actors/Actor.hpp"

namespace Einstein {

	namespace Video {
		
		namespace OpenGL {

			SpotLight::SpotLight(std::string name) :
				Light(name)
			{

			}

			bool SpotLight::Initialise(const rapidjson::Value& params)
			{

				if (params.HasMember("cutoff"))
				{
					this->_cutoff = params["cutoff"].GetDouble();
				}

				this->_shadowMap = std::make_unique<ShadowMap>();
				if (!this->_shadowMap->Initialise())
				{
					gLogError << "Could not initialise shadow map";
					return false;
				}

				return Light::Initialise(params);
			}

			void SpotLight::Update(const double& deltaTime)
			{
				Light::Update(deltaTime);
			}

			void SpotLight::Destroy()
			{
				if (this->_shadowMap)
				{
					this->_shadowMap->Destroy();
					this->_shadowMap.reset();
				}

				Light::Destroy();
			}

			void SpotLight::BindToShader(GLShader& shader)
			{
				shader.setUniform3f("u_SpotLight.Base.Color", this->_diffuse.x(), this->_diffuse.y(), this->_diffuse.z());
				shader.setUniform1f("u_SpotLight.Base.AmbientIntensity", Environment::ambientIntensity);
				mathfu::Vector<float, 3> dir = this->_owner->GetViewDirection();
				shader.setUniform3f("u_SpotLight.Direction", dir.x(), dir.y(), dir.z());
				shader.setUniform1f("u_SpotLight.Base.DiffuseIntensity", this->_diffuseIntensity);
				shader.setUniform1f("u_SpotLight.Cutoff", this->_cutoff);
				mathfu::Vector<float, 3> pos = this->_owner->GetDerivedPosition();
				shader.setUniform3f("u_SpotLight.Position", pos.x(), pos.y(), pos.z());
				shader.setUniform1f("u_SpotLight.Atten.Constant", this->_constantAtt);
				shader.setUniform1f("u_SpotLight.Atten.Linear", this->_linearAtt);
				shader.setUniform1f("u_SpotLight.Atten.Exp", this->_expAtt);
			}

		}
	}
	

}