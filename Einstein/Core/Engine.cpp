#include "Engine.hpp"
#include "System.hpp"
#include "../App/Application.hpp"

namespace Einstein {

	namespace Core {

		Engine::Engine()
		{
			Tasking::Channel::add<OnStop>(this);
		}

		void Engine::SetApplication(App::Application* application)
		{
			this->SetApplication(std::unique_ptr<App::Application>(application));
		}

		void Engine::SetApplication(std::unique_ptr<App::Application>&& application)
		{
			this->_application = std::move(application);

			if (this->_application)
			{
				this->_application->_engine = this;
				gLog << " Application set: " << this->_application->getName();
			}
		}

		void Engine::AddSystem(System* system)
		{
			this->AddSystem(std::unique_ptr<System>(system));
		}

		void Engine::AddSystem(std::unique_ptr<System>&& system)
		{
			auto it = this->_systemLookup.find(system->getName());

			if (it == this->_systemLookup.end())
			{
				system->_engine = this;
				this->_systemLookup[system->getName()] = system.get();
				this->_systems.emplace_back(std::move(system));
			}
			else {
				throw new Util::Exception(std::string(" Duplicate System is being added ") + system->getName());
			}
		}

		void Engine::Remove(std::string systemName)
		{
			System* s = nullptr;

			auto it = this->_systemLookup.find(systemName);
			if (it != this->_systemLookup.end())
			{
				s = it->second;
			}

			if (s) this->Remove(s);
			else {
				gLogError << "Trying to remove a system that is not registered " << systemName;
			}
		}

		void Engine::Remove(System* system)
		{
			auto it = std::remove_if(
				this->_systems.begin(),
				this->_systems.end(),
				[system](const SystemPtr& ptr) {
				return ptr.get() == system;
			}
			);

			if (it != this->_systems.end())
			{
				system->ShutDown();
				this->_systems.erase(it);

				for (auto jt = this->_systemLookup.begin(); jt != this->_systemLookup.end(); ++jt)
				{
					if (jt->second == system)
					{
						this->_systemLookup.erase(jt);
						break;
					}
						
				}
			}
		}

		bool Engine::Initialise()
		{
			this->_settings.load("settings.json");
			this->InitialiseSystems();
			return true;
		}

		void Engine::Run()
		{
			if (this->_application)
			{
				if (!this->_application->Initialise())
				{
					gLogFatal << "Failed to initialise application: " << this->_application->getName();
					return;
				}
			}
			else {

				gLogFatal << " Application not set ";
				return;
			}

			this->_taskManager.start();

			
		}

		void Engine::ShutDown()
		{
			this->_taskManager.stop();
			this->ShutDownSystems();
		}

		bool Engine::InitialiseSystems()
		{

			for (const auto& system : this->_systems)
			{
				if (!system->Initialise())
				{
					gLogError << " Could not initialise system: " << system->getName();
					return false;
				}
			}

			return true;
		}

		void Engine::ShutDownSystems()
		{
			if (this->_application)
				this->_application->ShutDown();

			for (const auto& system : this->_systems)
			{
				system->ShutDown();
			}

			this->_systems.clear();
			this->_systemLookup.clear();
		}

		void Engine::UpdateSystem(System* system, bool repeating, bool background)
		{
			assert(system);

			this->_taskManager.addWork([system]{ system->Update(); }, repeating, background);
		}

		void Engine::operator()(const OnStop&)
		{
			this->ShutDown();
		}

		Engine gEngine;
	}

}
