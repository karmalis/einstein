#include "ResourceManager.hpp"

#include "Engine.hpp"

#include "Scene\Loader.hpp"

namespace Einstein {

	namespace Core {

		ResourceManager::ResourceManager() :
			System("ResourceManager")
		{

		}

		bool ResourceManager::Initialise()
		{

			gLog << "Setting up Resources";

			this->_groupResourceMap[DATA_ACTORS] = std::vector<std::string>();
			this->_groupResourceMap[DATA_MODELS] = std::vector<std::string>();
			this->_groupResourceMap[DATA_SPRITES] = std::vector<std::string>();
			this->_groupResourceMap[DATA_SCRIPTS] = std::vector<std::string>();
			this->_groupResourceMap[DATA_SCENES] = std::vector<std::string>();
			this->_groupResourceMap[DATA_SHADERS] = std::vector<std::string>();
			this->_groupResourceMap[DATA_UI] = std::vector<std::string>();
			this->_groupResourceMap[DATA_OTHER] = std::vector<std::string>();
			this->_groupResourceMap[DATA_TEXTURES] = std::vector<std::string>();
			this->_groupResourceMap[DATA_SOUNDS] = std::vector<std::string>();

			this->_engine->UpdateSystem(this, true, true);
			this->ConstructFileSystem();
			this->ChangeStateFlags(INIT);

			gLog << "Resource loading complete";

			return System::Initialise();;
		}

		void ResourceManager::Update()
		{
			System::Update();

			switch (this->GetStateFlags())
			{
			case INIT:
				
				this->ChangeStateFlags(LOAD_RESOURCES);
				break;
			case LOAD_RESOURCES:
				this->PreLoadFiles();
			case IDLE:
			default:
				return;
			}
		}

		void ResourceManager::ShutDown()
		{
			System::ShutDown();

			gLog << "Clearing up resources";

			this->_resourceFileMap.clear();
			this->_groupResourceMap.clear();
			this->_loadedBinaryData.clear();
			this->_parsedJSONDocuments.clear();
		}

		const rapidjson::Document& ResourceManager::GetJSONDocumentResource(std::string name)
		{
			std::unordered_map<std::string, rapidjson::Document>::iterator it = this->_parsedJSONDocuments.find(name);
			if (it != this->_parsedJSONDocuments.end())
			{
				return it->second;
			}

			return nullptr;

		}

		const char* ResourceManager::GetBinaryDataResource(std::string name)
		{
			std::unordered_map<std::string, const char* >::iterator it = this->_loadedBinaryData.find(name);
			if (it != this->_loadedBinaryData.end())
			{
				return it->second;
			}

			return nullptr;

		}

		void ResourceManager::ConstructFileSystem()
		{
			gLog << "	Constructing filesystem";

			this->ConstructSingleElementFS(DATA_ACTORS);
			this->ConstructSingleElementFS(DATA_MODELS);
			this->ConstructSingleElementFS(DATA_SPRITES);
			this->ConstructSingleElementFS(DATA_SCRIPTS);
			this->ConstructSingleElementFS(DATA_SCENES);
			this->ConstructSingleElementFS(DATA_SHADERS);
			this->ConstructSingleElementFS(DATA_UI);
			this->ConstructSingleElementFS(DATA_OTHER);
			this->ConstructSingleElementFS(DATA_TEXTURES);
			this->ConstructSingleElementFS(DATA_SOUNDS);

			gLog << "	Finished constructing filesystem";

			this->ChangeStateFlags(IDLE);
		}

		void ResourceManager::ConstructSingleElementFS(std::string dataElement)
		{
			gLog << "		Constructing element: " << dataElement;
			std::string folders;
			Settings* settings = this->_engine->GetSettings();
			std::vector<std::string> fields;

			using namespace rapidjson;

			const Value& dataFolders = settings->_doc["data"];
			
			if (settings->_doc["data"].IsObject())
			{
				if (!settings->_doc["data"].HasMember(dataElement.c_str())) return;
				
				if (settings->_doc["data"][dataElement.c_str()].IsArray())
				{
					for (Value::ConstValueIterator itr = settings->_doc["data"][dataElement.c_str()].Begin(); itr != settings->_doc["data"][dataElement.c_str()].End(); itr++)
					{
						fields.push_back(itr->GetString());
					}
				}
				else if (settings->_doc["data"][dataElement.c_str()].IsString())
				{
					fields.push_back(settings->_doc["data"][dataElement.c_str()].GetString());
				}
			}

			while (!fields.empty())
			{
				this->ScanFolder(fields.back(), dataElement);
				fields.pop_back();
			}

			gLog << "		Done";

		}

		void ResourceManager::ScanFolder(std::string folder, std::string data)
		{
			gLog << "			Parsing " << folder;
			namespace fs = std::tr2::sys;

			try {

				fs::path fPath = fs::initial_path<fs::path>() / fs::path(folder.c_str());

				for (auto it = fs::recursive_directory_iterator(fPath); it != fs::recursive_directory_iterator(); ++it)
				{
					fs::path file = it->path();

					if (!fs::is_directory(file) && fs::is_regular_file(file))
					{
						std::string tempDelimeter = folder;
						size_t delPos = 0;
						while ((delPos = tempDelimeter.find("/", delPos)) != std::string::npos)
						{
							tempDelimeter.replace(delPos, std::string("/").length(), "\\");
							delPos += std::string("\\").length();
						}

						std::string tempFolderFile = file.directory_string();
						size_t pos = tempFolderFile.find(tempDelimeter);
						tempFolderFile.erase(0, pos + tempDelimeter.length());

						delPos = 0;
						while ((delPos = tempFolderFile.find("\\", delPos)) != std::string::npos)
						{
							tempFolderFile.replace(delPos, std::string("\\").length(), "/");
							delPos += std::string("/").length();
						}

						this->_resourceFileMap[tempFolderFile] = file.string();
						this->_groupResourceMap.find(data)->second.push_back(tempFolderFile);
					}
				}
			}
			catch (const fs::filesystem_error& e) {
				gLogError << " Resource System => unexpected file parse error" << e.what();
			}

			gLog << "			Done ";

		}

		void ResourceManager::PreLoadFiles(std::string sceneFileName)
		{
			gLog << "Preloading resources";
			if (sceneFileName.empty())
			{
				for (std::unordered_map<std::string, std::string>::iterator it = this->_resourceFileMap.begin(); it != this->_resourceFileMap.end(); ++it)
				{
					this->PreLoadSingleFile(it->first, it->second);
				}
			}
			else {
				// Pre load items from scene file
				// TODO Parse the scene file
			}
			this->ChangeStateFlags(IDLE);
			Einstein::Core::Tasking::Channel::broadcast(Core::UniversalScene::LoadScene{ "test_scene.json" }); // TEMPORARY, init scene loading from scripting
			gLog << "Resource preloading complete";
		}

		void ResourceManager::PreLoadSingleFile(std::string name, std::string file)
		{
			gLog << "	Loading file: " << name << " path: " << file;
			// Read the file first
			std::ifstream fileActual;
			fileActual.open(file);

			if (!fileActual.good())
			{
				gLogError << "Could not open file for reading: " << file;
				return;
			}

			std::string contents;
			fileActual.seekg(0, std::ios::end);
			contents.resize(fileActual.tellg());
			fileActual.seekg(0, std::ios::beg);
			fileActual.read(&contents[0], contents.size());
			fileActual.close();

			std::string tempName = name;
			std::transform(tempName.begin(), tempName.end(), tempName.begin(), ::tolower);
			if (tempName.find(".json") != std::string::npos)
			{
				// Pre load even if the item is already loaded (re-load simply)
				rapidjson::Document doc;
				doc.Parse(contents.c_str());
				if (doc.HasParseError())
				{
					gLogError << "Could not parse json document: " << file << " error: " << doc.GetParseError();
					return;
				}

				this->_parsedJSONDocuments[name] = std::move(doc);
			}
			else {
				//this->_loadedBinaryData[name] = std::move(contents.c_str()); TEMPORARY DISABLED
			}

			gLog << "	Loading done";
		}

		const std::string& ResourceManager::GetResourcePath(const std::string& name)
		{
			std::unordered_map<std::string, std::string>::const_iterator cit = this->_resourceFileMap.find(name);
			if (cit != this->_resourceFileMap.cend())
			{
				return cit->second;
			}

			return "";
		}

	}

}