#ifndef __OG_RENDER_COMPONENT_HPP__
#define __OG_RENDER_COMPONENT_HPP__

#include <mathfu\matrix.h>
#include "../OGLIncludes.hpp"
#include "../../../Core/Actors/ActorComponent.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			class RenderComponent : public Core::ActorComponent
			{
			public:

				virtual bool Initialise(const rapidjson::Value& params);
				virtual void Update(const double& deltaTime) {}
				virtual void Destroy() {}

				virtual void Render() = 0;

				static Core::ActorComponent* Create(const rapidjson::Value& params, Core::Actor* actor);

				const bool& IsVisible() { return this->_visible; }

			protected:

				RenderComponent(std::string name) : Core::ActorComponent(name), _visible(true) {};

				bool _visible;

			};

		}

	}
}

#endif