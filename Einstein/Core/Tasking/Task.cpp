#include "Task.hpp"
#include "../Logging/Logger.hpp"


#include "../../Util/Exception.hpp"

namespace Einstein {

	namespace Core {

		namespace Tasking {

			WrappedTask::WrappedTask()
			{

			}

			WrappedTask::WrappedTask(Task task, bool repeating, bool background, unsigned short priority) :
				_unwrappedTask{ std::move(task) },
				_isRepeating(repeating),
				_isBackground(background),
				_priority(priority)
			{

			}

			void WrappedTask::operator()() const {

				try {
					this->_unwrappedTask();
				}
				catch (const Util::Exception& ex) {
					gLogError << "Wrapped task execution gave an exception: " << ex.what();
				}
				catch (...) {
					gLogError << "Uknown exception";
				}

			}

			bool WrappedTask::isRepeating() const
			{
				return this->_isRepeating;
			}

			bool WrappedTask::isBackground() const
			{
				return this->_isBackground;
			}

			void WrappedTask::setPriority(unsigned short priority)
			{
				this->_priority = priority;
			}

			unsigned short WrappedTask::getPriority() const
			{
				return this->_priority;
			}

			void WrappedTask::setRepeating(bool enabled) {
				this->_isRepeating = enabled;
			}

			void WrappedTask::setBackground(bool enabled) {
				this->_isBackground = enabled;
			}

			bool CompareWrappedTasks::operator() (const WrappedTask& t1, const WrappedTask& t2) const
			{
				return t1._priority > t2._priority;
			}

			WrappedTask make_wrapped(
				Task task,
				bool repeating,
				bool background,
				unsigned short priority
				)
			{
				return WrappedTask{ task, repeating, background, priority };
			}

		}

	}

}