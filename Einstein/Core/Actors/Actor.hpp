#ifndef __ACTOR_HPP__
#define __ACTOR_HPP__

#include "../UObject.hpp"
#include "ActorComponent.hpp"
#include "Movable.hpp"

#include <rapidjson\document.h>
#include <rapidjson\reader.h>
#include <string>
#include <vector>
#include <mathfu\glsl_mappings.h>

namespace Einstein {

	namespace Core {

		class ActorComponent;

		class Actor : public UniversalObject, public Movable
		{
		public:

			using ActorComponentPtr = ActorComponent*;
			using ComponentList = std::vector < ActorComponentPtr > ;

			Actor(const Actor&) = delete;
			Actor& operator= (const Actor&) = delete;

			static Actor* create(const rapidjson::Document& doc);
			static Actor* create(const std::string& jsonString);

			virtual bool Initialise(const rapidjson::Document& doc);
			virtual void Update(const double& deltaTime);
			virtual void Destroy();

			const unsigned int& GetAndIncrementCID();

			void AddComponent(ActorComponent* ptr);
			void RemoveComponent(ActorComponent* ptr);

			const mathfu::Vector<float, 3>& GetDerivedPosition();
			const mathfu::Quaternion<float>& GetDerivedOreantation();
			const mathfu::Vector<float, 3>& GetDerivedScale();

			const mathfu::mat4& GetModelMatrix() const { return this->_modelMatrix; }

		protected:

			Actor(std::string name);

			unsigned int _componentID;
			ComponentList _components;

			void UpdateModelMatrix();

			mathfu::mat4 _modelMatrix;

		};

	}

}

#endif