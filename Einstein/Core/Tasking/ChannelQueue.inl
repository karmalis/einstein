#ifndef __CHANNEL_QUEUE_INL__
#define __CHANNEL_QUEUE_INL__

#include "ChannelQueue.hpp"

namespace Einstein {

	namespace Core {

		namespace Tasking {

			template <typename tMessage>
			ChannelQueue<tMessage>& ChannelQueue<tMessage>::instance()
			{
				static ChannelQueue instance;
				return instance;
			}

			template <typename tMessage>
			template <typename tHandler>
			void ChannelQueue<tMessage>::add(tHandler* handler)
			{
				ScopedLock lock(this->_mutex);

				this->_handlers.push_back(this->createHandler(handler));
				this->_originalPtrs.push_back(handler);
			}

			template <typename tMessage>
			template <typename tHandler>
			void ChannelQueue<tMessage>::remove(tHandler* handler)
			{
				ScopedLock lock(this->_mutex);

				auto it = std::find(this->_originalPtrs.begin(), this->_originalPtrs.end(), handler);

				if (it == this->_originalPtrs.end())
					throw std::runtime_error("Tried to remove a handler that is not in the list");

				auto idx = (it - this->_originalPtrs.begin());

				this->_handlers.erase(this->_handlers.begin() + idx);
				this->_originalPtrs.erase(it);
			}

			template <typename tMessage>
			void ChannelQueue<tMessage>::broadcast(const tMessage& message)
			{
				std::vector<Handler> localVector(this->_handlers.size());

				{
					ScopedLock lock(this->_mutex);
					localVector = this->_handlers;
				}

				for (const auto& handler : localVector)
				{
					handler(message);
				}
			}

			template<typename tMessage>
			ChannelQueue<tMessage>::ChannelQueue()
			{

			}

			template <typename tMessage>
			template <typename tHandler>
			typename ChannelQueue<tMessage>::Handler ChannelQueue<tMessage>::createHandler(tHandler* handler)
			{
				return [handler](const tMessage& message) { (*handler)(message); };
			}
		}

	}

}

#endif