#ifndef __RANDOM_HPP__
#define __RANDOM_HPP__

#include <string>

namespace Einstein {

	namespace Util {

		class Random {

		public:

			static std::string String(size_t length);
			
			static int Int(const int& min, const int& max);
			
			static unsigned int UInt(const unsigned int& min, const unsigned int& max);
			
			static float Float(const float& min, const float& max);
			
			static double Double(const double& min, const float& max);


		};

	}

}

#endif