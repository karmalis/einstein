#ifndef _U_INDEXABLE_HPP__
#define _U_INDEXABLE_HPP__

#include <string>
#include <tbb\atomic.h>

namespace Einstein {

	namespace Core {

		class UniversalScene;

		extern tbb::atomic<unsigned int> globalCIID;

		class UIndexable
		{
		public:

			friend class UniversalScene;
			
			UIndexable(const std::string& name);
			virtual ~UIndexable() {};

			unsigned int GetID() const { return this->_ID; }
			std::string GetName() const { return this->_name; }

			const bool& RequiresUpdate() { return this->_requiresUpdate; }

			virtual std::string GetIndexPath() const = 0;

		protected:

			unsigned int _ID;
			std::string _name;

			UniversalScene* _scene;
			bool _requiresUpdate;

		};

	}

}

#endif