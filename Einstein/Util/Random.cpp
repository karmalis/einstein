#include "Random.hpp"

#include <vector>
#include <chrono>
#include <random>
#include <algorithm>

namespace Einstein {

	namespace Util {

		
		std::string Random::String(size_t length)
		{
			std::vector<char> chars({
				'0', '1', '2', '3', '4',
				'5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F',
				'G', 'H', 'I', 'J', 'K',
				'L', 'M', 'N', 'O', 'P',
				'Q', 'R', 'S', 'T', 'U',
				'V', 'W', 'X', 'Y', 'Z',
				'a', 'b', 'c', 'd', 'e', 'f',
				'g', 'h', 'i', 'j', 'k',
				'l', 'm', 'n', 'o', 'p',
				'q', 'r', 's', 't', 'u',
				'v', 'w', 'x', 'y', 'z'
			});

			std::string str(length, 0);
			std::default_random_engine rng(std::random_device{}());
			std::mt19937 mt(rng());
			std::uniform_int_distribution<> dist(0, chars.size() - 1);

			std::generate_n(str.begin(), length, [chars, &dist, &mt]() {
				return chars[dist(mt)];
			});

			return str;
		}

		int Random::Int(const int& min, const int& max)
		{
			std::default_random_engine rd(std::random_device{}());
			std::mt19937 mt(rd());
			std::uniform_int_distribution<int> dist(min, max);

			return dist(mt);

		}

		unsigned int Random::UInt(const unsigned int& min, const unsigned int& max)
		{
			std::default_random_engine rd(std::random_device{}());
			std::mt19937 mt(rd());
			std::uniform_int_distribution<unsigned int> dist(min, max);

			return dist(mt);
		}

		float Random::Float(const float& min, const float& max)
		{
			std::default_random_engine rd(std::random_device{}());
			std::mt19937 mt(rd());
			std::uniform_real_distribution<float> dist(min, max);

			return dist(mt);
		}

		double Random::Double(const double& min, const float& max)
		{
			std::default_random_engine rd(std::random_device{}());
			std::mt19937 mt(rd());
			std::uniform_real_distribution<double> dist(min, max);

			return dist(mt);
		}

		

	}

}