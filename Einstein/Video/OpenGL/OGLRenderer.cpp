#include "OGLRenderer.hpp"

#include "../../Core/Logging/Logger.hpp"
#include "../../Util/Exception.hpp"
#include "../../Core/Engine.hpp"
#include "../../Core/UScene.hpp"

#include "../../Core/Actors/Actor.hpp"

#include <mathfu\matrix_4x4.h>
#include <mathfu\utilities.h>
#include <mathfu\glsl_mappings.h>

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			WindowParams::WindowParams() :
				width(DEFAULT_SCREEN_WIDTH),
				height(DEFAULT_SCREEN_HEIGHT),
				fullscreen(false),
				visible(false),
				name("Untitled")
			{
				master = nullptr;
			}

			Renderer::Renderer() :
				_windowParams(),
				_gBuffer()
			{

			}

			Renderer::Renderer(const WindowParams& windowParams) :
				_windowParams(windowParams)
			{
				this->_masterWindow = windowParams.master;
			}

			Renderer::~Renderer()
			{
				if (this->_renderWindow != nullptr)
				{
					glfwMakeContextCurrent(this->_renderWindow);
					this->Destroy();
				}
			}

			bool Renderer::Load()
			{
				//if (!this->LoadWindow()) return false;
				glfwMakeContextCurrent(this->_renderWindow);
				if (!this->_gBuffer.Initialise(this->_windowParams.width, this->_windowParams.height))
				{
#ifdef _DEBUG
					throw Util::Exception("Could not initialise renderer");
#endif
					gLogError << "Could not initialise renderer" << this->_windowParams.name;
					return false;
				}


				if (!this->LoadDefaultShaders()) return false;
				if (!this->LoadScreenElements()) return false;

				//glViewport(0, 0, this->_windowParams.width, this->_windowParams.height);
				glClearColor(0.3f, 0.3f, 0.3f, 0.0f);
				
				glFrontFace(GL_CW);
				glCullFace(GL_BACK);
				glEnable(GL_CULL_FACE);
				glEnable(GL_DEPTH_TEST);
				glDepthFunc(GL_LEQUAL);
				glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

				return true;
			}

			void Renderer::PreRender()
			{
				this->_gBuffer.PreRender();
			}

			void Renderer::RenderFrame(const RenderComponentList& renderList, const Core::CameraComponent* camera)
			{
				SpotLightComponentList spotLights;
				MeshComponentList meshes;
			
				for (const auto& it : renderList)
				{
					if (dynamic_cast<Mesh*>(it) != nullptr)
					{
						meshes.push_back(dynamic_cast<Mesh*>(it));
					}

					if (dynamic_cast<SpotLight*>(it) != nullptr)
					{
						spotLights.push_back(dynamic_cast<SpotLight*>(it));
					}
				}
				
				this->ShadowMapPass(meshes, spotLights, camera);
				this->GeometryPass(meshes, camera);
				//this->SeperateMaterialPass(meshes);
				this->SpotlightPass(spotLights, camera);
				this->PointlightPass();
				this->DirectionallightPass();
				this->CombinePasses();
				this->Element2DPass();
				this->FinalPass();
				
				
			}

			void Renderer::PostRender()
			{

				glBindFramebuffer(GL_FRAMEBUFFER, 0);
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, 0);
				
				glfwSwapBuffers(this->_renderWindow);
				glfwPollEvents();
			}

			void Renderer::Destroy()
			{
				this->_additionalPassElement.Destroy();

				this->_geometryShader.Unload();
				this->_nullShader.Unload();
				this->_pointLightShader.Unload();
				this->_dirLightShader.Unload();
				this->_spotLightShader.Unload();
				this->_uiShader.Unload();
				this->_combineShader.Unload();
				this->_alphaShader.Unload();

				this->_gBuffer.Destroy();
				glfwDestroyWindow(this->_renderWindow);
			}

			const WindowParams& Renderer::GetWindowParams()
			{
				return this->_windowParams;
			}

			bool Renderer::LoadWindow()
			{

				GLFWmonitor* monitor = nullptr;

				if (!this->_windowParams.visible)
				{
					glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
				}
				else {
					glfwWindowHint(GLFW_VISIBLE, GL_TRUE);
				}

				if (this->_windowParams.fullscreen)
				{
					monitor = glfwGetPrimaryMonitor();
				}

				this->_renderWindow = glfwCreateWindow(
					this->_windowParams.width,
					this->_windowParams.height,
					this->_windowParams.name.c_str(),
					monitor,
					NULL
					);

				if (!this->_renderWindow) {
#ifdef _DEBUG
					throw Util::Exception("Could not initialise renderer");
#endif
					gLogError << "Could not initialise renderer" << this->_windowParams.name;
					return false;
				}

				glfwMakeContextCurrent(this->_renderWindow);

				GLenum err = glewInit();

				if (err != GLEW_OK)
				{
					//Problem: glewInit failed, something is seriously wrong.
					gLogError << "Could not initialise OpenGL";
					glfwTerminate();
					return false;
				}

				return true;
			}

			bool Renderer::LoadDefaultShaders()
			{

				gLog << "Loading default shaders";

				if (!this->_geometryShader.Load(this->_windowParams.geometryShader))
					return false;
				this->_geometryShader.Enable();
				this->_geometryShader.setUniform1i("u_ColorMap", COLOR_TEXTURE_UNIT_INDEX);
				this->_geometryShader.setUniform1i("u_NormalMap", NORMAL_TEXTURE_UNIT_INDEX);
				this->_geometryShader.Disable();

				if (!this->_nullShader.Load(this->_windowParams.nullShader))
					return false;

				if (!this->_pointLightShader.Load(this->_windowParams.pointLightShader))
					return false;
				this->_pointLightShader.Enable();
				this->_pointLightShader.setUniform1i("u_PositionMap", GBuffer::GBUFFER_TEXTURE_TYPE_POSITION);
				this->_pointLightShader.setUniform1i("u_ColorMap", GBuffer::GBUFFER_TEXTURE_TYPE_DIFFUSE);
				this->_pointLightShader.setUniform1i("u_NormalMap", GBuffer::GBUFFER_TEXTURE_TYPE_NORMAL);
				this->_pointLightShader.setUniform2f("u_ScreenSize", this->_windowParams.width, this->_windowParams.height);
				this->_pointLightShader.Disable();

				if (!this->_dirLightShader.Load(this->_windowParams.dirLightShader))
					return false;
				this->_dirLightShader.Enable();
				this->_dirLightShader.setUniform1i("u_PositionMap", GBuffer::GBUFFER_TEXTURE_TYPE_POSITION);
				this->_dirLightShader.setUniform1i("u_ColorMap", GBuffer::GBUFFER_TEXTURE_TYPE_DIFFUSE);
				this->_dirLightShader.setUniform1i("u_NormalMap", GBuffer::GBUFFER_TEXTURE_TYPE_NORMAL);
				this->_dirLightShader.setUniform2f("u_ScreenSize", this->_windowParams.width, this->_windowParams.height);
				mathfu::Matrix<float, 4, 4> identity = mathfu::Matrix<float, 4, 4>::Identity();
				GLfloat* glIdMatrix = new GLfloat[16];
				for (int i = 0; i < 16; i++)
				{
					glIdMatrix[i] = identity[i];
				}
				this->_dirLightShader.setUniformMatrix4f("u_MVP", glIdMatrix);
				this->_dirLightShader.Disable();

				if (!this->_spotLightShader.Load(this->_windowParams.spotlightShader))
					return false;
				this->_spotLightShader.Enable();
				this->_spotLightShader.setUniform1i("u_PositionMap", GBuffer::GBUFFER_TEXTURE_TYPE_POSITION);
				this->_spotLightShader.setUniform1i("u_ColorMap", GBuffer::GBUFFER_TEXTURE_TYPE_DIFFUSE);
				this->_spotLightShader.setUniform1i("u_NormalMap", GBuffer::GBUFFER_TEXTURE_TYPE_NORMAL);
				this->_spotLightShader.setUniform2f("u_ScreenSize", this->_windowParams.width, this->_windowParams.height);
				this->_spotLightShader.Disable();

				if (!this->_uiShader.Load(this->_windowParams.uiShader))
					return false;

				this->_uiShader.Enable();
				this->_uiShader.setUniform1i("u_gSampler", 0);
				this->_uiShader.Disable();

				if (!this->_alphaShader.Load(this->_windowParams.alphaShader))
					return false;
				this->_alphaShader.Enable();
				this->_alphaShader.setUniform1i("u_ColorMap", COLOR_TEXTURE_UNIT_INDEX);
				this->_alphaShader.setUniform1i("u_NormalMap", NORMAL_TEXTURE_UNIT_INDEX);
				this->_alphaShader.Disable();

				if (!this->_combineShader.Load(this->_windowParams.combineShader))
					return false;

				this->_combineShader.Enable();
				this->_combineShader.setUniform1i("u_gSampler", 0);
				this->_combineShader.Disable();

				gLog << "	Done";

				return true;
			}

			bool Renderer::LoadScreenElements()
			{
				this->_additionalPassElement.SetScale(mathfu::Vector < float, 2 > {1.0f, 1.0f});
				this->_additionalPassElement.SetPosition(mathfu::Vector < float, 3 > {0.0f, 0.0f, 0.0f});
				this->_additionalPassElement.SetSize(mathfu::Vector < float, 2 > {(float)this->_windowParams.width, (float)this->_windowParams.height});
				this->_additionalPassElement.SetTextureID(this->_gBuffer.GetForwardTexture());
				if (!this->_additionalPassElement.Load()) return false;

				return true;
			}

			void Renderer::ShadowMapPass(const MeshComponentList& meshList, const SpotLightComponentList& lightList, const Core::CameraComponent* camera)
			{
				glCullFace(GL_FRONT);

				for (const auto& light : lightList)
				{
					light->GetShadowMap()->Start();

					mathfu::mat4 view = light->GetView();
					mathfu::mat4 projection = light->GetProjection();

					for (const auto& mesh : meshList)
					{
						mathfu::mat4 model = mesh->GetOnwer()->GetModelMatrix();
						mathfu::mat4 mvp = projection * view * model;

						GLShader depthShader = light->GetShadowMap()->GetDepthShader();

						depthShader.setUniformMatrix4f("u_Model", model);
						depthShader.setUniformMatrix4f("u_MVP", mvp);
						depthShader.setUniformMatrix4f("u_View", view);
						depthShader.setUniformMatrix4f("u_Projection", projection);

						mesh->Render();
					}

					light->GetShadowMap()->Stop();
				}
			}

			void Renderer::GeometryPass(const MeshComponentList& meshList, const Core::CameraComponent* camera)
			{
				this->_geometryShader.Enable();
				this->_gBuffer.BindForGeomPass();

				glDepthMask(GL_TRUE);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glEnable(GL_DEPTH_TEST);
				glCullFace(GL_BACK);

				mathfu::mat4 view = camera->GetView();
				mathfu::mat4 projection = camera->GetProjection();

				for (const auto& mesh : meshList)
				{

					mathfu::mat4 model = mesh->GetOnwer()->GetModelMatrix();									
					mathfu::mat4 mvp = projection * view * model;
					
					this->_geometryShader.setUniformMatrix4f("u_Model", model);
					this->_geometryShader.setUniformMatrix4f("u_MVP", mvp);
					this->_geometryShader.setUniformMatrix4f("u_View", view);
					this->_geometryShader.setUniformMatrix4f("u_Projection", projection);

					mesh->Render();
				}

				this->_geometryShader.Disable();

				glDepthMask(GL_FALSE);
			}

			void Renderer::SeperateMaterialPass(const MeshComponentList& meshList, const Core::CameraComponent* camera)
			{
				this->_gBuffer.BindForSeperateMaterialPass();
				this->_alphaShader.Enable();

				glDepthMask(GL_TRUE);
				glClear(GL_COLOR_BUFFER_BIT);
				glEnable(GL_DEPTH_TEST);
				glCullFace(GL_BACK);

				mathfu::mat4 view = camera->GetView();
				mathfu::mat4 projection = camera->GetProjection();

				for (const auto& it : meshList)
				{
					mathfu::mat4 model = it->GetOnwer()->GetModelMatrix();					
					mathfu::mat4 mvp = projection * view * model;
					
					this->_alphaShader.setUniformMatrix4f("u_Model", model);
					this->_alphaShader.setUniformMatrix4f("u_MVP", mvp);
					this->_alphaShader.setUniformMatrix4f("u_View", view);
					this->_alphaShader.setUniformMatrix4f("u_Projection", projection);

					it->Render();
					
				}

				glDepthMask(GL_FALSE);
				glDisable(GL_CULL_FACE);

				this->_alphaShader.Disable();
			}

			void Renderer::SpotlightPass(const SpotLightComponentList& lightList, const Core::CameraComponent* camera)
			{
				this->_gBuffer.BindForLightPass();
				GLint bindedTextureUnit = 0;
				glGetIntegerv(GL_ACTIVE_TEXTURE, &bindedTextureUnit);

				glDisable(GL_DEPTH_TEST);
				glEnable(GL_BLEND);
				glBlendEquation(GL_FUNC_ADD);
				glBlendFunc(GL_ONE, GL_ONE);

				mathfu::mat4 view = camera->GetView();
				mathfu::mat4 projection = camera->GetProjection();
				mathfu::mat4 model = mathfu::mat4::Identity();
				mathfu::mat4 mvp = projection * view * model;

				this->_spotLightShader.Enable();
				
				//Mesh* 
				Mesh* rQuad = Core::gEngine.GetSystem<Core::UniversalScene>()->GetIndexObject<Mesh>("bsquad");

				for (const auto& light : lightList)
				{
					this->_spotLightShader.setUniformMatrix4f("u_Model", model);
					this->_spotLightShader.setUniformMatrix4f("u_MVP", mvp);
					this->_spotLightShader.setUniformMatrix4f("u_View", view);
					this->_spotLightShader.setUniformMatrix4f("u_Projection", projection);

					glActiveTexture(SHADOW_TEXTURE_UNIT);
					this->_spotLightShader.setUniform1i("u_ShadowMap", SHADOW_TEXTURE_UNIT_INDEX);
					glBindTexture(GL_TEXTURE_2D, light->GetShadowMap()->GetShadowTexture());

					glActiveTexture(DEPTH_TEXTURE_UNIT);
					this->_spotLightShader.setUniform1i("u_DepthMap", DEPTH_TEXTURE_UNIT_INDEX);
					glBindTexture(GL_TEXTURE_2D, this->_gBuffer.GetDepthTexture());

					light->BindToShader(this->_spotLightShader);
					if (rQuad)
					{
						rQuad->GetOnwer()->SetPosition(light->GetOnwer()->GetDerivedPosition());
						rQuad->Render();
					}
				}

				glDisable(GL_BLEND);

				this->_spotLightShader.Disable();

				glActiveTexture(bindedTextureUnit);
			}

			void Renderer::PointlightPass()
			{

			}

			void Renderer::DirectionallightPass()
			{

			}

			void Renderer::CombinePasses()
			{
				this->_additionalPassElement.SetTextureID(this->_gBuffer.GetForwardTexture());
				this->_2dQueue.push_front(this->_additionalPassElement);
			}

			void Renderer::Element2DPass()
			{
				if (!this->_2dQueue.empty())
				{
					glEnable(GL_ALPHA_TEST);
					glAlphaFunc(GL_GREATER, 0);

					this->_uiShader.Enable();

					GLint viewport[4];
					glGetIntegerv(GL_VIEWPORT, viewport);
					GLfloat xScale = 2.0f / (GLfloat)viewport[2];
					GLfloat yScale = 2.0f / (GLfloat)viewport[3];

					while (!this->_2dQueue.empty())
					{
						GLfloat trans[] = {
							this->_2dQueue.front().GetScale()[0], 0.0f, 0.0f, this->_2dQueue.front().GetPosition()[0],
							0.0f, this->_2dQueue.front().GetScale()[1], 0.0f, this->_2dQueue.front().GetPosition()[1],
							0.0f, 0.0f, 1.0f, 1.0f,
							0.0f, 0.0f, 0.0f, 1.0f
						};

						this->_uiShader.setUniformMatrix4f("u_gTrans", trans);
						this->_2dQueue.front().Render();
						this->_2dQueue.pop_front();

					}

					this->_uiShader.Disable();

					glDisable(GL_ALPHA_TEST);
				}
			}

			void Renderer::FinalPass()
			{
				this->_gBuffer.BindForFinalPass();

				glBlitFramebuffer(0, 0, this->_windowParams.width, this->_windowParams.height,
					this->_windowParams.width, 0, 0, this->_windowParams.height, GL_COLOR_BUFFER_BIT, GL_LINEAR);

			}

		}
	}

}