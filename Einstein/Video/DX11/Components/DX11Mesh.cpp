#include "DX11Mesh.hpp"

#include "../../../Core/Actors/Actor.hpp"
#include "../../../Core/Logging/Logger.hpp"

#include "RenderComponent.hpp"


namespace Einstein {

	namespace Video {

		DX11Mesh::DX11Mesh() :
			_hasShaders(false),
			_textureID(0),
			_name("")
		{

		}

		DX11Mesh::~DX11Mesh()
		{

		}

		bool DX11Mesh::Load(const aiScene* pScene, ID3D11Device* device, const std::string& filename)
		{
			bool result = true;

			for (unsigned int i = 0; i < pScene->mNumMeshes; i++)
			{
				const aiMesh* mesh = pScene->mMeshes[i];
				if (!this->LoadMesh(i, mesh, device))
				{
					result = false;
					break;
				}
			}

			if (result)
			{
				// Load the texture sampler
				D3D11_SAMPLER_DESC samplerDesc;
				ZeroMemory(&samplerDesc, sizeof(samplerDesc));
				samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
				samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
				samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
				samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
				samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
				samplerDesc.MinLOD = 0;
				samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

				HRESULT hr = device->CreateSamplerState(&samplerDesc, &this->_samplerState);
				if (FAILED(hr))
				{
					gLogError << "Could not create a sampler state";
					return false;
				}
			}

			return result;
		}

		bool DX11Mesh::LoadShaders(const ShaderProfile& vsProfile, const ShaderProfile& psProfile, D3D11_INPUT_ELEMENT_DESC* inputDesc, const unsigned short& inputSize, ID3D11Device* device)
		{
			ID3D10Blob *vs, *ps;
			HRESULT hr;
			hr = D3DX11CompileFromFile(vsProfile.file.c_str(), 0, 0, vsProfile.function.c_str(), vsProfile.profile.c_str(), 0, 0, 0, &vs, 0, 0);
			if (FAILED(hr))
			{
				gLogError << "Could not load and compile vertex shader: " << vsProfile.file;
				return false;
			}

			hr = D3DX11CompileFromFile(psProfile.file.c_str(), 0, 0, psProfile.function.c_str(), psProfile.profile.c_str(), 0, 0, 0, &ps, 0, 0);
			if (FAILED(hr))
			{
				gLogError << "Could not load and compile pixel shader: " << vsProfile.file;
				return false;
			}

			device->CreateVertexShader(vs->GetBufferPointer(), vs->GetBufferSize(), NULL, &this->_vs);
			device->CreatePixelShader(ps->GetBufferPointer(), ps->GetBufferSize(), NULL, &this->_ps);

			device->CreateInputLayout(inputDesc, inputSize, vs->GetBufferPointer(), vs->GetBufferSize(), &this->_layout);

			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));

			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = 64;
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

			device->CreateBuffer(&bd, NULL, &this->_cBuffer);

			this->_hasShaders = true;

			return true;
		}

		const bool& DX11Mesh::HasShaders()
		{
			return this->_hasShaders;
		}

		bool DX11Mesh::LoadMesh(unsigned int index, const aiMesh* paiMesh, ID3D11Device* device)
		{
			std::vector<Vertex3f> vertices;
			std::vector<unsigned int> indices;

			mathfu::Vector<double, 3> ZeroVector = mathfu::kZeros3d;

			for (unsigned int i = 0; i < paiMesh->mNumVertices; i++)
			{
				const aiVector3D* pos = &(paiMesh->mVertices[i]);
				const aiVector3D* normal = &(paiMesh->mNormals[i]);
				const aiVector3D* texCoord;
				const aiColor4D* color;

				if (paiMesh->HasTextureCoords(0))
				{
					texCoord = &(paiMesh->mTextureCoords[0][i]);
				}
				else {
					texCoord = new aiVector3D(0.0f, 0.0f, 0.0f);
				}

				if (paiMesh->HasVertexColors(0))
				{
					color = &(paiMesh->mColors[0][i]);
				}
				else {
					color = new aiColor4D(0.5f, 0.5f, 0.5f, 1.0f);
				}

				Vertex3f vertex;

				vertex.position[0] = pos->x;
				vertex.position[1] = pos->y;
				vertex.position[2] = pos->z;
				vertex.position[3] = 1.0f;

				vertex.normal[0] = normal->x;
				vertex.normal[1] = normal->y;
				vertex.normal[2] = normal->z;
				vertex.normal[3] = 1.0f;

				vertex.textureCoordinate[0] = texCoord->x;
				vertex.textureCoordinate[1] = texCoord->y;

				vertex.color[0] = color->r;
				vertex.color[1] = color->g;
				vertex.color[2] = color->b;
				vertex.color[3] = color->a;

				const aiVector3D* tangent;

				if (paiMesh->HasTangentsAndBitangents())
				{
					tangent = &(paiMesh->mTangents[i]);
				}
				else {
					tangent = new aiVector3D(0.0f, 0.0f, 0.0f);
				}


				vertex.tangent[0] = tangent->x;
				vertex.tangent[1] = tangent->y;
				vertex.tangent[2] = tangent->z;
				vertex.tangent[3] = 1.0f;

				vertices.push_back(std::move(vertex));
			}

			for (unsigned int i = 0; i < paiMesh->mNumFaces; i++)
			{
				
				const aiFace& face = paiMesh->mFaces[i];

				indices.push_back(face.mIndices[0]);
				indices.push_back(face.mIndices[1]);
				indices.push_back(face.mIndices[2]);

				/*
				if (face.mNumIndices == 3)
				{
					/*indices.push_back(face.mIndices[2]);
					indices.push_back(face.mIndices[1]);
					indices.push_back(face.mIndices[0]); 
					indices.push_back(face.mIndices[2]);
					indices.push_back(face.mIndices[1]);
					indices.push_back(face.mIndices[0]);
				}
				*/
			}

			MeshEntry mEnt;
			mEnt.Init(vertices, indices, device);
			this->_entries.push_back(std::move(mEnt));

			return true;
		}

		struct CBUFFER
		{
			D3DXMATRIX Final;
			D3DXMATRIX Rotation;
			D3DXVECTOR4 LightVector;
			D3DXCOLOR LightColor;
			D3DXCOLOR AmbientColor;
		};

		void DX11Mesh::Render(ID3D11DeviceContext* context, const D3DXMATRIX& matView, const D3DXMATRIX& matProjection, const DX11TextureManager& textureManager) const
		{
			UINT stride = sizeof(Vertex3f);
			UINT offset = 0;

			mathfu::Vector<double, 3> posVector = this->_owner->GetOnwer()->GetDerivedPosition();
			mathfu::Quaternion<double> rotation = this->_owner->GetOnwer()->GetDerivedOreantation();
			
			D3DXMATRIX matTranslate, matRotate;

			D3DXMatrixTranslation(&matTranslate, posVector(0), posVector(1), posVector(2));

			static double Time = 0;

			

			D3DXMatrixRotationYawPitchRoll(
				&matRotate,
				rotation.ToEulerAngles()(1),
				rotation.ToEulerAngles()(0),
				rotation.ToEulerAngles()(2) + Time
				);

			Time += 0.001;

			D3DXMATRIX matFinal = matRotate * matTranslate * matView * matProjection;
			
			context->VSSetShader(this->_vs, 0, 0);
			context->PSSetShader(this->_ps, 0, 0);

			CBUFFER temp;
			temp.Final = matFinal;
			temp.Rotation = matRotate;
			temp.LightVector = D3DXVECTOR4(1.0f, 1.0f, -10.0f, 0.0f);
			temp.LightColor = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
			temp.AmbientColor = D3DXCOLOR(0.2f, 0.2f, 0.2f, 1.0f);

			context->IASetInputLayout(this->_layout);
			context->VSSetConstantBuffers(0, 1, &this->_cBuffer);
			context->UpdateSubresource(this->_cBuffer, 0, 0, &temp, 0, 0);

			if (this->_textureID != 0)
			{
				ID3D11ShaderResourceView* texture = textureManager.GetTexture(this->_textureID);
				context->PSSetShaderResources(0, 1, &texture);
				context->PSSetSamplers(0, 1, &this->_samplerState);
			}
			
			for (std::list<MeshEntry>::const_iterator cit = this->_entries.cbegin(); cit != this->_entries.cend(); ++cit)
			{
				context->IASetVertexBuffers(0, 1, &cit->_vBuffer, &stride, &offset);
				context->IASetIndexBuffer(cit->_iBuffer, DXGI_FORMAT_R32_UINT, 0);
				context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
				context->DrawIndexed(cit->_indexCount, 0, 0);
			}
		}

		DX11Mesh::MeshEntry::MeshEntry() :
			_indexCount(0)
		{

		}

		DX11Mesh::MeshEntry::~MeshEntry()
		{

		}

		void DX11Mesh::MeshEntry::Init(const std::vector<Vertex3f>& vertices, const std::vector<unsigned int>& indices, ID3D11Device* device)
		{
			this->_indexCount = indices.size();

			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));

			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = sizeof(Vertex3f) * vertices.size();
			bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.MiscFlags = 0;

			D3D11_SUBRESOURCE_DATA vertexData;
			vertexData.pSysMem = &vertices[0];
			vertexData.SysMemPitch = 0;
			vertexData.SysMemSlicePitch = 0;

			HRESULT hr = device->CreateBuffer(&bd, &vertexData, &this->_vBuffer);
			if (FAILED(hr))
			{
				gLogError << "Problem with creating a vertex buffer";
			}

			D3D11_BUFFER_DESC indexDesc;
			indexDesc.Usage = D3D11_USAGE_DEFAULT;
			indexDesc.ByteWidth = sizeof(unsigned int) * indices.size();
			indexDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			indexDesc.CPUAccessFlags = 0;
			indexDesc.MiscFlags = 0;

			D3D11_SUBRESOURCE_DATA indexData;
			indexData.pSysMem = &indices[0];
			indexData.SysMemPitch = 0;
			indexData.SysMemSlicePitch = 0;

			hr = device->CreateBuffer(&indexDesc, &indexData, &this->_iBuffer);
			if (FAILED(hr))
			{
				gLogError << "Problem with creating index buffer";
			}	
		}

		void DX11Mesh::MeshEntry::Unload()
		{
			this->_vBuffer->Release();
			this->_iBuffer->Release();
		}

		void DX11Mesh::Destroy()
		{
			this->_vs->Release();
			this->_ps->Release();
			while (!this->_entries.empty())
			{
				this->_entries.front().Unload();
				this->_entries.pop_front();
			}
		}

	}

}