#include "TaskManager.hpp"
#include "../Logging/Logger.hpp"

namespace Einstein {

	namespace Core {

		namespace Tasking {

			TaskManager::TaskManager(size_t numWorkers) :
				_numWorkers(numWorkers),
				_isRunning(false)
			{
				if (_numWorkers == 0)
					_numWorkers = tbb::tbb_thread::hardware_concurrency() + 1; 

				if (_numWorkers < 1)
					_numWorkers = 1;
			}

			void TaskManager::addWork(Task t, bool repeating, bool background)
			{
				this->addWork(make_wrapped(std::move(t), repeating, background));
			}

			void TaskManager::addRepeatingWork(Task t, bool background) {
				this->addWork(std::move(t), true, background);
			}

			void TaskManager::addBackgroundWork(Task t, bool repeating) {
				this->addWork(std::move(t), repeating, true);
			}

			void TaskManager::addRepeatingBackgroundWork(Task t) {
				this->addWork(std::move(t), true, true);
			}

			void TaskManager::start()
			{
				if (this->_isRunning) return;

				this->_isRunning = true;

				gLog << "Starting with " << this->_numWorkers << " background threads";

				for (size_t i = 0; i < this->_numWorkers; ++i)
				{
					this->_threads.push_back(
						new tbb::tbb_thread([this]{
						while (_isRunning)
						{
							WrappedTask t;
							bool popResult = _backgroundTasks.try_pop(t);
							if (popResult) execute(t);
						}
					})
					);
				}

				while (this->_isRunning)
				{
					TaskQueue localQueue;
					this->_mainTasks.swap(localQueue);
					WrappedTask t;
					while (localQueue.try_pop(t))
					{
						this->execute(t);
					}
					
				}
			}

			void TaskManager::stop()
			{
				this->_isRunning = false;

				for (size_t i = 0; i < this->_numWorkers; i++)
				{
					this->addBackgroundWork([this]{ _isRunning = false;  });
				}

				for (const auto& it : this->_threads)
				{
					it->join();
				}
				
			}

			void TaskManager::addWork(WrappedTask t)
			{
				if (t.isBackground())
				{
					this->_backgroundTasks.push(t);
				}
				else {
					this->_mainTasks.push(t);
				}
			}

			void TaskManager::execute(WrappedTask t)
			{
				t();

				if (t.isRepeating())
				{
					this->addWork(std::move(t));
				}
			}

		}

	}

}