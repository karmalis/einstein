#include "ActorComponent.hpp"

#include "Actor.hpp"

namespace Einstein {

	namespace Core {

		std::string ActorComponent::GetIndexPath() const
		{
			std::string name = this->_owner->GetIndexPath();
			name.append("/Components/").append(this->GetName());
			return name;
		}

	}

}