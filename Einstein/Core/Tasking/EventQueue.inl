#ifndef __EVENT_QUEUE_INL__
#define __EVENT_QUEUE_INL__

#include "EventQueue.hpp"

namespace Einstein {

	namespace Core {

		namespace Tasking {

			template <typename T>
			EventQueue<T>& EventQueue<T>::instance()
			{
				static EventQueue instance;
				return instance;
			}

			template <typename T>
			bool EventQueue<T>::PollEvents(Handler& handler)
			{
				return this->_eventHandlerQueue.try_pop(handler);
			}

			template <typename T>
			bool EventQueue<T>::PollEvents(Event& evt)
			{
				return this->_eventQueue.try_pop(evt);
			}

			template <typename T>
			void  EventQueue<T>::QueueEvent(const Handler& handler)
			{
				this->_eventHandlerQueue.push(handler);
			}

			template <typename T>
			void  EventQueue<T>::QueueEvent(const Event& evt)
			{
				this->_eventQueue.push(evt);
			}

			template <typename T>
			EventQueue<T>::EventQueue()
			{

			}

		}

	}

}

#endif