#ifndef __LIGHT_HPP__
#define __LIGHT_HPP__

#include <list>
#include <vector>
#include <rapidjson\document.h>

#include "../../Vertex.hpp"
#include "RenderComponent.hpp"
#include "../ShadowMap.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			class Environment {

			public: 

				static mathfu::Vector<float, 4> ambient;
				static float ambientIntensity;

			};

			class Light : public RenderComponent
			{
			public:

				static Core::ActorComponent* Create(const rapidjson::Value& params, Core::Actor* actor);

				virtual bool Initialise(const rapidjson::Value& params);
				virtual void Update(const double& deltaTime);
				virtual void Destroy();

				virtual void Render() {}

				const mathfu::Vector<float, 4>& GetDiffuse() { return this->_diffuse; }
				const float& GetDiffuseIntensity() { return this->_diffuseIntensity; }				
				const float& GetConstantAttenuation() { return this->_constantAtt; }
				const float& GetLinearAttenuation() { return this->_linearAtt; }
				const float& GetExpAttenuation() { return this->_expAtt; }
				void SetDiffuse(const mathfu::Vector<float, 4>& colour) { this->_diffuse = colour; }				
				void SetDiffuseIntensity(const float& value) { this->_diffuseIntensity = value; }
				void SetConstantAttenuation(const float& value) { this->_constantAtt = value; }
				void SetLinearAttenuation(const float& value) { this->_linearAtt = value; }
				void SetExpAttenuation(const float& value) { this->_expAtt = value; }

				const mathfu::mat4& GetView() { return this->_view; }
				const mathfu::mat4& GetProjection() { return this->_projection; }

				virtual void BindToShader(GLShader& shader) = 0;

			protected:

				virtual void CalculateMatrices();

				Light(std::string name);

				mathfu::Vector<float, 4> _diffuse;
				
				float _diffuseIntensity;				
				float _brightness;
				float _constantAtt;
				float _linearAtt;
				float _expAtt;
				
				mathfu::mat4 _view;
				mathfu::mat4 _projection;

			};

		}

	}

}

#endif