#include "Actor.hpp"

#include "../Logging/Logger.hpp"

namespace Einstein {

	namespace Core {

		tbb::atomic<unsigned int> globalCID = tbb::make_atomic<unsigned int>(1);

		Actor::Actor(std::string name) :
			UniversalObject(name),
			Movable(),
			_componentID(1)
		{

		}

		bool Actor::Initialise(const rapidjson::Document& doc)
		{

			if (doc.HasMember("properties") && doc["properties"].IsObject())
			{
				if (doc["properties"].HasMember("position") && doc["properties"]["position"].IsObject())
				{
					mathfu::Vector<float, 3> pos(
						doc["properties"]["position"]["x"].GetDouble(),
						doc["properties"]["position"]["y"].GetDouble(),
						doc["properties"]["position"]["z"].GetDouble()
						);
					this->SetPosition(std::move(pos));
				}

				if (doc["properties"].HasMember("rotation") && doc["properties"]["rotation"].IsObject())
				{
					this->SetRotationX(doc["properties"]["rotation"]["x"].GetDouble());
					this->SetRotationY(doc["properties"]["rotation"]["y"].GetDouble());
					this->SetRotationZ(doc["properties"]["rotation"]["z"].GetDouble());
				}

				if (doc["properties"].HasMember("up") && doc["properties"]["up"].IsObject())
				{
					mathfu::Vector<float, 3> up(
						doc["properties"]["up"]["x"].GetDouble(),
						doc["properties"]["up"]["y"].GetDouble(),
						doc["properties"]["up"]["z"].GetDouble()
						);
					this->SetUp(std::move(up));
				}
			}

			return true;
		}

		void Actor::Update(const double& deltaTime)
		{
			UniversalObject::Update(deltaTime);
			Movable::UpdateMovable(deltaTime);
			for (const auto& it : this->_components)
			{
				it->Update(deltaTime);
			}
			this->_requiresUpdate = false;
			this->UpdateModelMatrix();
		}

		void Actor::Destroy()
		{
			UniversalObject::Destroy();
		}

		Actor* Actor::create(const rapidjson::Document& doc)
		{
			std::string name = "temp";
			
			// TODO: Validate the json
			if (doc.HasMember("properties"))
			{
				if (doc["properties"].IsObject() && doc["properties"].HasMember("name"))
				{
					name = doc["properties"]["name"].GetString();
				}
			}

			Actor* result = new Actor(name);

			if (result->Initialise(doc))
			{
				return std::move(result);
			}

			return nullptr;
			
		}

		Actor* Actor::create(const std::string& jsonString)
		{
			rapidjson::Document doc;
			doc.Parse(jsonString.c_str());

			if (doc.HasParseError())
			{
				gLogError << "Could not parse actor json string: " << doc.GetParseError();
				return nullptr;
			}

			return Actor::create(std::move(doc));
		}

		const unsigned int& Actor::GetAndIncrementCID()
		{
			return globalCID.fetch_and_increment();
		}

		void Actor::AddComponent(ActorComponent* ptr)
		{
			ptr->SetOwner(this);
			this->_components.emplace_back(ptr);
		}

		void Actor::RemoveComponent(ActorComponent* ptr)
		{
			for (ComponentList::iterator it = this->_components.begin(); it != this->_components.end(); ++it)
			{
				if (*it == ptr)
				{
					this->_components.erase(it);
					break;
				}
			}
		}

		const mathfu::Vector<float, 3>& Actor::GetDerivedPosition()
		{
			if (this->_parent)
			{
				Actor* parent = dynamic_cast<Actor*>(this->_parent);
				if (parent == nullptr) return this->_position;

				mathfu::Quaternion<float> rot = parent->GetDerivedOreantation();
				mathfu::Vector<float, 3> scale = parent->GetDerivedScale();
				mathfu::Vector<float, 3> derPos = scale * this->_position;
				derPos = rot * derPos;

				derPos += parent->GetDerivedPosition();

				return std::move(derPos);
			}

			return this->_position;
		}

		const mathfu::Quaternion<float>& Actor::GetDerivedOreantation()
		{
			if (this->_parent)
			{
				Actor* parent = dynamic_cast<Actor*>(this->_parent);
				if (parent == nullptr) return this->_oreantation;

				mathfu::Quaternion<float> rot = parent->GetDerivedOreantation() * this->_oreantation;

				return std::move(rot);
			}

			return this->_oreantation;
		}

		const mathfu::Vector<float, 3>& Actor::GetDerivedScale()
		{
			if (this->_parent)
			{
				Actor* parent = dynamic_cast<Actor*>(this->_parent);
				if (parent == nullptr) return this->_scale;

				mathfu::Vector<float, 3> scale = parent->GetDerivedScale() * this->_scale;

				return std::move(scale);

			}

			return this->_scale;
		}

		void Actor::UpdateModelMatrix()
		{
			mathfu::mat4 translation = mathfu::mat4::FromTranslationVector(this->GetDerivedPosition());
			mathfu::mat4 scale = mathfu::mat4::FromScaleVector(this->GetDerivedScale());
			mathfu::mat4 rotation = mathfu::mat4::FromRotationMatrix(this->GetDerivedOreantation().ToMatrix());

			this->_modelMatrix = translation * rotation * scale;
			this->_requiresUpdate = true;
		}

	}

}
