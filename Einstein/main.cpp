
#include "Core\Engine.hpp"
#include "Core\Logging\Logger.hpp"

#include "Core\System.hpp"
#include "Core\ResourceManager.hpp"
#include "Core\UScene.hpp"
#include "Video\OpenGL\OGLSystem.hpp"
#include "Video\OpenGL\Shading\MaterialManager.hpp"
#include "App\SystemTest\VideoTest.hpp"

int main(int argc, char** argv)
{
	// Core
	Einstein::Core::gEngine.AddSystem(new Einstein::Core::ResourceManager);
	Einstein::Core::gEngine.AddSystem(new Einstein::Core::UniversalScene);
	
	// Video
	Einstein::Core::gEngine.AddSystem(new Einstein::Video::OpenGL::RenderSystem);
	
	Einstein::Core::gEngine.Initialise();

	Einstein::Core::gEngine.SetApplication(new Einstein::Test::VideoTest);

	Einstein::Core::gEngine.Run();

	return 0;
}