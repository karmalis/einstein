#include "DX11TextureManager.hpp"


#include "../../Core/Engine.hpp"
#include "DX11System.hpp"
#include "../../Core/ResourceManager.hpp"

namespace Einstein {

	namespace Video {

		tbb::atomic<unsigned int> globalTextureID = tbb::make_atomic<unsigned int>(1);

		DX11TextureManager::DX11TextureManager()
		{

		}

		DX11TextureManager::~DX11TextureManager()
		{
			for (auto it : this->_textureMap)
			{
				it.second->Release();
			}

			this->_textureMap.clear();
		}

		ID3D11ShaderResourceView* DX11TextureManager::GetTexture(const unsigned int& id) const
		{
			auto it = this->_textureMap.find(id);
			if (it != this->_textureMap.end())
			{
				return it->second;
			}

			return nullptr;
		}

		ID3D11ShaderResourceView* DX11TextureManager::GetTexture(const rapidjson::Value& params)
		{
			std::string target = params["file"].GetString();
			return this->GetTexture(target);
		}

		ID3D11ShaderResourceView* DX11TextureManager::GetTexture(const std::string& target)
		{
			unsigned int id = 0;

			auto it = this->_textureIDMap.find(target);
			if (it == this->_textureIDMap.end())
			{
				id = this->LoadTexture(target);
			}

			return this->GetTexture(id);
		}

		void DX11TextureManager::DestroyTexture(const unsigned int& id)
		{
			auto it = this->_textureMap.find(id);
			if (it != this->_textureMap.end())
			{
				it->second->Release();
				this->_textureMap.erase(it);
			}

			TextureIDMap::iterator jit;
			for (jit = this->_textureIDMap.begin(); jit != this->_textureIDMap.end(); ++jit)
			{
				if (id == jit->second) break;
			}

			if (jit != this->_textureIDMap.end()) this->_textureIDMap.erase(jit);
		}

		const unsigned int& DX11TextureManager::LoadTexture(const rapidjson::Value& params)
		{
			return this->LoadTexture(params["file"].GetString());
		}

		const unsigned int& DX11TextureManager::LoadTexture(const std::string& target)
		{
			Core::ResourceManager* rm = Core::gEngine.GetSystem<Core::ResourceManager>();
			DX11System* sm = Core::gEngine.GetSystem<DX11System>();

			//std::string filePath = rm->GetBinaryDataResource
			std::string filepath = rm->GetResourcePath(target);

			ID3D11ShaderResourceView* texture;

			HRESULT hr = D3DX11CreateShaderResourceViewFromFile(
				sm->GetD11Device(),
				filepath.c_str(),
				NULL,
				NULL,
				&texture,
				NULL
				);

			if (FAILED(hr))
			{
				gLogError << "Could not load texture: " << target;
				return std::move(int(0));
			}

			this->_currTextureID = globalTextureID.fetch_and_increment();
			
			this->_textureMap[this->_currTextureID] = texture;
			this->_textureIDMap[target] = this->_currTextureID;

			return this->_currTextureID;
		}

	}

}