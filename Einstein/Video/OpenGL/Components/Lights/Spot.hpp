#ifndef __SPOT_LIGHT_HPP__
#define __SPOT_LIGHT_HPP__

#include "../Light.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			class Light;

			class SpotLight : public Light
			{
			public:

				SpotLight(std::string name);

				virtual bool Initialise(const rapidjson::Value& params);
				virtual void Update(const double& deltaTime);
				virtual void Destroy();

				virtual void BindToShader(GLShader& shader);

				ShadowMap* GetShadowMap() { return this->_shadowMap.get(); }

			protected:

				std::unique_ptr<ShadowMap> _shadowMap;
				float _cutoff;

			};

		}

	}

}



#endif