#pragma once

#include "LogSink.hpp"

namespace Einstein {

	namespace Core {

		template <typename T>
		LogSink::LogSink(T impl) :
			_wrapper(new Model<T>(std::forward<T>(impl)))
		{

		}

		template <typename T>
		LogSink::Model<T>::Model(T impl) :
			_impl(std::forward<T>(impl))
		{

		}

		template <typename T>
		LogSink::Concept* LogSink::Model<T>::clone() const {
			return new Model<T>(_impl);
		}

		template <typename T>
		void LogSink::Model<T>::forward(
			const LogMessage::Meta& meta,
			const std::string& message
			) const {
			this->_impl(meta, message);
		}

	}

}
