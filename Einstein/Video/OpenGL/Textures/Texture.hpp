#ifndef __GL_TEXTURE_HPP__
#define __GL_TEXTURE_HPP__

#include "../OGLIncludes.hpp"
#include <vector>

#define NORMAL_TEXTURE_UNIT GL_TEXTURE2
#define DEFAULT_NORMAL_MAP "default_normal.jpg"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			class Texture
			{
			public:

				enum {
					TEXTURE_2D,
					CUBEMAP
				};

				Texture();
				Texture(
					const unsigned int& width,
					const unsigned int& height,
					const unsigned char* pixels,
					const unsigned int& w_width = 0,
					const unsigned int& w_height = 0);
				~Texture();

				bool Load(const unsigned int& width,
					const unsigned int& height,
					const unsigned char* pixels,
					const unsigned int& w_width = 0,
					const unsigned int& w_height = 0);

				bool Reload();
				void Update(double deltaTime);
				void Unload();
				void Bind(GLenum texture = 0, GLenum type = GL_TEXTURE_2D);
				void Unbind(GLenum type = GL_TEXTURE_2D);

				bool LoadFromFile(
					const std::string& filename,
					GLenum image_format = GL_RGB,
					GLint internal_format = GL_RGB,
					GLint level = 0,
					GLint border = 0
					);

				bool LoadCubeMap(std::vector<std::string> filenames,
					GLenum image_format = GL_RGB,
					GLint internal_format = GL_RGB,
					GLint level = 0,
					GLint border = 0);

				const unsigned int& GetID() { return this->_ID; }
				std::string GetName() { return this->_name; }
				const unsigned int& GetWidth() { return this->_width; }
				const unsigned int& GetHeight() { return this->_height; }
				const unsigned char* GetPixels() const { return this->_pixels; }
				void SetID(const unsigned int& id) { this->_ID = id; }
				void Setname(std::string name) { this->_name = name; }
				void SetWidth(const unsigned int& width) { this->_width = width; }
				void SetHeight(const unsigned int& height) { this->_height = height; }
				void SetPixels(const unsigned char *pixels) { this->_pixels = pixels; this->_reload = true; }
				void SetTextureType(const unsigned short& type) { this->_textureType = type; }
				const unsigned short& GetTextureType() { return this->_textureType; }

				const unsigned int& GetUseCount() { return this->_useCount; }
				void DecUseCount() { this->_useCount--; }
				void IncUseCount() { this->_useCount++; }
				void SetUseCount(const unsigned int& value) { this->_useCount = value; }

			protected:

				GLuint _boundedTexture;
				bool _unboundTexture;

				unsigned int _ID;
				unsigned int _useCount;
				std::string _name;
				unsigned int _width, _height;
				const unsigned char* _pixels;
				bool _reload;

				unsigned short _textureType;

			};

		}

	}

}

#endif