#ifndef __NAMED_HPP__
#define __NAMED_HPP__

#include <string>

namespace Einstein {

	namespace Util {
		
		class Named
		{
		public:

			Named(std::string name);
			const std::string& getName() const;

		protected:
			void setName(std::string name);

		private:

			std::string _name;
		};

	}

}

#endif