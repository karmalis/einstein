#ifndef __APPLICATION_HPP__
#define __APPLICATION_HPP__

#include "../Core/System.hpp"

namespace Einstein {

	namespace App {

		class Application : public Core::System
		{
		public:

			enum {
				INIT = 0x1 << 0,
				RUNNING = 0x1 << 1
			};

			Application(std::string name);

			virtual bool Initialise() = 0;
			virtual void Update() = 0;
			virtual void ShutDown() = 0;

		};

	}

}

#endif