#ifndef __CHANNEL_INL__
#define __CHANNEL_INL__

#include "Channel.hpp"

namespace Einstein {

	namespace Core {

		namespace Tasking {

			template <typename tEvent, class tHandler>
			void Channel::add(tHandler* handler)
			{
				ChannelQueue<tEvent>::instance().add(handler);
			}

			template <typename tEvent, class tHandler>
			void Channel::remove(tHandler* handler)
			{
				ChannelQueue<tEvent>::instance().remove(handler);
			}

			template <typename tEvent>
			void Channel::broadcast(const tEvent& message)
			{
				ChannelQueue<tEvent>::instance().broadcast(message);
			}

			template <typename T>
			MessageHandler<T>::MessageHandler() {
				Channel::add<T>(this);
			}

			template <typename T>
			MessageHandler<T>::~MessageHandler() {
				Channel::remove<T>(this);
			}
		}
	}

}

#endif