#include "GBuffer.hpp"

#include "../../Core/Logging/Logger.hpp"

#include "../../Util/Exception.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			GBuffer::GBuffer() :
				_fbo(0),
				_depthTexture(0),
				_finalTexture(0),
				_frTexture(0)
			{

			}

			GBuffer::~GBuffer()
			{

			}

			bool GBuffer::Initialise(int width, int height)
			{
				gLog << "Loading OpenGL GBuffer";

				glGenFramebuffers(1, &this->_fbo);
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->_fbo);

				unsigned int textureSize = sizeof(this->_textures) / sizeof(this->_textures[0]);
				glGenTextures(textureSize, this->_textures);

				glGenTextures(1, &this->_depthTexture);
				glGenTextures(1, &this->_finalTexture);
				glGenTextures(1, &this->_frTexture);

				for (unsigned int i = 0; i < textureSize; i++)
				{
					glBindTexture(GL_TEXTURE_2D, this->_textures[i]);
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, NULL);
					glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
					glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
					glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, this->_textures[i], 0);
				}

				glBindTexture(GL_TEXTURE_2D, this->_depthTexture);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH32F_STENCIL8, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
				glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, this->_depthTexture, 0);

				glBindTexture(GL_TEXTURE_2D, this->_frTexture);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, this->_frTexture, 0);

				glBindTexture(GL_TEXTURE_2D, this->_finalTexture);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
				glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_2D, this->_finalTexture, 0);

				GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
				if (status != GL_FRAMEBUFFER_COMPLETE)
				{
#ifdef _DEBUG
					throw new Util::Exception("Could not initialise GBuffer");
#endif
					gLogFatal << "Could not initialise OpenGL GBuffer";
					return false;
				}

				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

				gLog << "	Done";

				return true;
			}

			void GBuffer::Destroy()
			{
				gLog << "Destroying OpenGL GBuffer";

				if (this->_fbo != 0) glDeleteFramebuffers(1, &this->_fbo);
				if (this->_frTexture != 0) glDeleteFramebuffers(1, &this->_frTexture);
				if (this->_textures[0] != 0) glDeleteFramebuffers(sizeof(this->_textures) / sizeof(this->_textures[0]), this->_textures);
				if (this->_finalTexture != 0) glDeleteFramebuffers(1, &this->_finalTexture);
				if (this->_depthTexture != 0) glDeleteFramebuffers(1, &this->_depthTexture);

				gLog << "	Done";
			}

			void GBuffer::PreRender()
			{
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->_fbo);
				glDrawBuffer(GL_COLOR_ATTACHMENT5);
				glClear(GL_COLOR_BUFFER_BIT);
			}

			void GBuffer::BindForGeomPass()
			{
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->_fbo);
				GLenum drawBuffer[] = {
					GL_COLOR_ATTACHMENT0,
					GL_COLOR_ATTACHMENT1,
					GL_COLOR_ATTACHMENT3
				};

				glDrawBuffers(sizeof(drawBuffer) / sizeof(drawBuffer[0]), drawBuffer);
			}

			void GBuffer::BindForStencilPass()
			{
				glDrawBuffer(GL_NONE);
			}

			void GBuffer::BindForLightPass()
			{
				glDrawBuffer(GL_COLOR_ATTACHMENT5);
				unsigned int size = sizeof(this->_textures) / sizeof(this->_textures[0]);
				for (unsigned int i = 0; i < size; i++)
				{
					glActiveTexture(GL_TEXTURE0 + i);
					glBindTexture(GL_TEXTURE_2D, this->_textures[GBUFFER_TEXTURE_TYPE_POSITION + i]);
				}
			}

			void GBuffer::BindForSeperateMaterialPass()
			{
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->_fbo);
				glDrawBuffer(GL_COLOR_ATTACHMENT4);
			}

			void GBuffer::BindFor2DElementPass()
			{
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->_fbo);
				glDrawBuffer(GL_COLOR_ATTACHMENT5);
			}

			void GBuffer::BindForFinalPass()
			{
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
				glBindFramebuffer(GL_READ_FRAMEBUFFER, this->_fbo);
				glReadBuffer(GL_COLOR_ATTACHMENT5);
			}

		}

	}

}