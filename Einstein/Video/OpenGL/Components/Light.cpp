#include "Light.hpp"
#include "../../../Core/Logging/Logger.hpp"
#include "../../../Core/Actors/Actor.hpp"
#include "../../../Core/Engine.hpp"

#include "Lights\Directional.hpp"
#include "Lights\Point.hpp"
#include "Lights\Spot.hpp"

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			mathfu::Vector<float, 4> Environment::ambient = mathfu::Vector < float, 4 > {0.5f, 0.5f, 0.5f, 1.0f};
			float Environment::ambientIntensity = 0.2f;

			Core::ActorComponent* Light::Create(const rapidjson::Value& params, Core::Actor* actor)
			{
				std::string name = actor->GetName();
				std::string type = params["type"].GetString();
				std::string actorID = std::to_string(actor->GetID());

				name.append("_").append(type).append("_").append(actorID);

				ActorComponent* result = nullptr;

				if (!params.HasMember("light_type"))
				{
					return nullptr;
				}

				std::string lightType = params["light_type"].GetString();

				if (lightType == "spotlight")
				{
					result = new SpotLight(name);
				}

				if (result == nullptr) return nullptr;

				if (result->Initialise(params["properties"]))
				{
					return std::move(result);
				}

				delete[] result;

				return nullptr;
			}

			Light::Light(std::string name) :
				RenderComponent(name),
				_diffuseIntensity(0),
				_brightness(0),
				_constantAtt(0),
				_linearAtt(0),
				_expAtt(0)
			{
				
			}

			bool Light::Initialise(const rapidjson::Value& params)
			{
				
				if (params.HasMember("constant_attenuation"))
				{
					this->_constantAtt = params["constant_attenuation"].GetDouble();
				}

				if (params.HasMember("linear_attenuation"))
				{
					this->_linearAtt = params["linear_attenuation"].GetDouble();
				}

				if (params.HasMember("exp_attenuation"))
				{
					this->_expAtt = params["exp_attenuation"].GetDouble();
				}
				/*
				if (params.HasMember("quadratic_attenuation"))
				{
					this->_quadraticAtt = params["quadratic_attenuation"].GetDouble();
				}
				*/

				if (params.HasMember("diffuse_intensity"))
				{
					this->_diffuseIntensity = params["diffuse_intensity"].GetDouble();
				}

				if (params.HasMember("diffuse"))
				{
					this->_diffuse[0] = params["diffuse"]["r"].GetDouble();
					this->_diffuse[1] = params["diffuse"]["g"].GetDouble();
					this->_diffuse[2] = params["diffuse"]["b"].GetDouble();
				}

				return RenderComponent::Initialise(params);
			}

			void Light::Update(const double& deltaTime)
			{
				if (this->_requiresUpdate || this->_owner->RequiresUpdate())
				{
					this->CalculateMatrices();
					this->_requiresUpdate = false;
				}
			}

			void Light::Destroy()
			{
				
			}

			void Light::CalculateMatrices()
			{
				this->_view = mathfu::mat4::LookAt(
					this->_owner->GetTarget(),
					this->_owner->GetDerivedPosition(),
					this->_owner->GetUp()
					);

				int sWidth = Core::gEngine.GetSettings()->get("width", 800);
				int sHeight = Core::gEngine.GetSettings()->get("height", 600);

				this->_projection = mathfu::mat4::Ortho(
					0.0f,
					(float)sWidth,
					(float)sHeight,
					0.0f,
					0.1f,
					9999.9f
					);

			}

		}

	}

}