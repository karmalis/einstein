#ifndef __EVENT_QUEUE_HPP__
#define __EVENT_QUEUE_HPP__

#include <algorithm>
#include <functional>
#include <iostream>
#include <tbb/mutex.h>
#include <vector>
#include <utility>
#include <tbb\concurrent_queue.h>

namespace Einstein {

	namespace Core {

		class System;

		namespace Tasking {

			struct Event {
				
				std::function<void(const System* system)> handler;

			};

			template<typename T>
			class EventQueue
			{
			public:

				using Handler = std::function < void(const T&) > ;

				static EventQueue& instance();

				bool PollEvents(Handler& handler);
				bool PollEvents(Event& evt);

				void QueueEvent(const Handler& handler);
				void QueueEvent(const Event& evt);
			
			protected:

				EventQueue();

				tbb::concurrent_queue<Handler> _eventHandlerQueue;
				tbb::concurrent_queue<Event> _eventQueue;

			};

		}

	}

}

#include "EventQueue.inl"

#endif