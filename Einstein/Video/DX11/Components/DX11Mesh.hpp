#ifndef __DX11_MESH_HPP__
#define __DX11_MESH_HPP__

#include "../../Vertex.hpp"

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>

#include "../../../Includes.hpp"

#include <list>
#include <vector>

namespace Einstein {

	namespace Video {

		class DX11RenderComponent;
		class DX11TextureManager;

		class DX11Mesh
		{
		public:

			struct ShaderProfile {
				std::string file;
				std::string function;
				std::string profile;

			};

			DX11Mesh();
			~DX11Mesh();

			void Destroy();

			unsigned int vertexCount;
			unsigned int indexCount;

			bool Load(const aiScene* pScene, ID3D11Device* device, const std::string& filename = "");
			bool LoadShaders(const ShaderProfile& vsProfile, const ShaderProfile& psProfile, D3D11_INPUT_ELEMENT_DESC* inputDesc, const unsigned short& inputSize,  ID3D11Device* device);
			// Use below with caution, make sure render is called from the same thread as it was created on
			void Render(ID3D11DeviceContext* context, const D3DXMATRIX& matView, const D3DXMATRIX& matProjection, const DX11TextureManager& textureManager) const;

			const bool& HasShaders();

			const std::string& GetName() { return this->_name;  }

		protected:

			friend class DX11RenderComponent;

			DX11RenderComponent* _owner;

			bool LoadMesh(unsigned int index, const aiMesh* paiMesh, ID3D11Device* device);

			std::string _name;
			unsigned int _textureID;

			struct MeshEntry {
				MeshEntry();
				~MeshEntry();

				void Init(const std::vector<Vertex3f>& vertices, const std::vector<unsigned int>& indices, ID3D11Device* device);
				void Unload();

				unsigned int _indexCount;
				ID3D11Buffer* _vBuffer;
				ID3D11Buffer* _iBuffer;

			};

			bool _hasShaders;

			std::list<MeshEntry> _entries;
			ID3D11VertexShader* _vs;
			ID3D11PixelShader* _ps;
			ID3D11InputLayout* _layout;
			ID3D11Buffer* _cBuffer;
			ID3D11SamplerState* _samplerState;

		};

	}

}

#endif