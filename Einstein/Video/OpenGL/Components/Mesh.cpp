#include "Mesh.hpp"

#include "../../../Core/Logging/Logger.hpp"

#include "../../../Core/Engine.hpp"
#include "../../../Core/ResourceManager.hpp"
#include "../../../Core/Actors/Actor.hpp"

#include "../Textures/TextureManager.hpp"
#include "../Shading/MaterialManager.hpp"

#include "../OGLSystem.hpp"

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			Mesh::Mesh(std::string name) : RenderComponent(name)
			{

			}

			bool Mesh::Initialise(const rapidjson::Value& params)
			{
				if (!params.IsArray()) return false;

				for (rapidjson::Value::ConstValueIterator itr = params.Begin(); itr != params.End(); ++itr)
				{
					if (!this->LoadMesh(*itr))
					{
						gLogError << "Could not load mesh: " << (*itr)["name"].GetString();
					}

					if (!this->LoadShading(*itr))
					{
						gLogError << "Could not load shading for mesh: " << (*itr)["name"].GetString();
					}

					if (!this->LoadTextures(*itr))
					{
						gLogError << "Could not load textures for mesh: " << (*itr)["name"].GetString();
					}

					if (!this->LoadMaterial(*itr))
					{
						gLogError << "Could not load material for mesh: " << (*itr)["name"].GetString();
					}
				}

				return RenderComponent::Initialise(params);
			}

			void Mesh::Update(const double& deltaTime)
			{

			}

			void Mesh::Destroy()
			{
				if (this->_textureID > 0)
				{
					TextureManager* tm = Core::gEngine.GetSystem<RenderSystem>()->GetTextureManager();
					tm->DestroyTexture(this->_textureID);
				}

				if (this->_normalID > 0)
				{
					TextureManager* tm = Core::gEngine.GetSystem<RenderSystem>()->GetTextureManager();
					tm->DestroyTexture(this->_normalID);
				}

				if (this->_materialID > 0)
				{
					MaterialManager* mm = Core::gEngine.GetSystem<RenderSystem>()->GetMaterialManager();
					mm->DestroyMaterial(this->_materialID);
				}

				while (!this->_entries.empty())
				{
					this->_entries.back().Unload();
					this->_entries.pop_back();
				}

			}

			void Mesh::Render()
			{
				glEnable(GL_TEXTURE_2D);

				TextureManager* texMan = Core::gEngine.GetSystem<RenderSystem>()->GetTextureManager();
				
				glEnableVertexAttribArray(0);
				glEnableVertexAttribArray(1);
				glEnableVertexAttribArray(2);
				glEnableVertexAttribArray(3);
				//glEnableVertexAttribArray(4);

				for (const auto& it : this->_entries)
				{
					int test1 = sizeof(mathfu::Vector<float, 3>);
					int test2 = sizeof(mathfu::Vector<float, 2>);
					int test3 = sizeof(mathfu::Vector<float, 4>);
					glBindBuffer(GL_ARRAY_BUFFER, it._vb);
					glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3f), 0);
					//glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex3f), (const GLvoid*)12);
					glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex3f), (const GLvoid*)12);
					glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3f), (const GLvoid*)20);
					glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3f), (const GLvoid*)32);
					
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, it._ib);

					glEnableClientState(GL_TEXTURE_COORD_ARRAY);
					glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex3f), (const GLvoid*)12);

					if (this->_textureID != 0)
					{
						glActiveTexture(GL_TEXTURE0);
						glBindTexture(GL_TEXTURE_2D, this->_textureID);
					}

					if (this->_normalID != 0)
					{
						glActiveTexture(NORMAL_TEXTURE_UNIT);
						glBindTexture(GL_TEXTURE_2D, this->_normalID);
					}

					glDrawElements(GL_TRIANGLES, it._indexCount, GL_UNSIGNED_INT, 0);
					glDisableClientState(GL_TEXTURE_COORD_ARRAY);

					glActiveTexture(GL_TEXTURE0);

				}

				glDisableVertexAttribArray(0);
				glDisableVertexAttribArray(1);
				glDisableVertexAttribArray(2);
				glDisableVertexAttribArray(3);
				//glDisableVertexAttribArray(4);

				glDisable(GL_TEXTURE_2D);
			}

			Core::ActorComponent* Mesh::Create(const rapidjson::Value& params, Core::Actor* actor)
			{
				std::string name = actor->GetName();
				std::string type = params["type"].GetString();
				std::string actorID = std::to_string(actor->GetID());

				name.append("_").append(type).append("_").append(actorID);

				ActorComponent* result = new Mesh(name);

				if (result->Initialise(params["properties"]))
				{
					return std::move(result);
				}

				delete[] result;

				return nullptr;
			}

			void Mesh::SetTextureID(const unsigned int& id)
			{
				this->_textureID = id;
			}

			const unsigned int& Mesh::GetTextureID()
			{
				return this->_textureID;
			}

			void Mesh::SetNormalID(const unsigned int& id)
			{
				this->_normalID = id;
			}
			
			const unsigned int& Mesh::GetNormalID()
			{
				return this->_normalID;
			}

			bool Mesh::LoadMesh(const rapidjson::Value& params)
			{
				std::string meshFile = params["file"].GetString();

				Core::ResourceManager* rm = Core::gEngine.GetSystem<Core::ResourceManager>();
				std::string path = rm->GetResourcePath(meshFile);

				if (path.empty())
				{
					gLogError << "Mesh filepath invalid: " << meshFile;
					return false;
				}

				Assimp::Importer importer;
				const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate);

				if (!scene)
				{
					gLogError << "Error while importing file: " << importer.GetErrorString();
					return false;
				}

				bool hasTangents, hasNormals;

				for (unsigned int i = 0; i < scene->mNumMeshes; i++)
				{
					if (scene->mMeshes[i]->HasNormals())
					{
						hasNormals = true; 
					}
					else {
						hasNormals = false;
					}

					if (scene->mMeshes[i]->HasTangentsAndBitangents())
					{
						hasTangents = true;
					}
					else {
						hasTangents = false;
					}
				}

				if (!hasNormals)
				{
					scene = importer.ApplyPostProcessing(aiPostProcessSteps::aiProcess_GenNormals);
				}

				if (!hasTangents)
				{
					scene = importer.ApplyPostProcessing(aiPostProcessSteps::aiProcess_CalcTangentSpace);
				}

				this->_entries.resize(scene->mNumMeshes);
				for (unsigned int i = 0; i < this->_entries.size(); i++)
				{
					const aiMesh* mesh = scene->mMeshes[i];

					std::vector<Vertex3f> Vertices;
					std::vector<unsigned int> Indices;

					for (unsigned int j = 0; j < mesh->mNumVertices; j++)
					{
						const aiVector3D* pos = &(mesh->mVertices[j]);
						const aiVector3D* normal = &(mesh->mNormals[j]);
						const aiVector3D* texCoord;
						const aiVector3D* tangent;
						//const aiColor4D* color;

						if (mesh->HasTextureCoords(0))
						{
							texCoord = &(mesh->mTextureCoords[0][j]);
						}
						else {
							texCoord = new aiVector3D(0.0f, 0.0f, 0.0f);
						}

						if (mesh->HasTangentsAndBitangents())
						{
							tangent = &(mesh->mTangents[j]);
						}
						else {
							tangent = new aiVector3D(0.0f, 0.0f, 0.0f);
						}

						/*if (mesh->HasVertexColors(0))
						{
							color = &(mesh->mColors[0][j]);
						}
						else {
							color = new aiColor4D(0.2f, 0.2f, 0.2f, 1.0f);
						}*/

						Vertex3f vertex;
						vertex.position[0] = pos->x;
						vertex.position[1] = pos->y;
						vertex.position[2] = pos->z;

						vertex.normal[0] = normal->x;
						vertex.normal[1] = normal->y;
						vertex.normal[2] = normal->z;

						vertex.textureCoordinate[0] = texCoord->x;
						vertex.textureCoordinate[1] = texCoord->y;

						vertex.tangent[0] = tangent->x;
						vertex.tangent[1] = tangent->y;
						vertex.tangent[2] = tangent->z;

						/*vertex.color[0] = color->r;
						vertex.color[1] = color->g;
						vertex.color[2] = color->b;
						vertex.color[3] = color->a;*/

						Vertices.push_back(vertex);
					}

					for (unsigned int j = 0; j < mesh->mNumFaces; j++)
					{
						const aiFace& Face = mesh->mFaces[j];

						if (Face.mNumIndices == 3)
						{
							Indices.push_back(Face.mIndices[0]);
							Indices.push_back(Face.mIndices[1]);
							Indices.push_back(Face.mIndices[2]);
						}
					}

					this->_entries[i].Init(Vertices, Indices);
				}

				return true;
			}

			bool Mesh::LoadShading(const rapidjson::Value& params)
			{
				if (params.HasMember("shader"))
				{
					
				}
				else {
					// load a "default" shader
				}
				return true;
			}

			bool Mesh::LoadTextures(const rapidjson::Value& params)
			{
				if (params.HasMember("diffuse"))
				{
					TextureManager* textureManager = Core::gEngine.GetSystem<RenderSystem>()->GetTextureManager();
					this->_textureID = textureManager->LoadTexture(params["diffuse"]);
					if (this->_textureID == 0) return false;
				}
				else {
					// load a "default" diffuse texture
				}

				if (params.HasMember("normal"))
				{
					TextureManager* textureManager = Core::gEngine.GetSystem<RenderSystem>()->GetTextureManager();
					this->_normalID = textureManager->LoadTexture(params["normal"]);
					if (this->_normalID == 0) return false;
				}
				else {
					// load a "default" normal texture
				}

				return true;
			}

			bool Mesh::LoadMaterial(const rapidjson::Value& params)
			{
				if (params.HasMember("material"))
				{
					MaterialManager* matManager = Core::gEngine.GetSystem<RenderSystem>()->GetMaterialManager();
					this->_materialID = matManager->LoadMaterial(params["material"]);
					if (this->_materialID == 0)
					{
						gLogError << "Could not load material for mesh";
					}
				}
				else {
					// load a "default" material
				}

				return true;
			}

			Mesh::MeshEntry::MeshEntry() :
				_ib(INVALID_OGL_VALUE),
				_vb(INVALID_OGL_VALUE),
				_indexCount(0)
			{
				
			}

			Mesh::MeshEntry::~MeshEntry()
			{
				if (this->_vb != INVALID_OGL_VALUE)
				{
					glDeleteBuffers(1, &this->_vb);
				}

				if (this->_ib != INVALID_OGL_VALUE)
				{
					glDeleteBuffers(1, &this->_ib);
				}

				this->_indexCount = 0;
			}

			void Mesh::MeshEntry::Init(const std::vector<Vertex3f>& vertices, const std::vector<unsigned int>& indices)
			{
				this->_indexCount = indices.size();

				glGenBuffers(1, &this->_vb);
				glBindBuffer(GL_ARRAY_BUFFER, this->_vb);
				glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex3f) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

				glGenBuffers(1, &this->_ib);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->_ib);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * this->_indexCount, &indices[0], GL_STATIC_DRAW);
			}

			void Mesh::MeshEntry::Unload()
			{
				if (this->_vb != INVALID_OGL_VALUE)
				{
					glDeleteBuffers(1, &this->_vb);
				}

				if (this->_ib != INVALID_OGL_VALUE)
				{
					glDeleteBuffers(1, &this->_ib);
				}

				this->_indexCount = 0;
			}


			
		}

	}

}