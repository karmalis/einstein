#ifndef __GL_MATERIAL_HPP__
#define __GL_MATERIAL_HPP__

#include "../OGLIncludes.hpp"
#include <rapidjson\document.h>
#include <mathfu\vector.h>

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			using Colour4f = mathfu::Vector < float, 4 >;

			class Material {

			public:

				Material();
				~Material();

				bool Load(const rapidjson::Value& params);
				void Destroy();

				const unsigned int GetID();
				const std::string& GetName();
				const Colour4f& GetEmissive();
				const Colour4f& GetAmbient();
				const Colour4f& GetDiffuse();
				const Colour4f& GetSpecular();
				const float& GetShininess();

				void SetID(const unsigned int& id);
				void SetName(const std::string& name);
				void SetEmissive(const Colour4f& colour);
				void SetDiffuse(const Colour4f& colour);
				void SetSpecular(const Colour4f& colour);
				void SetAmbient(const Colour4f& colour);
				void SetShininess(const float& value);




			protected:

				unsigned int _ID;
				std::string _name;

				Colour4f _emissive;
				Colour4f _ambient;
				Colour4f _diffuse;
				Colour4f _specular;
				float _shininess;

			};

		}

	}

}

#endif