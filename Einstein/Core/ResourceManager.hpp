#ifndef __RESOURCE_MANAGER_HPP__
#define __RESOURCE_MANAGER_HPP__

#include "System.hpp"

#include <filesystem>
#include <fstream>
#include <istream>
#include <unordered_map>
#include <vector>
#include <string>

#include <rapidjson\document.h>
#include <rapidjson\reader.h>
#include <rapidjson\stringbuffer.h>

#ifndef DATA_ACTORS
#define DATA_ACTORS "actors"
#endif

#ifndef DATA_MODELS
#define DATA_MODELS "models"
#endif

#ifndef DATA_SPRITES
#define DATA_SPRITES "sprites"
#endif

#ifndef DATA_SCRIPTS
#define DATA_SCRIPTS "scripts"
#endif

#ifndef DATA_SCENES
#define DATA_SCENES "scenes"
#endif

#ifndef DATA_SHADERS
#define DATA_SHADERS "shaders"
#endif

#ifndef DATA_TEXTURES
#define DATA_TEXTURES "textures"
#endif

#ifndef DATA_SOUNDS
#define DATA_SOUNDS "sounds"
#endif

#ifndef DATA_UI
#define DATA_UI "ui"
#endif

#ifndef DATA_OTHER
#define DATA_OTHER "other"
#endif

namespace Einstein
{
	namespace Core
	{
		class ResourceManager : public System
		{
		public:

			ResourceManager();

			enum {
				IDLE = 0x0,

				INIT = 0x1 << 0,
				LOAD_RESOURCES = 0x1 << 1
			};

			virtual bool Initialise();
			virtual void Update();
			virtual void ShutDown();

			const rapidjson::Document& GetJSONDocumentResource(std::string name);
			const char* GetBinaryDataResource(std::string name);
			const std::string& GetResourcePath(const std::string& name);

		protected:

			// Filename / Full path
			std::unordered_map<std::string, std::string> _resourceFileMap;
			// Group / filename
			std::unordered_map<std::string, std::vector<std::string>> _groupResourceMap;

			// Loaded items
			std::unordered_map<std::string, rapidjson::Document> _parsedJSONDocuments;
			std::unordered_map<std::string, const char* > _loadedBinaryData;

			void ConstructFileSystem();
			void ConstructSingleElementFS(std::string dataElement);
			void ScanFolder(std::string folder, std::string data);

			void PreLoadFiles(std::string sceneFileName = "");
			void PreLoadSingleFile(std::string name, std::string file);
		};
	}
}

#endif