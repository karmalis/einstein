#ifndef __VIDEO_TEST_HPP__
#define __VIDEO_TEST_HPP__

#include "../Application.hpp"

#include "../../Core/ResourceManager.hpp"

namespace Einstein {

	namespace Test {

		class VideoTest : public App::Application
		{
		public:

			VideoTest();

			virtual bool Initialise();
			virtual void Update();
			virtual void ShutDown();
		};

	}

}

#endif