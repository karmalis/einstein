#include "Exception.hpp"
#include "../Core/Logging/Logger.hpp"

namespace Einstein {

	namespace Util {

		Exception::Exception(std::string name, std::string description) :
			Named(name),
			_description(description)
		{

		}

		Exception::Exception(std::string description) :
			Named("Exception"),
			_description(description)
		{

		}

		const char* Exception::what() const
		{
			std::string message = " Exception: ";

			if (this->_description.length() > 0)
			{
				message.append(this->_description);
			}
			else {
				message.append(" Not Defined ");
			}

			gLogError << message;

			return message.c_str();
		}


	}

}