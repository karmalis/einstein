#pragma once

#include "LogMessage.hpp"

namespace Einstein {
	namespace Core {

		template <typename T>
		LogMessage& LogMessage::operator << (const T& message) {
			this->_buffer << message;
			return *this;
		}

	}
}
