#ifndef __GL_TEXTURE_MANAGER_HPP__
#define __GL_TEXTURE_MANAGER_HPP__

#include "../../../Core/System.hpp"
#include "../../../Core/ResourceManager.hpp"

#include "../OGLSystem.hpp"

#include "../OGLIncludes.hpp"
#include "Texture.hpp"

#include <unordered_map>
#include <list>

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			extern tbb::atomic<unsigned int> globalTextureID;

			class TextureManager
			{

			public:

				using TextureNamedMap = std::unordered_map<std::string, Texture*>;
				using TextureIDMap = std::unordered_map<unsigned int, Texture*>;
				using TextureList = std::list<Texture*>;

				TextureManager();

				virtual bool Initialise();
				virtual void Update();
				virtual void ShutDown();

				Texture* GetTexture(const unsigned int& id);
				Texture* GetTexture(const std::string& name);

				bool TextureLoaded(const std::string& name);

				const unsigned int& LoadTexture(const rapidjson::Value& params);
				const unsigned int& LoadFromMemory(const unsigned char* data, const unsigned int& width, const unsigned int& height, const std::string& name = "");
				const unsigned int& LoadFromFile(const std::string& filename, const std::string& name);
				const unsigned int& LoadCubeMap(const std::vector<std::string>& filenames, const std::string& name);

				bool UpdateTexture(const unsigned int& id, const unsigned char* data);

				void DestroyTexture(const std::string& hash);
				void DestroyTexture(Texture* texturePtr);
				void DestroyTexture(const unsigned int& id);


			protected:

				TextureNamedMap  _namedTextureMap;
				TextureIDMap _idTextureMap;
				TextureList _textureList;

				RenderSystem* _renderSystem;
			};

		}

	}

}

#endif