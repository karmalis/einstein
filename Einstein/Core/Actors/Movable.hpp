#ifndef __MOVABLE_HPP__
#define __MOVABLE_HPP__


#define MATHFU_COMPILE_WITHOUT_SIMD_SUPPORT 1

//#include <Eigen\Dense>
#include <mathfu\vector_3.h>
#include <mathfu\quaternion.h>
#include <mathfu\constants.h>
#include <unordered_map>

#define DEFAULT_SCALE 1.0

namespace Einstein {

	namespace Core {

		/**
		*	Movable class\n
		*	This class defines all the necessary attributes and functions in order for the actor to move\n
		*	It also acts as a base class for the actor
		*/
		class Movable
		{
		public:

			/**
			*	Default global X axis
			*/
			static mathfu::Vector<float, 3> UNIT_X;

			/**
			*	Default global Y axis
			*/
			static mathfu::Vector<float, 3> UNIT_Y;

			/**
			*	Default global Z axis
			*/
			static mathfu::Vector<float, 3> UNIT_Z;

			/**
			*	The Movement vector map definition\n
			*	Provides a structure where the object has to move on each iteration
			*/
			using MovementVectorMap = std::unordered_map < unsigned int, mathfu::Vector<float, 3> > ;

			/**
			*	The Movement speed map definition\n
			*	Provides a structure on how fast the object moves on each iteration
			*/
			using MovementSpeedMap = std::unordered_map < unsigned int, float > ;

			enum {
				FREE,
				ORBITAL,
				FPS
			};

			/**
			*	Constructor
			*/
			Movable();


			/**
			*	Virtual Destructor
			*/
			virtual ~Movable() {}

			/**
			*	Pitches the movable by a specified angle
			*	@param angle a float32 angle
			*/
			void Pitch(const float& angle);

			/**
			*	Yaws the movable by a specified angle
			*	@param angle a float32 angle
			*/
			void Yaw(const float& angle);

			/**
			*	Rolls the movable by a specified angle
			*	@param angle a float32 angle
			*/
			void Roll(const float& angle);

			/**
			*	Rotate the movable by a specified axis and angle
			*	Pitch, Yaw and Roll call this function with the default axis
			*	@param axis a 3D Vector of the wanted axis
			*	@param angle a float32 angle
			*/
			void Rotate(const mathfu::Vector<float, 3>& axis, const float& angle);

			/**
			*	Move the movable by a certain distance in a certain direction
			*	@param distance a float32 type variable
			*	@param direction a 3D Vector defining a direction
			*/
			virtual void Move(const float& distance, const mathfu::Vector<float, 3>& direction);

			/**
			*	Sets the position of the movable
			*	@param position a 3D Vector point of the position
			*/
			void SetPosition(const mathfu::Vector<float, 3>& position) { this->_position = position; };

			/**
			*	Creates a new move order for future update iterations\n
			*	Each tick, the movable will move in a certain direction
			*	by a certain distance until stopped\n
			*	Movement is basically a cumulative vector
			*	@param speed the speed that the object will be moved
			*	@param direction a 3D vector presenting the direction
			*/
			void TriggerMove(const float& speed, const mathfu::Vector<float, 3>& direction);

			/**
			*	Stops movement order for the movable in a certain direction\n
			*	@param direction a 3D vector presenting the direction
			*/
			void TriggerStop(const mathfu::Vector<float, 3>& direction);

			/**
			*	Creates a new pitch order for future update iterations\n
			*	Each tick, the movable will pitch in a certain direction
			*	by a certain angle until stopped
			*	@param angle a float32 angle
			*/
			void TriggerPitch(const float& angle);

			/**
			*	Creates a new yaw order for future update iterations\n
			*	Each tick, the movable will yaw in a certain direction
			*	by a certain angle until stopped
			*	@param angle a float32 angle
			*/
			void TriggerYaw(const float& angle);

			/**
			*	Creates a new roll order for future update iterations\n
			*	Each tick, the movable will roll in a certain direction
			*	by a certain angle until stopped
			*	@param angle a float32 angle
			*/
			void TriggerRoll(const float& angle);

			/**
			* Will stop all movement and rotation orders
			*/
			void AllStop();

			/**
			*	Update iteration function for the movable\n
			*	Each actor will call this on the Update() function
			*	@param deltaTime a float32 variable
			*/
			void UpdateMovable(const double& deltaTime);

			/**
			*	Gets the current rotation on the X axis
			*	@return the current rotation in X axis as a float32
			*/
			const float& GetRotationX() const
			{
				return this->_rotX;
			}

			/**
			*	Gets the current rotation on the Y axis
			*	@return the current rotation in Y axis as a float32
			*/
			const float& GetRotationY() const
			{
				return this->_rotY;
			}

			/**
			*	Gets the current rotation on the Z axis
			*	@return the current rotation in Z axis as a float32
			*/
			const float& GetRotationZ() const
			{
				return this->_rotZ;
			}

			/**
			*	Gets thecurrent position of the movable
			*	@return a 3D Vector of the current movable position
			*/
			const mathfu::Vector<float, 3>& GetPosition() const
			{
				return this->_position;
			}

			/**
			*	Gets the current up direction relative to the movable
			*	@return a 3D Vector of the up direction
			*/
			const mathfu::Vector<float, 3>& GetUp() const
			{
				return this->_up;
			}

			/**
			*	Gets the current view (forward) direction relative to the movable
			*	@return a 3D Vector of the view direction
			*/
			const mathfu::Vector<float, 3>& GetViewDirection() const
			{
				return this->_viewDir;
			}

			/**
			*	Gets the current right direction relative to the movable
			*	@return a 3D Vector of the right direction
			*/
			const mathfu::Vector<float, 3>& GetRight()  const
			{
				return this->_right;
			}

			/**
			*	Gets the current scale for all 3 axis
			*	@return a 3D Vector of the scale
			*/
			const mathfu::Vector<float, 3>& GetScaleVector() const
			{
				return this->_scale;
			}

			/**
			*	Gets the current scale for the X axis
			*	@return a float32 of the X axis scale
			*/
			const float& GetScaleX() const
			{
				return this->_scale(0);
			}

			/**
			*	Gets the current scale for the Y axis
			*	@return a float32 of the Y axis scale
			*/
			const float& GetScaleY() const
			{
				return this->_scale(1);
			}

			/**
			*	Gets the current scale for the Z axis
			*	@return a float32 of the Z axis scale
			*/
			const float& GetScaleZ() const
			{
				return this->_scale(2);
			}

			/**
			*	Returns if the target needs to calculated using the direction vector or use an actual target already defined
			*	@return a bool flag true if the object needs to return
			*/
			const bool& GetCalcTargetByDir() { return this->_calculateTargetByDirection; }

			/**
			*	Returns the object movement type
			*	@return a MovableType enumarator for the MovableType
			*/
			const unsigned short& GetMovementType() { return this->_movementType; }

			/**
			*	Set the up direction of the movable
			*	@param up a 3D vector of the up direction
			*/
			void SetUp(const mathfu::Vector<float, 3>& up) { this->_up = up; }

			/**
			*	Set the view (forward) direction of the movable
			*	@param viewDir a 3D vector of the up direction
			*/
			void SetViewDir(const mathfu::Vector<float, 3>& viewDir) { this->_viewDir = viewDir; }

			/**
			*	Set the right direction of the movable
			*	@param right a 3D vector of the up direction
			*/
			void SetRight(const mathfu::Vector<float, 3>& right) { this->_right = right; }

			/**
			*	Set the current movable rotation in X axis
			*	@param x a float32 angle
			*/
			void SetRotationX(const float& x);

			/**
			*	Set the current movable rotation in Y axis
			*	@param y a float32 angle
			*/
			void SetRotationY(const float& y);

			/**
			*	Set the current movable rotation in Z axis
			*	@param z a float32 angle
			*/
			void SetRotationZ(const float& z);

			/**
			*	Set the current scale in X axis
			*	@param x a float32 scale
			*/
			void SetScaleX(const float& x) { this->_scale(0) = x; }

			/**
			*	Set the current scale in Y axis
			*	@param y a float32 scale
			*/
			void SetScaleY(const float& y) { this->_scale(1) = y; }

			/**
			*	Set the current scale in Z axis
			*	@param z a float32 scale
			*/
			void SetScaleZ(const float& z) { this->_scale(2) = z; }

			/**
			*	Set the current scale in all axis
			*	@param scaleVector a Vector3D scale in all axis
			*/
			void SetScale(mathfu::Vector<float, 3> scaleVector) { this->_scale = scaleVector; }

			/**
			*	Set the movement type for the object
			*	@param type a MovableType enumarator as the type
			*/
			void SetMovementType(const unsigned short& type) { this->_movementType = type; }

			/**
			*	Set the flag to true or false if the movable target needs to be calculated based on direction
			*	@param value a bool flag
			*/
			void SetCalcTargetByDir(const bool& value) { this->_calculateTargetByDirection = value; }

			/**
			*	Toggles the flag for calculating the movable target by direction
			*/
			void ToggleCalcTargetByDir() { this->_calculateTargetByDirection = !this->_calculateTargetByDirection; }

			/**
			*	Basically sets the scale of the movable to teh default one in all three axis
			*	@see ROM_MOVABLE_DEFAULT_SCALE
			*/
			void ResetScale() { this->_scale(0) = DEFAULT_SCALE; this->_scale(1) = DEFAULT_SCALE; this->_scale(2) = DEFAULT_SCALE; }

			/**
			*	Gets the oreantation quaternion
			*	@param cal a bool flag if it needs to be recalculated. It can be used to force-recalculate the oreantation since it only updates during an update iteration
			*/
			const mathfu::Quaternion<float>& GetOrientation(bool calc = false);

			/**
			*	Set the oreantation of the movable
			*	@param orientation a Quaternion that is already pre-calculated for the oreantation fo the object
			*/
			void SetOrientation(const mathfu::Quaternion<float>& orientation) { this->_oreantation = orientation; }

			/**
			*	Get the target of the movable
			*	@return a 3d vector of the target position
			*/
			const mathfu::Vector<float, 3>& GetTarget() { return this->_target; }

		protected:

			unsigned short _movementType;

			/**
			* Current rotation angle
			*/
			float _rotX, _rotY, _rotZ;
			/**
			*	Position of the movable
			*/
			mathfu::Vector<float, 3> _position;

			/**
			*	View (forward) direction of the movable
			*/
			mathfu::Vector<float, 3> _viewDir;

			/**
			*	Up direction of the movable
			*/
			mathfu::Vector<float, 3> _up;

			/**
			*	Right direction of the movable
			*/
			mathfu::Vector<float, 3> _right;

			/**
			*	Target position of the movable
			*/
			mathfu::Vector<float, 3> _target;

			/**
			*	Scale vector of the movable
			*/
			mathfu::Vector<float, 3> _scale;

			/**
			*	Current movable oreantation quaternion
			*/
			mathfu::Quaternion<float> _oreantation;

			/**
			*	The flag to determine if the movable target needs to be calculated based on direction
			*/
			bool _calculateTargetByDirection;

			/**
			*	The movement direction to be moved to
			*/
			mathfu::Vector<float, 3> _currMovementDirection;

			/**
			* The distance to be moved
			*/
			float _currDistance;

			/**
			*	The angle to be pitched in the next update iteration
			*/
			float _currPitchAngle;

			/**
			*	The angle to be yawed in the next update iteration
			*/
			float _currYawAngle;

			/**
			*	The angle to be rolled in the next update iteration
			*/
			float _currRollAngle;

			
		};

	}

}

#endif