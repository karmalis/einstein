#ifndef __UNIVERSAL_SCENE_HPP__
#define __UNIVERSAL_SCENE_HPP__

#include "System.hpp"
#include "../Util/Exception.hpp"

#include "UIndexable.hpp"
#include "UObject.hpp"

#include "Scene\Loader.hpp"

// Include default core components
#include "Actors\Components\CameraComponent.hpp"

#include <tbb\mutex.h>
#include <unordered_map>
#include <list>

namespace Einstein {

	namespace Core {

		class UniversalObject;
		class UIndexable;

		class SceneRoot : public UniversalObject, public Util::NonCopyable
		{
		public:
			
			SceneRoot(); 
			~SceneRoot();

			void ClearSceneObjects();
		};

		class UniversalScene : public System {

		public:

			using ScopedLock = tbb::mutex::scoped_lock;

			struct Indexer {

				enum {
					NONE = 0,
					ADD = 1,
					REMOVE = 2,
					REINDEX = 3
				};

				UIndexable* _index;
				short command;
			};

			struct LoadScene {
				std::string scene;
				LoadScene(const std::string& sceneFile) : scene(sceneFile) { }
			};

			using IndexPtr = std::unique_ptr < UIndexable >;
			using IndexList = std::list < IndexPtr > ;
			using IndexMap = std::unordered_map < std::string, UIndexable* >;

			UniversalScene();

			virtual bool Initialise();
			virtual void Update();
			virtual void ShutDown();

			SceneRoot* GetSceneRoot() const;

			void UnloadScene();

			void AddIndex(UIndexable* object);
			void Remove(std::string index);
			void Remove(UIndexable* index);
			void Reindex(UIndexable* object);

			void operator()(const Indexer& ri);
			void operator()(const LoadScene& ls);

			CameraComponent* GetMainCamera();

			template <typename T>
			T* GetIndexObject(const std::string& name) const;

			template <typename T>
			std::list<T*> GetIndexObjectsByType() const;

			friend class Loader;

			Loader* GetSceneLoader();
		
		protected:

			void AddIndex(std::unique_ptr<UIndexable> object);

			IndexList _uIndexList;
			IndexMap _uIndexMap;

			Loader* _sceneLoader;

			Engine* GetEngine() const { return this->_engine; }

			tbb::mutex _mutex;

		};

	}

}

#include "UScene.inl"

#endif