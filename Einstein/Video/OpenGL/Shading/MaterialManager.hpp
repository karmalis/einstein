#ifndef __GL_MATERIAL_MANAGER_HPP__
#define __GL_MATERIAL_MANAGER_HPP__

#include "../../../Core/System.hpp"

#include "Material.hpp"
#include <unordered_map>
#include <list>

namespace Einstein {

	namespace Video {

		namespace OpenGL {

			extern tbb::atomic<unsigned int> globalMaterialID;

			class MaterialManager
			{

			public:

				using MaterialNameMap = std::unordered_map<std::string, Material*>;
				using MaterialIDMap = std::unordered_map<unsigned int, Material*>;
				using MaterialList = std::list<Material*>;

				MaterialManager();

				virtual bool Initialise();
				virtual void Update();
				virtual void ShutDown();

				Material* GetMaterial(const unsigned int& id);
				Material* GetMaterial(const std::string& name);

				bool MaterialExists(const std::string& name);

				const unsigned int& LoadMaterial(const rapidjson::Value& params);
				
				void DestroyMaterial(const std::string& hash);
				void DestroyMaterial(Material* materialPtr);
				void DestroyMaterial(const unsigned int& id);


			protected:

				MaterialNameMap  _namedMaterialMap;
				MaterialIDMap _idMaterialMap;
				MaterialList _materialList;
			};

		}

	}

}

#endif