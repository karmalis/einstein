#ifndef __ELEMENT_2D_HPP__
#define __ELEMENT_2D_HPP__

#include <mathfu\vector.h>

#include "OGLIncludes.hpp"
#include "../Vertex.hpp"

namespace Einstein {

	namespace Video {

		class Element2D
		{
		public:

			Element2D();
			virtual ~Element2D() {};

			virtual void Render();
			virtual bool Load();
			virtual void Destroy();


			const unsigned char* GetData() { return this->_data; }
			const unsigned int& GetTextureID() { return this->_textureID; }
			const unsigned int& GetVB()  { return this->_vb; }
			const unsigned int& GetIB() { return this->_ib; }
			const mathfu::Vector<float, 3>& GetPosition() { return this->_position; }
			const mathfu::Vector<float, 2>& GetSize() { return this->_size; }
			const mathfu::Vector<float, 2>& GetScale() { return this->_scale; }

			void SetData(const unsigned char* data) { this->_data = data; }
			void SetTextureID(const unsigned int& id) { this->_textureID = id; }
			void SetPosition(const mathfu::Vector<float, 3>& pos) { this->_position = pos; }
			void SetSize(const mathfu::Vector<float, 2>& size) { this->_size = size; }
			void SetScale(const mathfu::Vector<float, 2>& scale) { this->_scale = scale; }
			void SetVB(const unsigned int& vb) { this->_vb = vb; }
			void SetIB(const unsigned int& ib) { this->_ib = ib; }

			void SetFlip(const bool& value) { this->_flip = value; }
			const bool& GetFlipValue() { return this->_flip; }

		protected:

			mathfu::Vector<float, 3> _position;
			mathfu::Vector<float, 2> _size;
			mathfu::Vector<float, 2> _scale;
			const unsigned char* _data;
			GLuint _textureID;
			GLuint _vb;
			GLuint _ib;
			
			bool _flip;

		};

	}

}

#endif