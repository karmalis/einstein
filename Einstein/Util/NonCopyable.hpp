#ifndef __NONCOPYABLE_HPP__
#define __NONCOPYABLE_HPP__

namespace Einstein {

	namespace Util {

		class NonCopyable
		{
		
		private:
			NonCopyable(const NonCopyable& copy) = delete;
			const NonCopyable& operator= (const NonCopyable&) = delete;
		
		protected:

			NonCopyable() {}
			~NonCopyable() {}

		};

	}

}

#endif